﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ProyectosMultiples
{
    public partial class Inicio : Form
    {
        public Inicio()
        {
            InitializeComponent();
        }
        int seg = 0;
        private void timer1_Tick(object sender, EventArgs e)
        {
            // Codigo para la barra de cargando...
            seg++;
            switch (seg)
            {
                case 19:
                    labelTexto.Text = "Inicializando...";
                    break;
                case 39:
                    labelTexto.Text = "Arrancando procesos...";
                    break;
                case 59:
                    labelTexto.Text = "Conectando con el servidor...";
                    break;
                case 79:
                    labelTexto.Text = "Conectando con la base de datos...";
                    break;
                case 99:
                    labelTexto.Text = "Finalizando...";
                    break;
            }
            if (seg > 100)
            {
                if (seg >= 120)
                {
                    DialogResult = DialogResult.OK;
                    this.Close();
                }
            }
            else
            {
                progressBar1.Value = seg;
            }
        }

        private void Inicio_Load(object sender, EventArgs e)
        {

        }
    }
}
