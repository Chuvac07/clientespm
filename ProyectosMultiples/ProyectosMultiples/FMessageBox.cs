﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ProyectosMultiples
{
    public partial class FMessageBox : Form
    {
        public FMessageBox(string informacion, string inicio,int c)
        {
            InitializeComponent();
            lbInicio.Text = inicio;
            txtInformacion.Text = informacion;
            if (c == 1)
            {
                txtInformacion.ForeColor = Color.Red;
            }
        }

        private void FMessageBox_Load(object sender, EventArgs e)
        {

        }

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
        }
    }
}
