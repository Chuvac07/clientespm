﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;

namespace ProyectosMultiples
{
    public partial class FCotizaciones : Form
    {
        public FCotizaciones()
        {
            InitializeComponent();
        }

        Conexion conect = new Conexion();
        private void label10_Click(object sender, EventArgs e)
        {

        }

        // variables globales para recibir los valores del FORM CLIENTES
        public static string idCliente = "";
        public static string nombreCliente = "";
        public static string apellidoCliente = "";
        public static string nitCliente = "";
        public static string telefonoCliente = "";
        public static string emailCliente = "";

        private void FCotizaciones_Load(object sender, EventArgs e)
        {
            try
            {
                if (nombreCliente == apellidoCliente)
                {
                    label2.Visible = false;
                    Lbapellido.Visible = false;
                }
                else
                {
                    label2.Visible = true;
                    Lbapellido.Visible = true;
                }

                Lbnombre.Text = nombreCliente;
                Lbapellido.Text = apellidoCliente;
                Lbnit.Text = nitCliente;
                Lbtelefono.Text = telefonoCliente;
                Lbemail.Text = emailCliente;
                cbEstado.Text = cbEstado.Items[0].ToString();
                if (Lbnombre.Text != "" && Lbapellido.Text != "")
                {
                    btnNuevoCliente.Enabled = false;
                    gBCotizacion.Visible = true;
                    this.ActiveControl = txtCantidad;
                }
                conect.actualizarCotizacion(dgvCotizaciones,"Todos");
                lbCotizacion.Text = Convert.ToString(dgvCotizaciones.Rows.Count + 1);
            }
            catch(Exception ex)
            {
                FMessageBox obj = new FMessageBox("Error COT1: " + ex.Message, "¡Error de Conexión!", 1);
                if (obj.ShowDialog() == DialogResult.OK) ;
            }
        }

        private void label12_Click(object sender, EventArgs e)
        {

        }

        private void listBox3_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void radioButton3_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void listBox2_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void label10_Click_1(object sender, EventArgs e)
        {

        }

        private void label13_Click(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            dgvProductos.Rows.Clear();
            lbTotal.Text = "0";
        }

        private void button3_Click(object sender, EventArgs e)
        {
            decimal total;
            dgvProductos.Rows.Remove(dgvProductos.CurrentRow);
            total = totalVentas(dgvProductos, "subTotal");
            lbTotal.Text = Convert.ToString(total);
            this.ActiveControl = txtCantidad;
        }

        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }


        // Funcion para determinar el total de la Cotización
        public decimal totalVentas(DataGridView datos, string columna)
        {
            decimal total = 0;

            foreach (DataGridViewRow f in datos.Rows)
            {
                total += Convert.ToDecimal(f.Cells[columna].Value);
            }

            return total;
        }
        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                int cantidad = Convert.ToInt16(txtCantidad.Text);
                string descripcion = txtDescripcion.Text;
                decimal precioU = Convert.ToDecimal(txtPrecioU.Text);
                decimal subtotal = cantidad * precioU;
                decimal total;
                if (descripcion != "")
                {
                    DataGridViewRow fila = new DataGridViewRow();
                    fila.CreateCells(dgvProductos);
                    fila.Cells[0].Value = txtCantidad.Text;
                    fila.Cells[1].Value = txtDescripcion.Text;
                    fila.Cells[2].Value = txtPrecioU.Text;
                    fila.Cells[3].Value = Convert.ToString(subtotal);
                    dgvProductos.Rows.Add(fila);
                    txtPrecioU.Clear();
                    txtDescripcion.Clear();
                    txtCantidad.Clear();
                    this.ActiveControl = txtCantidad;
                    total = totalVentas(dgvProductos,"subTotal");
                    lbTotal.Text = Convert.ToString(total);
                }
                else
                {
                    FMessageBox obj = new FMessageBox("Se deben ingresar todos los campos que se solicitan.", "Error COT", 1);
                    if (obj.ShowDialog() == DialogResult.OK) ;
                }
            }
            catch (Exception ex)
            {
                FMessageBox obj = new FMessageBox("Error COT2: " + ex.Message, "¡Error de Conexión!", 1);
                if (obj.ShowDialog() == DialogResult.OK) ;
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
           

        }

        private void btnNuevoCliente_Click(object sender, EventArgs e)
        {
            Form1.acceso = 3;
        }
        int elementos = 0;
        private void btnfinalizar_Click(object sender, EventArgs e)
        {
            try
            {
                DateTime fechaActual = dtfecha1.Value;
                DateTime fechaFururo = fechaActual.AddDays(15);
                string estado = cbEstado.Text;
                int usuarioId = Form1.idUsuario;
                string nitCliente = Lbnit.Text;
                string condi = txtCondiciones.Text;
                string ti = txtTiempo.Text;
                string obse = txtObservaciones.Text;
                int cantidadProducto;
                string descripcionProducto;
                double precioUnitarioProducto;
                double subtotalProducto;
                int IdCotizacion = 0;
                double total = Convert.ToDouble(lbTotal.Text);
                elementos = dgvProductos.Rows.Count; 
                conect.ingresarCotizaciones(fechaActual,estado,usuarioId,idCliente, condi, ti, obse, fechaFururo, total);
                foreach (DataGridViewRow columna in dgvProductos.Rows)
                {
                    cantidadProducto = Convert.ToInt16(columna.Cells[0].Value.ToString());
                    descripcionProducto = columna.Cells[1].Value.ToString();
                    precioUnitarioProducto = Convert.ToDouble(columna.Cells[2].Value.ToString());
                    subtotalProducto = cantidadProducto * precioUnitarioProducto;
                    IdCotizacion = Convert.ToInt16(lbCotizacion.Text);
                    conect.ingresarProductosCot(cantidadProducto,descripcionProducto,precioUnitarioProducto, subtotalProducto, IdCotizacion);
                }
                exportarPdf(Convert.ToString(IdCotizacion));

                Lbnombre.Text = "";
                Lbapellido.Text = "";
                Lbtelefono.Text = "";
                Lbemail.Text = "";
                lbTotal.Text = "0";
                Lbnit.Text = "";
                gBCotizacion.Visible = false;
                int noCotizacion = Convert.ToInt16(lbCotizacion.Text);
                noCotizacion++;
                lbCotizacion.Text = Convert.ToString(noCotizacion);
                btnNuevoCliente.Enabled = true; 
            }
            catch (Exception ex)
            {
                FMessageBox obj = new FMessageBox("Error: COT3" + ex.Message, "¡Error de Conexión!", 1);
                if (obj.ShowDialog() == DialogResult.OK) ;
            }
        }

        // Funcion para generar los PDF 
        public void exportarPdf(string nombreC)
        {
            int m = dtfecha1.Value.Month;
            int d = dtfecha1.Value.Day;
            int a = dtfecha1.Value.Year;
            string ff = Convert.ToString(d) + "-" + Convert.ToString(m) + "-" +Convert.ToString(a);
            string mes = "";
            switch (m)
            {
                case 1: mes = "Enero"; break;
                case 2: mes = "Febrero"; break;
                case 3: mes = "Marzo"; break;
                case 4: mes = "Abril"; break;
                case 5: mes = "Mayo"; break;
                case 6: mes = "Junio"; break;
                case 7: mes = "Julio"; break;
                case 8: mes = "Agosto"; break;
                case 9: mes = "Septiembre"; break;
                case 10: mes = "Octubre"; break;
                case 11: mes = "Noviembre"; break;
                case 12: mes = "Diciembre"; break;
            }
            string escritorio = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
            string cliente = Lbnombre.Text;
            string carpeta = escritorio + "\\" + "PM Cotizaciones" + "\\" + mes;
            // string carpeta = @"C:\Proyectos Multiples\" + mes;
            string subcarpeta = carpeta + "\\" + cliente;

            if (!(Directory.Exists(carpeta)))
            {
                Directory.CreateDirectory(carpeta);
            }
            if (!(Directory.Exists(subcarpeta)))
            {
                Directory.CreateDirectory(subcarpeta);
            }
            

            string ruta = subcarpeta + "\\" + nombreC + " Cotización "+ ff +".pdf";
            float porcentaje = 0.0f;

            Document doc = new Document(iTextSharp.text.PageSize.A4);
            doc.SetMargins(10, 10, 210, 10);
            PdfWriter escribir = PdfWriter.GetInstance(doc, new FileStream(ruta, FileMode.Create));
            doc.Open();

            // Agregar Imagen de Encabezado al PDF
            iTextSharp.text.Image encabezado = iTextSharp.text.Image.GetInstance("Encabezado.jpg");
            encabezado.SetAbsolutePosition(50, 690);
            porcentaje = 500 / encabezado.Width;
            encabezado.ScalePercent(porcentaje * 100);

            // Agregar Imagen del Cliente al PDF
            iTextSharp.text.Image persona = iTextSharp.text.Image.GetInstance("Cliente.jpg");
            persona.SetAbsolutePosition(50, 630);
            porcentaje = 500 / persona.Width;
            persona.ScalePercent(porcentaje * 100);


            // Agregar la DATAGRIDVIEW al PDF
            PdfPTable pdf = new PdfPTable(dgvProductos.Columns.Count);
            BaseFont bf = BaseFont.CreateFont(BaseFont.TIMES_BOLD, BaseFont.CP1252 , BaseFont.EMBEDDED);

            PdfContentByte cb = escribir.DirectContent;
            
            pdf.DefaultCell.Padding = 1;
            pdf.HorizontalAlignment = Element.ALIGN_CENTER;

            float[] ancho = new float[4] {8f,20f,10f,10f};
            pdf.SetWidths(ancho);

            iTextSharp.text.Font text1 = new iTextSharp.text.Font(bf, 12, iTextSharp.text.Font.NORMAL, BaseColor.WHITE);
            iTextSharp.text.Font text2 = new iTextSharp.text.Font(bf, 10, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);

            // Agregar encabezado
            foreach (DataGridViewColumn columna in dgvProductos.Columns)
            {
                PdfPCell cell = new PdfPCell(new Phrase(columna.HeaderText, text1));
                cell.BackgroundColor = new iTextSharp.text.BaseColor(20, 32, 40);
                cell.BorderWidth = 2;
                pdf.AddCell(cell);
            }
            
            // Informacion de filas
            foreach (DataGridViewRow row in dgvProductos.Rows)
            {
                foreach (DataGridViewCell cell in row.Cells)
                {
                    pdf.AddCell(new Phrase(cell.Value.ToString(),text2));
                }
            }
            // FIN de la tabla

            // Agregar Imagen Informacion del Cliente al PDF
            iTextSharp.text.Image info = iTextSharp.text.Image.GetInstance("Informacion.jpg");
            info.SetAbsolutePosition(50, 120);
            porcentaje = 500 / info.Width;
            info.ScalePercent(porcentaje * 100);

            // Agregar Imagen Final del Cliente al PDF
            iTextSharp.text.Image final = iTextSharp.text.Image.GetInstance("Final.png");
            final.SetAbsolutePosition(50, 50);
            porcentaje = 500 / final.Width;
            final.ScalePercent(porcentaje * 100);

            // Agregar titulos
            cb.BeginText();
            //  bf.FontType()
            cb.SetFontAndSize(bf, 10);
            cb.SetTextMatrix(60, 650);  
            cb.ShowText("Cliente: "+Lbnombre.Text);
            cb.SetTextMatrix(265, 650);
            cb.ShowText("Nit: " + Lbnit.Text);
            cb.SetTextMatrix(425, 650);
            cb.ShowText("Teléfono: " + Lbtelefono.Text);
            cb.SetTextMatrix(410, 670);
            cb.ShowText("Fecha: " + dtfecha1.Value.ToString());
            cb.SetTextMatrix(410, 680);
            cb.ShowText("Cotización No. " + lbCotizacion.Text);

            cb.SetTextMatrix(400, 140);
            cb.ShowText("Atentido por: " + Form1.nombreUsuario);
            cb.SetTextMatrix(440, 500 - (elementos * 20));
            cb.ShowText("TOTAL: Q" + lbTotal.Text);
            cb.EndText();



            // Agregar objetos al PDF

            doc.Add(encabezado);
            doc.Add(persona);
            doc.Add(pdf);
            doc.Add(info);
            doc.Add(final);
            doc.Close();
        }

        private void Btnhistorial_Click(object sender, EventArgs e)
        {
            Form1.acceso = 4;
        }

        private void dgvProductos_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void lbCotizacion_Click(object sender, EventArgs e)
        {

        }
    }
}