﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ProyectosMultiples
{
    public partial class Colaboradores : Form
    {
        public Colaboradores()
        {
            InitializeComponent();
        }
        Conexion conect = new Conexion();//conectar con base de datos

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                string codigo = txtcodigo.Text;
                string nombre = txtnombre.Text;
                string telefono = txttelefono.Text;
                string dpi = txtdpi.Text;
                string direccion = txtdireccion.Text;
                string email = txtemail.Text;
                bool bandera = false;

                if (codigo == "" || nombre == "" || telefono == "" || dpi == "" || direccion == "" || email == "")
                    bandera = false;
                else
                    bandera = true;
                if (bandera)
                {
                    conect.ingresarColaboradores(codigo, nombre, dpi, telefono, direccion, email);
                    conect.actualizarColaboradores(dgvcolaboradores);
                    txtcodigo.Clear();
                    txtnombre.Clear();
                    txtdpi.Clear();
                    txttelefono.Clear();
                    txtdireccion.Clear();
                    txtemail.Clear();
                }
                else
                {
                    FMessageBox obj = new FMessageBox("Se deben ingresar todos los campos que se solicitan.", "ERROR C0:", 1);
                    if (obj.ShowDialog() == DialogResult.OK) ;
                }
            }
            catch(Exception ex)
            {
                FMessageBox obj = new FMessageBox("Error C01: " + ex.Message, "¡Error de Conexión!", 1);
                if (obj.ShowDialog() == DialogResult.OK) ;
            }
        }

        private void Colaboradores_Load(object sender, EventArgs e)
        {
            conect.actualizarColaboradores(dgvcolaboradores);//actualizar datagridview

        }

        private void dgvcolaboradores_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
         
            txtcodigo.Text = dgvcolaboradores.CurrentRow.Cells[0].Value.ToString();
            txtnombre.Text = dgvcolaboradores.CurrentRow.Cells[1].Value.ToString();
            txtdpi.Text = dgvcolaboradores.CurrentRow.Cells[2].Value.ToString();
            txttelefono.Text = dgvcolaboradores.CurrentRow.Cells[3].Value.ToString();
            txtdireccion.Text = dgvcolaboradores.CurrentRow.Cells[4].Value.ToString();
            txtemail.Text = dgvcolaboradores.CurrentRow.Cells[5].Value.ToString();
            btnagregar.Enabled = false;
            btnModificar.Visible = true;

        }

        private void btnModificar_Click(object sender, EventArgs e)
        {
            try
            {
                string codigo=txtcodigo.Text;
                string nombre = txtnombre.Text;
                string dpi=txtdpi.Text;
                string telefono=txttelefono.Text;
                string direccion=txtdireccion.Text;
                string email = txtemail.Text;
                conect.modificarColaboradores(codigo, nombre, dpi, telefono, direccion, email);
                conect.actualizarColaboradores(dgvcolaboradores);
                btnagregar.Enabled = false;
                txtcodigo.Enabled = false;
                txtnombre.Enabled = false;
                txttelefono.Enabled = false;
                txtdpi.Enabled = false;
                txtdireccion.Enabled = false;
                txtemail.Enabled = false;
            }
            catch(Exception ex)
            {
                FMessageBox obj = new FMessageBox("Error C02: " + ex.Message, "¡Error de Conexión!", 1);
                if (obj.ShowDialog() == DialogResult.OK) ;
            }
        }

        private void btneliminar_Click(object sender, EventArgs e)
        {
            try
            {
                string codigo = txtcodigo.Text;
                conect.eliminarColaboradores(codigo);
                conect.actualizarColaboradores(dgvcolaboradores);
                txtcodigo.Clear();
                txtnombre.Clear();
                txtdpi.Clear();
                txttelefono.Clear();
                txtdireccion.Clear();
                txtemail.Clear();
            }
            catch(Exception ex)
            {
                FMessageBox obj = new FMessageBox("Error C03: " + ex.Message, "¡Error de Conexión!", 1);
                if (obj.ShowDialog() == DialogResult.OK) ;
            }
        }
    }
}
