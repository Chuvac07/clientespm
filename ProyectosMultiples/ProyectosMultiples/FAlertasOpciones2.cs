﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ProyectosMultiples
{
    public partial class FAlertasOpciones2 : Form
    {
        public FAlertasOpciones2()
        {
            InitializeComponent();
        }

        static public string no;
        static public string nombre;
        static public string empresa;
        static public string nit;
        static public string telefono;
        static public string fecha;
        static public string descripcion;
        static public string email;
        static public bool estado = true;

        Conexion conect = new Conexion();
        private void FAlertasOpciones2_Load(object sender, EventArgs e)
        {
            if (nombre == empresa)
            {
                lbEmpresa.Visible = false;
                label4.Visible = false;
            }
            else
            {
                lbEmpresa.Visible = true;
                label4.Visible = true;
            }
            lbNo.Text = no;
            lbNombre.Text = nombre;
            lbEmpresa.Text = empresa;
            lbNit.Text = nit;
            lbTelefono.Text = telefono;
            lbFecha.Text = fecha;
            txtDescripcion.Text = descripcion;
            lbEmail.Text = email;
            if (estado)
            {
                cbEstado.Text = cbEstado.Items[0].ToString();
                cbEstado.ForeColor = Color.DarkGreen;
            }
            else
            {
                cbEstado.Text = cbEstado.Items[1].ToString();
                cbEstado.ForeColor = Color.DarkRed;
            }
        }

        private void btnFinalizar_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            Close();
        }

        private void cbEstado_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbEstado.Text == "Activa")
            {
                cbEstado.ForeColor = Color.DarkGreen;
            }
            else
            {
                cbEstado.ForeColor = Color.DarkRed;
            }
        }

        private void btnModificar_Click(object sender, EventArgs e)
        {
            if (cbEstado.Text == "Activa")
            {
                conect.modificarAlertasServicios("1", dtfecha1.Value.ToString(), lbNo.Text);
            }
            else
            {
                conect.modificarAlertasServicios("0", dtfecha1.Value.ToString(), lbNo.Text);
            }
        }
    }
}
