﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;//Libreria para mover ventana formulario

namespace ProyectosMultiples
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            // Se crea el objeto para abrir el FORM inicial
            Inicio obj = new Inicio();
            if (obj.ShowDialog() == DialogResult.OK)
            {
                InitializeComponent();
                Abrirformhijo(new FUsuarios());
                Point direccion = new Point(1,550);
                btnCerrarSesion.Location = direccion;
            }
        }

        private void Btncerrar_Click(object sender, EventArgs e)
        {
            Application.Exit(); // Cerramos formulario
        }

        private void Btnmaximizar_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;// maximizamos formulario
            Btnmaximizar.Visible = false;//ocultamos boton maximizar
            Btnrestaurar.Visible = true;//mostramos boton restaurar
        }

        private void Btnrestaurar_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Normal;// restaruamos tamanio formulario
            Btnrestaurar.Visible = false;//ocultamos boton restaurar
            Btnmaximizar.Visible = true;//mostramos boton maximizar

        }

        private void Btnminimizar_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized; //minimizamos formulario
        }
        //CODIGO PARA CREAR FUNCION QUE MUEVA LA VENTANA DEL FORMULARIO 43-48
        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hWnd, int wMsg, int wParam, int lParam);



        private void Barratitulo_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0); // instancia para mover el formulario de lugar
        }

        private void Panelcontenedor_Paint(object sender, PaintEventArgs e)
        {

        }
        public void Abrirformhijo(object formhijo)
        {
            if (this.Panelcontenedor.Controls.Count > 0)//si hay un control en el panel
                this.Panelcontenedor.Controls.RemoveAt(0);// de ser verdadero lo eliminamos
            Form fh = formhijo as Form;//creamos formulario 
            fh.TopLevel = false;//para definir que es un formulario secundario
            fh.Dock = DockStyle.None;// para que se acople al tamanio del panel
            this.Panelcontenedor.Controls.Add(fh);//agregamos el form al panel
            this.Panelcontenedor.Tag = fh; //establecemos la instancia como contenedor del panel
            fh.Show();
        }

        private void Btnclientes_Click(object sender, EventArgs e)
        {
            Abrirformhijo(new FClientes());
            Panelc.Visible = true;
            Panelco.Visible = false;
            Panelcol.Visible = false;
            Panelh.Visible = false;
            PanelR.Visible = false;
            Panels.Visible = false;
            FCotizaciones.nombreCliente = "";
            FCotizaciones.apellidoCliente = "";
            FCotizaciones.nitCliente = "";
            FCotizaciones.telefonoCliente = "";
            FCotizaciones.emailCliente = "";
            FServicios.nombreCliente = "";
            FServicios.apellidoCliente = "";
            FServicios.nitCliente = "";
            FServicios.telefonoCliente = "";
            FServicios.direccionCliente = "";
    }

        public void button1_Click(object sender, EventArgs e)
        {
            Abrirformhijo(new FCotizaciones());
            Panelc.Visible = false;
            Panelco.Visible = true;
            Panelcol.Visible = false;
            Panelh.Visible = false;
            PanelR.Visible = false;
            Panels.Visible = false;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Abrirformhijo(new Alertas());
            Panelc.Visible = false;
            Panelco.Visible = false;
            Panelcol.Visible = false;
            Panelh.Visible = true;
            PanelR.Visible = false;
            Panels.Visible = false;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Abrirformhijo(new FReportes());
            Panelc.Visible = false;
            Panelco.Visible = false;
            Panelcol.Visible = false;
            Panelh.Visible = false;
            PanelR.Visible = true;
            Panels.Visible = false;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Abrirformhijo(new Colaboradores());
            Panelc.Visible = false;
            Panelco.Visible = false;
            Panelcol.Visible = true;
            Panelh.Visible = false;
            PanelR.Visible = false;
            Panels.Visible = false;
        }

        // Varaibles publicas para habilitar el FORM 1
        public static int acceso = 0;
        public static string nombreUsuario = "";
        public static int idUsuario = 0;
        public static string permiso = ""; 

        private void timer1_Tick_1(object sender, EventArgs e)
        {

            if (acceso == 1)
            {
                //Menuvertical.Enabled = true;
                LbId.Text = Convert.ToString(idUsuario);
                Lbusuario.Text = Convert.ToString(nombreUsuario);
                lbPermiso.Text = permiso;
                pictureBox2.Visible = true;
                Lbusuario.Visible = true;
                if (permiso == "Alto")
                {
                    Btnclientes.Visible = true;
                    panel1.Visible = true;
                    Btncotizaciones.Visible = true;
                    panel2.Visible = true;
                    btnservicios.Visible = true;
                    panel8.Visible = true;
                    Btnhistorial.Visible = true;
                    panel3.Visible = true;
                    btnreportes.Visible = true;
                    panel4.Visible = true;
                    Btncolaboradores.Visible = true;
                    panel5.Visible = true; 
                    button2.Visible = true;
                    btnCerrarSesion.Visible = true;
                }
                if (permiso == "Medio")
                {
                    Btnclientes.Visible = true;
                    panel1.Visible = true;
                    Btncotizaciones.Visible = true;
                    panel2.Visible = true;
                    btnservicios.Visible = true;
                    panel8.Visible = true;
                    Btnhistorial.Visible = true;
                    panel3.Visible = true;
                    btnCerrarSesion.Visible = true;
                }
                if (permiso == "Bajo")
                {
                    Btnclientes.Visible = true;
                    panel1.Visible = true;
                    Btncotizaciones.Visible = true;
                    panel2.Visible = true;
                    btnCerrarSesion.Visible = true;
                }
            }
            if (acceso == 2)
            {
                button1_Click(sender,e);
            }
            if (acceso == 3)
            {
                Btnclientes_Click(sender,e);    
            }
            if(acceso==4)
            {
                Abrirformhijo(new FHistorial());

            }
            if(acceso==5)
            {
                Abrirformhijo(new FCotizaciones());

            }
            if (acceso == 6)
            {
                button1_Click_1(sender, e);

            }
            acceso = 0;
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            Abrirformhijo(new FServicios());
            Panelc.Visible = false;
            Panelco.Visible = false;
            Panelcol.Visible = false;
            Panelh.Visible = false;
            PanelR.Visible = false;
            Panels.Visible = true;
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            FCrearUsuario obj = new FCrearUsuario();
            obj.Show();
        }

        private void btnCerrarSesion_Click(object sender, EventArgs e)
        {
            Lbusuario.Visible = false;
            pictureBox2.Visible = false;
            Btnclientes.Visible = false;
            panel1.Visible = false;
            Btncotizaciones.Visible = false;
            panel2.Visible = false;
            btnservicios.Visible = false;
            panel8.Visible = false;
            Btnhistorial.Visible = false;
            panel3.Visible = false;
            btnreportes.Visible = false;
            panel4.Visible = false;
            Btncolaboradores.Visible = false;
            panel5.Visible = false;
            button2.Visible = false;
            btnCerrarSesion.Visible = false;

            Abrirformhijo(new FUsuarios());
        }

        private void button1_Click_2(object sender, EventArgs e)
        {
            About obj = new About();
            obj.Show();
        }
    }
}
