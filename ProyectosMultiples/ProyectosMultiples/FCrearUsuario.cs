﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ProyectosMultiples
{
    public partial class FCrearUsuario : Form
    {
        public FCrearUsuario()
        {
            InitializeComponent();
        }
        Conexion conect = new Conexion();//conectar con base de datos

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {

        }

        private void comboBoxpermiso_SelectedIndexChanged(object sender, EventArgs e)
        {
        }

        private void comboBoxpermiso_SelectedIndexChanged_1(object sender, EventArgs e)
        {
          
        }

        private void btncrearusuario_Click(object sender, EventArgs e)
        {
            string nombre = txtnombre.Text;
            string dpi = txtdpi.Text;
            string telefono = txttelefono.Text;
            string direccion = txtdireccion.Text;
            string email = txtemail.Text;
            string usuario = txtusuario.Text;
            string contra = txtpass.Text;
            string permiso = txtpermiso.Text;
            conect.ingresarUsuariosSistema(nombre, dpi, telefono, direccion, email, permiso, usuario, contra);
            conect.actualizarUsuariosSistema(dgvusuariossistema);
            dgvusuariossistema.Columns[2].Visible = false;
            dgvusuariossistema.Columns[4].Visible = false;
            dgvusuariossistema.Columns[5].Visible = false;
            txtnombre.Clear();
            txtdpi.Clear();
            txttelefono.Clear();
            txtdireccion.Clear();
            txtemail.Clear();
            txtusuario.Clear();
            txtpass.Clear();


        }

        private void txtpermiso_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (txtpermiso.Text == "Permiso Total")
            {
                lbpermisos.Text = "Clientes, Cotizaciones y ventas, Servicios, Alertas, Reportes, Colaboradores";
            }
            if (txtpermiso.Text == "Permiso Medio")
            {
                lbpermisos.Text = "Clientes, Cotizaciones y ventas, Servicios, Alertas, Colaboradores";
            }
            if (txtpermiso.Text == "Permiso Basico")
            {
                lbpermisos.Text = "Clientes, Cotizaciones y ventas, Alertas";
            }
        }

        private void FCrearUsuario_Load(object sender, EventArgs e)
        {
            conect.actualizarUsuariosSistema(dgvusuariossistema);//actualizar datagridview
            dgvusuariossistema.Columns[2].Visible = false;
            dgvusuariossistema.Columns[4].Visible = false;
            dgvusuariossistema.Columns[5].Visible = false;
        }

        private void btnModificar_Click(object sender, EventArgs e)
        {
            try
            {
                string nombre = txtnombre.Text;
                string dpi = txtdpi.Text;
                string telefono = txttelefono.Text;
                string direccion = txtdireccion.Text;
                string email = txtdireccion.Text;
                string permiso = txtpermiso.Text;
                string usuario = txtusuario.Text;
                string contra = txtpass.Text;
                conect.modificarUsuariossistema(nombre, dpi, telefono, direccion, email,permiso,usuario,contra);
                conect.actualizarUsuariosSistema(dgvusuariossistema);
                dgvusuariossistema.Columns[2].Visible = false;
                dgvusuariossistema.Columns[4].Visible = false;
                dgvusuariossistema.Columns[5].Visible = false;
                btncrear.Enabled = false;
                txtnombre.Enabled = false;
                txttelefono.Enabled = false;
                txtdpi.Enabled = false;
                txtdireccion.Enabled = false;
                txtemail.Enabled = false;
                txtpermiso.Enabled = false;
                txtusuario.Enabled = false;
                txtpass.Enabled = false;
            }
            catch (Exception ex)
            {
                FMessageBox obj = new FMessageBox("Error CU1: " + ex.Message, "¡Error de Conexión!", 1);
                if (obj.ShowDialog() == DialogResult.OK) ;
            }
        }

        private void dgvusuariossistema_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            txtnombre.Text = dgvusuariossistema.CurrentRow.Cells[1].Value.ToString();
            txtdpi.Text = dgvusuariossistema.CurrentRow.Cells[2].Value.ToString();
            txttelefono.Text = dgvusuariossistema.CurrentRow.Cells[3].Value.ToString();
            txtdireccion.Text = dgvusuariossistema.CurrentRow.Cells[4].Value.ToString();
            txtemail.Text = dgvusuariossistema.CurrentRow.Cells[5].Value.ToString();
            txtpermiso.Text = dgvusuariossistema.CurrentRow.Cells[6].Value.ToString();
            txtusuario.Text = dgvusuariossistema.CurrentRow.Cells[7].Value.ToString();
            txtpass.Text = dgvusuariossistema.CurrentRow.Cells[8].Value.ToString();

            btncrear.Enabled = false;
            btnModificar.Visible = true;
        }

        private void btneliminar_Click(object sender, EventArgs e)
        {
            try
            {
                string usuario = txtusuario.Text;
                conect.eliminarusuariossistema(usuario);
                conect.actualizarUsuariosSistema(dgvusuariossistema);
                dgvusuariossistema.Columns[2].Visible = false;
                dgvusuariossistema.Columns[4].Visible = false;
                dgvusuariossistema.Columns[5].Visible = false;
                txtnombre.Clear();
                txtdpi.Clear();
                txttelefono.Clear();
                txtdireccion.Clear();
                txtemail.Clear();
                txtusuario.Clear();
                txtpass.Clear();
               
            }
            catch (Exception ex)
            {
                FMessageBox obj = new FMessageBox("Error CU2: " + ex.Message, "¡Error de Conexión!", 1);
                if (obj.ShowDialog() == DialogResult.OK) ;
            }
        }
    }
}
