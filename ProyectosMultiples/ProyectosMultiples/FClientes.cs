﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace ProyectosMultiples
{
    public partial class FClientes : Form
    {
        public FClientes()
        {
            InitializeComponent();
        }
        Conexion conect = new Conexion();
        // Ingresar clientes a la Base de Datos.
        private void btnAgregar_Click_1(object sender, EventArgs e)
        {
            try
            {
                string nit = txtNit.Text;
                string nombre = txtNombre.Text;
                string empresa = txtEmpresa.Text;
                string telefono = txtTelefono.Text;
                string direccion = txtDireccion.Text;
                string email = txtEmail.Text;
                bool bandera1 = false;
                bool bandera2 = false; 

                if (rBEmpresa.Checked == true && nit != "" && nombre != "" && empresa != "" && telefono != "" && direccion != "")
                   bandera1 = true;
                else
                   bandera1 = false; 
                
                if (rbCliente.Checked == true && nit != "" && nombre != "" && telefono != "" && direccion != "")
                    bandera2 = true;
                else
                    bandera2 = false; 

                if (bandera1 || bandera2)
                {

                    if (rbCliente.Checked == true)
                    {
                        conect.ingresarClientes(nit, nombre, nombre, "Cliente", telefono, direccion, email);
                        conect.actualizarClientes(dgvClientes, "Cliente");
                    }
                    if (rBEmpresa.Checked == true)
                    {
                        conect.ingresarClientes(nit, nombre, empresa, "Empresa", telefono, direccion, email);
                        conect.actualizarClientes(dgvClientes, "Empresa");
                    }

                    txtNombre.Enabled = false;
                    txtEmpresa.Enabled = false;
                    txtNit.Enabled = false;
                    txtTelefono.Enabled = false;
                    txtDireccion.Enabled = false;
                    txtEmail.Enabled = false;

                    btnservicios.Visible = true;
                    btnCotizacion.Visible = true;
                    btnAgregar.Visible = false;
                    txtNit.Enabled = false;
                    Point direccion1 = new Point(320, 145);
                    btnservicios.Location = direccion1;
                    Point direccion2 = new Point(520, 145);
                    btnCotizacion.Location = direccion2;
                }
                else
                {
                    FMessageBox obj = new FMessageBox("Se deben ingresar todos los campos que se solicitan.", "ERROR CLI:",1);
                    if (obj.ShowDialog() == DialogResult.OK) ;
                }
            }
            catch (Exception ex)
            {
                FMessageBox obj = new FMessageBox("Error CL1: " + ex.Message, "¡Error de Conexión!", 1);
                if (obj.ShowDialog() == DialogResult.OK) ;
            }
        }

        private void FClientes_Load(object sender, EventArgs e)
        {
            try
            {
                conect.actualizarClientes(dgvC1, "TODO");
                lbClientes.Text = Convert.ToString(dgvC1.Rows.Count + 1);
                this.ActiveControl = txtNit;
                rBEmpresa.Checked = true;
                if (Form1.permiso == "Bajo")
                {
                    btnservicios.Visible = false; 
                }
            }
            catch (Exception ex)
            {
                FMessageBox obj = new FMessageBox("Error CL2: " + ex.Message, "¡Error de Conexión!", 1);
                if (obj.ShowDialog() == DialogResult.OK) ;
            }
        }

        private void dgvClientes_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            btnservicios.Visible = true;
            btnCotizacion.Visible = true;
            btnAgregar.Visible = false;

            txtNit.Enabled = false;

            Point direccion1 = new Point(320, 145);
            btnservicios.Location = direccion1;
            Point direccion2 = new Point(520, 145);
            btnCotizacion.Location = direccion2;

            lbClientes.Text = dgvClientes.CurrentRow.Cells[0].Value.ToString();
            txtEmpresa.Text = dgvClientes.CurrentRow.Cells[1].Value.ToString();
            txtNombre.Text = dgvClientes.CurrentRow.Cells[2].Value.ToString();
            txtNit.Text = dgvClientes.CurrentRow.Cells[3].Value.ToString();
            txtTelefono.Text = dgvClientes.CurrentRow.Cells[5].Value.ToString();
            txtDireccion.Text = this.dgvClientes.CurrentRow.Cells[6].Value.ToString();
            txtEmail.Text = dgvClientes.CurrentRow.Cells[7].Value.ToString();
            btnModificar.Visible = true;
            btnAgregar.Visible = false;
            if (Form1.permiso == "Bajo")
            {
                btnservicios.Visible = false;
            }
        }

        private void txtApellido_KeyUp_1(object sender, KeyEventArgs e)
        {
        }

        private void dgvClientes_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {

        }

        private void btnModificar_Click(object sender, EventArgs e)
        {
            try
            {
                string nit = txtNit.Text;
                string nombre = txtNombre.Text;
                string apellido = txtEmpresa.Text;
                string telefono = txtTelefono.Text;
                string direccion = txtDireccion.Text;
                string email = txtEmail.Text;

                if (rbCliente.Checked == true)
                {
                    conect.modificarClientes(lbClientes.Text,nit, nombre, nombre, telefono, direccion, email);
                    conect.actualizarClientes(dgvClientes, "Cliente");
                }
                if (rBEmpresa.Checked == true)
                {
                    conect.modificarClientes(lbClientes.Text, nit, nombre, apellido, telefono, direccion, email);
                    conect.actualizarClientes(dgvClientes, "Empresa");
                }
                btnAgregar.Enabled = false;
                txtNombre.Enabled = false;
                txtEmpresa.Enabled = false;
                txtNit.Enabled = false;
                txtTelefono.Enabled = false;
                txtDireccion.Enabled = false;
                txtEmail.Enabled = false;
            }
            catch (Exception ex)
            {
                FMessageBox obj = new FMessageBox("Error CL3: " + ex.Message, "¡Error de Conexión!", 1);
                if (obj.ShowDialog() == DialogResult.OK) ;
            }
        }

        private void btnCotizacion_Click(object sender, EventArgs e)
        {
            if (txtNombre.Text == "" && txtEmpresa.Text == "" && txtNit.Text == "" && txtTelefono.Text == "" && txtDireccion.Text == "")
                MessageBox.Show("Porfavor debe llenar todos los campos para realizar la cotización.", "Error de Cotizaciones");
            else
            {
                if (rbCliente.Checked == true)
                {
                    FCotizaciones.nombreCliente = txtNombre.Text;
                    FCotizaciones.apellidoCliente = txtNombre.Text;
                }
                if (rBEmpresa.Checked == true)
                {
                    FCotizaciones.nombreCliente = txtNombre.Text;
                    FCotizaciones.apellidoCliente = txtEmpresa.Text;
                }
                FCotizaciones.idCliente = lbClientes.Text;
                FCotizaciones.nitCliente = txtNit.Text;
                FCotizaciones.telefonoCliente = txtTelefono.Text;
                FCotizaciones.emailCliente = txtEmail.Text;
                Form1.acceso = 2;
            }
        }
        
        private void rbCliente_CheckedChanged(object sender, EventArgs e)
        {
            txtNit.Enabled = true;
            conect.actualizarClientes(dgvClientes, "Cliente");
            dgvClientes.Columns[1].Visible = false;
            dgvClientes.Columns[4].Visible = false;

            lbEmpresa.Visible = false;
            txtEmpresa.Visible = false;
            groupBox1.Text = "Datos del Cliente";
            groupBox1.Visible = true;
            dgvClientes.Visible = true;
            txtNombre.Clear();
            txtEmpresa.Clear();
            txtTelefono.Clear();
            txtDireccion.Clear();
            txtNit.Clear();
            txtEmail.Clear();
            this.ActiveControl = txtNit;
        }
        private void rBEmpresa_CheckedChanged(object sender, EventArgs e)
        {
            txtNit.Enabled = true;
            conect.actualizarClientes(dgvClientes, "Empresa");
            dgvClientes.Columns[1].Visible = true;
            dgvClientes.Columns[4].Visible = false;

            lbEmpresa.Visible = true;
            txtEmpresa.Visible = true;
            btnAgregar.Visible = true; 
            Point direccion = new Point(593, 145);
            btnAgregar.Location = direccion;
            groupBox1.Text = "Datos de la Empresa";
            groupBox1.Visible = true;
            dgvClientes.Visible = true;
            txtNombre.Clear();
            txtEmpresa.Clear();
            txtTelefono.Clear();
            txtDireccion.Clear();
            txtNit.Clear();
            txtEmail.Clear();
            this.ActiveControl = txtNit;
        }

        private void txtNombre_KeyUp(object sender, KeyEventArgs e)
        {
            if (rbCliente.Checked == true)
            {
                conect.busqueda(dgvClientes, txtNit, "nombre_clientespm", "Cliente");
            }
            if (rBEmpresa.Checked == true)
            {
                conect.busqueda(dgvClientes, txtNit, "nombre_clientespm", "Empresa");
            }
        }

        private void dgvClientes_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void btnservicios_Click(object sender, EventArgs e)
        {
            if (txtNombre.Text == "" && txtEmpresa.Text == "" && txtNit.Text == "" && txtTelefono.Text == "" && txtDireccion.Text == "")
                MessageBox.Show("Porfavor debe llenar todos los campos para realizar el servicio.", "Error de Servicio");
            else
            {
                if (rbCliente.Checked == true)
                {
                    FServicios.nombreCliente = txtNombre.Text;
                    FServicios.apellidoCliente = txtNombre.Text;
                }
                if (rBEmpresa.Checked == true)
                {
                    FServicios.nombreCliente = txtNombre.Text;
                    FServicios.apellidoCliente = txtEmpresa.Text;
                }
                FServicios.idCliente = lbClientes.Text;
                FServicios.nitCliente = txtNit.Text;
                FServicios.telefonoCliente = txtTelefono.Text;
                FServicios.direccionCliente = txtDireccion.Text;
                Form1.acceso = 6;
            }
        }

        private void txtNit_KeyUp(object sender, KeyEventArgs e)
        {
            if (rbCliente.Checked == true)
            {
                conect.busqueda(dgvClientes, txtNit, "nit_clientespm", "Cliente");
            }
            if (rBEmpresa.Checked == true)
            {
                conect.busqueda(dgvClientes, txtNit, "nit_clientespm", "Empresa");
            }
        }
    }
}
