﻿namespace ProyectosMultiples
{
    partial class FServicios
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            this.groupcliente = new System.Windows.Forms.GroupBox();
            this.btnNuevoCliente = new System.Windows.Forms.Button();
            this.txtdireccion = new System.Windows.Forms.Label();
            this.txttelefono = new System.Windows.Forms.Label();
            this.txtnit = new System.Windows.Forms.Label();
            this.txtapellido = new System.Windows.Forms.Label();
            this.txtnombre = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.dtfecha = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupcolaborador = new System.Windows.Forms.GroupBox();
            this.txtdescripcion = new System.Windows.Forms.TextBox();
            this.txtfinal = new System.Windows.Forms.TextBox();
            this.txtinicio = new System.Windows.Forms.TextBox();
            this.txttipo = new System.Windows.Forms.ComboBox();
            this.txtid = new System.Windows.Forms.ComboBox();
            this.txtcorrelativo = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.groupalerta = new System.Windows.Forms.GroupBox();
            this.txtdescripcionalerta = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.dtfechaalerta = new System.Windows.Forms.DateTimePicker();
            this.label28 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.btnguardar = new System.Windows.Forms.Button();
            this.btncargarfotos = new System.Windows.Forms.Button();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label6 = new System.Windows.Forms.Label();
            this.btnbuscar = new System.Windows.Forms.Button();
            this.btnatras = new System.Windows.Forms.Button();
            this.groupbuscar = new System.Windows.Forms.GroupBox();
            this.dgvservicios = new System.Windows.Forms.DataGridView();
            this.combotipo = new System.Windows.Forms.ComboBox();
            this.txtidcolaborador = new System.Windows.Forms.TextBox();
            this.lbcolaborador = new System.Windows.Forms.Label();
            this.combobusqueda = new System.Windows.Forms.ComboBox();
            this.txtnombrec = new System.Windows.Forms.TextBox();
            this.lbapellidoc = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.groupservicio = new System.Windows.Forms.GroupBox();
            this.lbcorrelativo = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.txtmostrardescripcion = new System.Windows.Forms.TextBox();
            this.lbfecha = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lbfechafinal = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.lbdireccioncliente = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.lbtelefonocliente = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.lbnombrecliente = new System.Windows.Forms.Label();
            this.lbapellidocliente = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.lbfechainicio = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.lbtipodeservicio = new System.Windows.Forms.Label();
            this.lbidcolaborador = new System.Windows.Forms.Label();
            this.lbnombrecolaborador = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.dgvcolaboradores = new System.Windows.Forms.DataGridView();
            this.label5 = new System.Windows.Forms.Label();
            this.lbnoservicio = new System.Windows.Forms.Label();
            this.groupcliente.SuspendLayout();
            this.groupcolaborador.SuspendLayout();
            this.groupalerta.SuspendLayout();
            this.groupbuscar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvservicios)).BeginInit();
            this.groupservicio.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvcolaboradores)).BeginInit();
            this.SuspendLayout();
            // 
            // groupcliente
            // 
            this.groupcliente.Controls.Add(this.btnNuevoCliente);
            this.groupcliente.Controls.Add(this.txtdireccion);
            this.groupcliente.Controls.Add(this.txttelefono);
            this.groupcliente.Controls.Add(this.txtnit);
            this.groupcliente.Controls.Add(this.txtapellido);
            this.groupcliente.Controls.Add(this.txtnombre);
            this.groupcliente.Controls.Add(this.label21);
            this.groupcliente.Controls.Add(this.label20);
            this.groupcliente.Controls.Add(this.label19);
            this.groupcliente.Controls.Add(this.dtfecha);
            this.groupcliente.Controls.Add(this.label3);
            this.groupcliente.Controls.Add(this.label2);
            this.groupcliente.Controls.Add(this.label1);
            this.groupcliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupcliente.ForeColor = System.Drawing.Color.White;
            this.groupcliente.Location = new System.Drawing.Point(26, 67);
            this.groupcliente.Name = "groupcliente";
            this.groupcliente.Size = new System.Drawing.Size(662, 162);
            this.groupcliente.TabIndex = 0;
            this.groupcliente.TabStop = false;
            this.groupcliente.Text = "Cliente";
            // 
            // btnNuevoCliente
            // 
            this.btnNuevoCliente.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(66)))), ((int)(((byte)(82)))));
            this.btnNuevoCliente.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(175)))), ((int)(((byte)(75)))));
            this.btnNuevoCliente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNuevoCliente.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnNuevoCliente.Image = global::ProyectosMultiples.Properties.Resources.clientes;
            this.btnNuevoCliente.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnNuevoCliente.Location = new System.Drawing.Point(480, 57);
            this.btnNuevoCliente.Name = "btnNuevoCliente";
            this.btnNuevoCliente.Size = new System.Drawing.Size(109, 72);
            this.btnNuevoCliente.TabIndex = 12;
            this.btnNuevoCliente.Text = "Agregar Nuevo Cliente";
            this.btnNuevoCliente.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnNuevoCliente.UseVisualStyleBackColor = false;
            this.btnNuevoCliente.Click += new System.EventHandler(this.btnNuevoCliente_Click);
            // 
            // txtdireccion
            // 
            this.txtdireccion.AutoSize = true;
            this.txtdireccion.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtdireccion.ForeColor = System.Drawing.Color.Turquoise;
            this.txtdireccion.Location = new System.Drawing.Point(110, 92);
            this.txtdireccion.Name = "txtdireccion";
            this.txtdireccion.Size = new System.Drawing.Size(11, 16);
            this.txtdireccion.TabIndex = 16;
            this.txtdireccion.Text = ".";
            // 
            // txttelefono
            // 
            this.txttelefono.AutoSize = true;
            this.txttelefono.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txttelefono.ForeColor = System.Drawing.Color.Turquoise;
            this.txttelefono.Location = new System.Drawing.Point(361, 58);
            this.txttelefono.Name = "txttelefono";
            this.txttelefono.Size = new System.Drawing.Size(11, 16);
            this.txttelefono.TabIndex = 15;
            this.txttelefono.Text = ".";
            // 
            // txtnit
            // 
            this.txtnit.AutoSize = true;
            this.txtnit.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtnit.ForeColor = System.Drawing.Color.Turquoise;
            this.txtnit.Location = new System.Drawing.Point(110, 59);
            this.txtnit.Name = "txtnit";
            this.txtnit.Size = new System.Drawing.Size(11, 16);
            this.txtnit.TabIndex = 14;
            this.txtnit.Text = ".";
            // 
            // txtapellido
            // 
            this.txtapellido.AutoSize = true;
            this.txtapellido.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtapellido.ForeColor = System.Drawing.Color.Turquoise;
            this.txtapellido.Location = new System.Drawing.Point(110, 27);
            this.txtapellido.Name = "txtapellido";
            this.txtapellido.Size = new System.Drawing.Size(11, 16);
            this.txtapellido.TabIndex = 13;
            this.txtapellido.Text = ".";
            // 
            // txtnombre
            // 
            this.txtnombre.AutoSize = true;
            this.txtnombre.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtnombre.ForeColor = System.Drawing.Color.Turquoise;
            this.txtnombre.Location = new System.Drawing.Point(394, 27);
            this.txtnombre.Name = "txtnombre";
            this.txtnombre.Size = new System.Drawing.Size(11, 16);
            this.txtnombre.TabIndex = 12;
            this.txtnombre.Text = ".";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.White;
            this.label21.Location = new System.Drawing.Point(22, 122);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(59, 18);
            this.label21.TabIndex = 11;
            this.label21.Text = "Fecha:";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.White;
            this.label20.Location = new System.Drawing.Point(22, 89);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(85, 18);
            this.label20.TabIndex = 9;
            this.label20.Text = "Dirección:";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.White;
            this.label19.Location = new System.Drawing.Point(266, 57);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(79, 18);
            this.label19.TabIndex = 7;
            this.label19.Text = "Teléfono:";
            // 
            // dtfecha
            // 
            this.dtfecha.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtfecha.Location = new System.Drawing.Point(112, 122);
            this.dtfecha.Name = "dtfecha";
            this.dtfecha.Size = new System.Drawing.Size(102, 21);
            this.dtfecha.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(22, 57);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(39, 18);
            this.label3.TabIndex = 2;
            this.label3.Text = "NIT:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(264, 24);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(122, 18);
            this.label2.TabIndex = 1;
            this.label2.Text = "Representante:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(22, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 18);
            this.label1.TabIndex = 0;
            this.label1.Text = "Cliente:";
            // 
            // groupcolaborador
            // 
            this.groupcolaborador.Controls.Add(this.txtdescripcion);
            this.groupcolaborador.Controls.Add(this.txtfinal);
            this.groupcolaborador.Controls.Add(this.txtinicio);
            this.groupcolaborador.Controls.Add(this.txttipo);
            this.groupcolaborador.Controls.Add(this.txtid);
            this.groupcolaborador.Controls.Add(this.txtcorrelativo);
            this.groupcolaborador.Controls.Add(this.label26);
            this.groupcolaborador.Controls.Add(this.groupalerta);
            this.groupcolaborador.Controls.Add(this.button1);
            this.groupcolaborador.Controls.Add(this.btnguardar);
            this.groupcolaborador.Controls.Add(this.btncargarfotos);
            this.groupcolaborador.Controls.Add(this.label23);
            this.groupcolaborador.Controls.Add(this.label22);
            this.groupcolaborador.Controls.Add(this.label9);
            this.groupcolaborador.Controls.Add(this.label8);
            this.groupcolaborador.Controls.Add(this.label7);
            this.groupcolaborador.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupcolaborador.ForeColor = System.Drawing.Color.White;
            this.groupcolaborador.Location = new System.Drawing.Point(26, 234);
            this.groupcolaborador.Name = "groupcolaborador";
            this.groupcolaborador.Size = new System.Drawing.Size(662, 474);
            this.groupcolaborador.TabIndex = 1;
            this.groupcolaborador.TabStop = false;
            this.groupcolaborador.Text = "Colaborador";
            this.groupcolaborador.Visible = false;
            this.groupcolaborador.Enter += new System.EventHandler(this.groupcolaborador_Enter);
            // 
            // txtdescripcion
            // 
            this.txtdescripcion.Location = new System.Drawing.Point(167, 195);
            this.txtdescripcion.Multiline = true;
            this.txtdescripcion.Name = "txtdescripcion";
            this.txtdescripcion.Size = new System.Drawing.Size(430, 103);
            this.txtdescripcion.TabIndex = 38;
            // 
            // txtfinal
            // 
            this.txtfinal.Location = new System.Drawing.Point(234, 158);
            this.txtfinal.Name = "txtfinal";
            this.txtfinal.Size = new System.Drawing.Size(189, 21);
            this.txtfinal.TabIndex = 37;
            // 
            // txtinicio
            // 
            this.txtinicio.Location = new System.Drawing.Point(234, 123);
            this.txtinicio.Name = "txtinicio";
            this.txtinicio.Size = new System.Drawing.Size(189, 21);
            this.txtinicio.TabIndex = 36;
            // 
            // txttipo
            // 
            this.txttipo.FormattingEnabled = true;
            this.txttipo.Items.AddRange(new object[] {
            "Instalación",
            "Mantenimiento Preventivo",
            "Mantenimiento Correctivo",
            "Garantía",
            "Otros"});
            this.txttipo.Location = new System.Drawing.Point(234, 87);
            this.txttipo.Name = "txttipo";
            this.txttipo.Size = new System.Drawing.Size(187, 23);
            this.txttipo.TabIndex = 35;
            this.txttipo.SelectedIndexChanged += new System.EventHandler(this.txttipo_SelectedIndexChanged);
            // 
            // txtid
            // 
            this.txtid.FormattingEnabled = true;
            this.txtid.Location = new System.Drawing.Point(234, 51);
            this.txtid.Name = "txtid";
            this.txtid.Size = new System.Drawing.Size(187, 23);
            this.txtid.TabIndex = 34;
            // 
            // txtcorrelativo
            // 
            this.txtcorrelativo.Location = new System.Drawing.Point(232, 17);
            this.txtcorrelativo.Name = "txtcorrelativo";
            this.txtcorrelativo.Size = new System.Drawing.Size(189, 21);
            this.txtcorrelativo.TabIndex = 33;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.White;
            this.label26.Location = new System.Drawing.Point(56, 17);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(96, 18);
            this.label26.TabIndex = 32;
            this.label26.Text = "Correlativo:";
            // 
            // groupalerta
            // 
            this.groupalerta.Controls.Add(this.txtdescripcionalerta);
            this.groupalerta.Controls.Add(this.label29);
            this.groupalerta.Controls.Add(this.dtfechaalerta);
            this.groupalerta.Controls.Add(this.label28);
            this.groupalerta.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupalerta.ForeColor = System.Drawing.Color.White;
            this.groupalerta.Location = new System.Drawing.Point(165, 299);
            this.groupalerta.Name = "groupalerta";
            this.groupalerta.Size = new System.Drawing.Size(309, 164);
            this.groupalerta.TabIndex = 30;
            this.groupalerta.TabStop = false;
            this.groupalerta.Text = "Alerta";
            this.groupalerta.Visible = false;
            // 
            // txtdescripcionalerta
            // 
            this.txtdescripcionalerta.Location = new System.Drawing.Point(17, 75);
            this.txtdescripcionalerta.Multiline = true;
            this.txtdescripcionalerta.Name = "txtdescripcionalerta";
            this.txtdescripcionalerta.Size = new System.Drawing.Size(275, 80);
            this.txtdescripcionalerta.TabIndex = 27;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(112, 58);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(78, 13);
            this.label29.TabIndex = 2;
            this.label29.Text = "Descripcion:";
            // 
            // dtfechaalerta
            // 
            this.dtfechaalerta.CalendarFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtfechaalerta.Location = new System.Drawing.Point(17, 33);
            this.dtfechaalerta.Name = "dtfechaalerta";
            this.dtfechaalerta.Size = new System.Drawing.Size(275, 22);
            this.dtfechaalerta.TabIndex = 1;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(112, 15);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(83, 13);
            this.label28.TabIndex = 0;
            this.label28.Text = "Fecha Alerta:";
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(66)))), ((int)(((byte)(82)))));
            this.button1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(175)))), ((int)(((byte)(75)))));
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Image = global::ProyectosMultiples.Properties.Resources.alertas;
            this.button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button1.Location = new System.Drawing.Point(60, 308);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(86, 42);
            this.button1.TabIndex = 29;
            this.button1.Text = "Alerta";
            this.button1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnguardar
            // 
            this.btnguardar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(66)))), ((int)(((byte)(82)))));
            this.btnguardar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(175)))), ((int)(((byte)(75)))));
            this.btnguardar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnguardar.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnguardar.ForeColor = System.Drawing.Color.White;
            this.btnguardar.Image = global::ProyectosMultiples.Properties.Resources.p2;
            this.btnguardar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnguardar.Location = new System.Drawing.Point(480, 308);
            this.btnguardar.Name = "btnguardar";
            this.btnguardar.Size = new System.Drawing.Size(116, 42);
            this.btnguardar.TabIndex = 28;
            this.btnguardar.Text = "Guardar";
            this.btnguardar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnguardar.UseVisualStyleBackColor = false;
            this.btnguardar.Click += new System.EventHandler(this.btnguardar_Click);
            // 
            // btncargarfotos
            // 
            this.btncargarfotos.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(66)))), ((int)(((byte)(82)))));
            this.btncargarfotos.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(80)))), ((int)(((byte)(200)))));
            this.btncargarfotos.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btncargarfotos.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btncargarfotos.Image = global::ProyectosMultiples.Properties.Resources.P1;
            this.btncargarfotos.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btncargarfotos.Location = new System.Drawing.Point(479, 29);
            this.btncargarfotos.Name = "btncargarfotos";
            this.btncargarfotos.Size = new System.Drawing.Size(117, 75);
            this.btncargarfotos.TabIndex = 27;
            this.btncargarfotos.Text = "Cargar Fotos ";
            this.btncargarfotos.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btncargarfotos.UseVisualStyleBackColor = false;
            this.btncargarfotos.Click += new System.EventHandler(this.btncargarfotos_Click);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.White;
            this.label23.Location = new System.Drawing.Point(56, 159);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(100, 18);
            this.label23.TabIndex = 23;
            this.label23.Text = "Fecha Final:";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.White;
            this.label22.Location = new System.Drawing.Point(57, 122);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(100, 18);
            this.label22.TabIndex = 22;
            this.label22.Text = "Fecha inico:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.White;
            this.label9.Location = new System.Drawing.Point(57, 54);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(128, 18);
            this.label9.TabIndex = 14;
            this.label9.Text = "ID Colaborador:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(56, 192);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(103, 18);
            this.label8.TabIndex = 11;
            this.label8.Text = "Descripción ";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(56, 90);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(46, 18);
            this.label7.TabIndex = 9;
            this.label7.Text = "Tipo:";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.panel1.Location = new System.Drawing.Point(713, 1);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(10, 731);
            this.panel1.TabIndex = 15;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.panel2.Location = new System.Drawing.Point(0, 724);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(720, 10);
            this.panel2.TabIndex = 14;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.panel3.Location = new System.Drawing.Point(0, 1);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(720, 10);
            this.panel3.TabIndex = 15;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(45, 22);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(296, 25);
            this.label6.TabIndex = 16;
            this.label6.Text = "HISTORIAL DE SERVICIOS";
            // 
            // btnbuscar
            // 
            this.btnbuscar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(66)))), ((int)(((byte)(82)))));
            this.btnbuscar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(80)))), ((int)(((byte)(200)))));
            this.btnbuscar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnbuscar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnbuscar.ForeColor = System.Drawing.Color.White;
            this.btnbuscar.Location = new System.Drawing.Point(528, 19);
            this.btnbuscar.Name = "btnbuscar";
            this.btnbuscar.Size = new System.Drawing.Size(94, 36);
            this.btnbuscar.TabIndex = 17;
            this.btnbuscar.Text = "Buscar";
            this.btnbuscar.UseVisualStyleBackColor = false;
            this.btnbuscar.Click += new System.EventHandler(this.button3_Click);
            // 
            // btnatras
            // 
            this.btnatras.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(66)))), ((int)(((byte)(82)))));
            this.btnatras.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(80)))), ((int)(((byte)(200)))));
            this.btnatras.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnatras.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnatras.ForeColor = System.Drawing.Color.White;
            this.btnatras.Location = new System.Drawing.Point(528, 19);
            this.btnatras.Name = "btnatras";
            this.btnatras.Size = new System.Drawing.Size(94, 36);
            this.btnatras.TabIndex = 18;
            this.btnatras.Text = "Atras";
            this.btnatras.UseVisualStyleBackColor = false;
            this.btnatras.Visible = false;
            this.btnatras.Click += new System.EventHandler(this.btnatras_Click);
            // 
            // groupbuscar
            // 
            this.groupbuscar.Controls.Add(this.dgvservicios);
            this.groupbuscar.Controls.Add(this.combotipo);
            this.groupbuscar.Controls.Add(this.txtidcolaborador);
            this.groupbuscar.Controls.Add(this.lbcolaborador);
            this.groupbuscar.Controls.Add(this.combobusqueda);
            this.groupbuscar.Controls.Add(this.txtnombrec);
            this.groupbuscar.Controls.Add(this.lbapellidoc);
            this.groupbuscar.Controls.Add(this.label12);
            this.groupbuscar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupbuscar.ForeColor = System.Drawing.Color.White;
            this.groupbuscar.Location = new System.Drawing.Point(24, 60);
            this.groupbuscar.Name = "groupbuscar";
            this.groupbuscar.Size = new System.Drawing.Size(670, 238);
            this.groupbuscar.TabIndex = 19;
            this.groupbuscar.TabStop = false;
            this.groupbuscar.Text = "Busqueda";
            this.groupbuscar.Visible = false;
            this.groupbuscar.Enter += new System.EventHandler(this.groupbuscar_Enter);
            // 
            // dgvservicios
            // 
            this.dgvservicios.AllowUserToAddRows = false;
            this.dgvservicios.AllowUserToDeleteRows = false;
            this.dgvservicios.AllowUserToOrderColumns = true;
            this.dgvservicios.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.dgvservicios.BackgroundColor = System.Drawing.Color.WhiteSmoke;
            this.dgvservicios.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvservicios.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            this.dgvservicios.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableWithoutHeaderText;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(32)))), ((int)(((byte)(40)))));
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvservicios.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.dgvservicios.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvservicios.Cursor = System.Windows.Forms.Cursors.Hand;
            this.dgvservicios.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.dgvservicios.EnableHeadersVisualStyles = false;
            this.dgvservicios.GridColor = System.Drawing.Color.Black;
            this.dgvservicios.Location = new System.Drawing.Point(24, 52);
            this.dgvservicios.MultiSelect = false;
            this.dgvservicios.Name = "dgvservicios";
            this.dgvservicios.ReadOnly = true;
            this.dgvservicios.RowHeadersVisible = false;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(32)))), ((int)(((byte)(40)))));
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.SteelBlue;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.White;
            this.dgvservicios.RowsDefaultCellStyle = dataGridViewCellStyle6;
            this.dgvservicios.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvservicios.Size = new System.Drawing.Size(622, 178);
            this.dgvservicios.TabIndex = 39;
            this.dgvservicios.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvservicios_CellDoubleClick);
            // 
            // combotipo
            // 
            this.combotipo.FormattingEnabled = true;
            this.combotipo.Items.AddRange(new object[] {
            "Instalación",
            "Mantenimiento Preventivo",
            "Mantenimiento Correctivo",
            "Garantía",
            "Otros"});
            this.combotipo.Location = new System.Drawing.Point(318, 19);
            this.combotipo.Name = "combotipo";
            this.combotipo.Size = new System.Drawing.Size(121, 23);
            this.combotipo.TabIndex = 10;
            this.combotipo.Visible = false;
            this.combotipo.SelectedIndexChanged += new System.EventHandler(this.combotipo_SelectedIndexChanged);
            this.combotipo.KeyUp += new System.Windows.Forms.KeyEventHandler(this.combotipo_KeyUp);
            // 
            // txtidcolaborador
            // 
            this.txtidcolaborador.Location = new System.Drawing.Point(447, 21);
            this.txtidcolaborador.Name = "txtidcolaborador";
            this.txtidcolaborador.Size = new System.Drawing.Size(94, 21);
            this.txtidcolaborador.TabIndex = 9;
            this.txtidcolaborador.Visible = false;
            this.txtidcolaborador.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtidcolaborador_KeyUp);
            // 
            // lbcolaborador
            // 
            this.lbcolaborador.AutoSize = true;
            this.lbcolaborador.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbcolaborador.ForeColor = System.Drawing.Color.White;
            this.lbcolaborador.Location = new System.Drawing.Point(316, 20);
            this.lbcolaborador.Name = "lbcolaborador";
            this.lbcolaborador.Size = new System.Drawing.Size(123, 18);
            this.lbcolaborador.TabIndex = 8;
            this.lbcolaborador.Text = "ID Colaborador";
            this.lbcolaborador.Visible = false;
            // 
            // combobusqueda
            // 
            this.combobusqueda.FormattingEnabled = true;
            this.combobusqueda.Items.AddRange(new object[] {
            "Cliente",
            "Colaborador",
            "Tipo"});
            this.combobusqueda.Location = new System.Drawing.Point(169, 21);
            this.combobusqueda.Name = "combobusqueda";
            this.combobusqueda.Size = new System.Drawing.Size(121, 23);
            this.combobusqueda.TabIndex = 7;
            this.combobusqueda.SelectedIndexChanged += new System.EventHandler(this.combobusqueda_SelectedIndexChanged);
            // 
            // txtnombrec
            // 
            this.txtnombrec.Location = new System.Drawing.Point(399, 23);
            this.txtnombrec.Name = "txtnombrec";
            this.txtnombrec.Size = new System.Drawing.Size(189, 21);
            this.txtnombrec.TabIndex = 4;
            this.txtnombrec.Visible = false;
            this.txtnombrec.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtnombrec_KeyUp);
            // 
            // lbapellidoc
            // 
            this.lbapellidoc.AutoSize = true;
            this.lbapellidoc.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbapellidoc.ForeColor = System.Drawing.Color.White;
            this.lbapellidoc.Location = new System.Drawing.Point(321, 26);
            this.lbapellidoc.Name = "lbapellidoc";
            this.lbapellidoc.Size = new System.Drawing.Size(80, 18);
            this.lbapellidoc.TabIndex = 1;
            this.lbapellidoc.Text = "Empresa:";
            this.lbapellidoc.Visible = false;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.White;
            this.label12.Location = new System.Drawing.Point(21, 26);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(135, 16);
            this.label12.TabIndex = 0;
            this.label12.Text = "Tipo de busqueda";
            // 
            // groupservicio
            // 
            this.groupservicio.Controls.Add(this.lbcorrelativo);
            this.groupservicio.Controls.Add(this.label14);
            this.groupservicio.Controls.Add(this.txtmostrardescripcion);
            this.groupservicio.Controls.Add(this.lbfecha);
            this.groupservicio.Controls.Add(this.label4);
            this.groupservicio.Controls.Add(this.lbfechafinal);
            this.groupservicio.Controls.Add(this.label27);
            this.groupservicio.Controls.Add(this.lbdireccioncliente);
            this.groupservicio.Controls.Add(this.label25);
            this.groupservicio.Controls.Add(this.lbtelefonocliente);
            this.groupservicio.Controls.Add(this.label24);
            this.groupservicio.Controls.Add(this.lbnombrecliente);
            this.groupservicio.Controls.Add(this.lbapellidocliente);
            this.groupservicio.Controls.Add(this.label18);
            this.groupservicio.Controls.Add(this.label17);
            this.groupservicio.Controls.Add(this.lbfechainicio);
            this.groupservicio.Controls.Add(this.label16);
            this.groupservicio.Controls.Add(this.lbtipodeservicio);
            this.groupservicio.Controls.Add(this.lbidcolaborador);
            this.groupservicio.Controls.Add(this.lbnombrecolaborador);
            this.groupservicio.Controls.Add(this.label10);
            this.groupservicio.Controls.Add(this.label11);
            this.groupservicio.Controls.Add(this.label13);
            this.groupservicio.Controls.Add(this.label15);
            this.groupservicio.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupservicio.ForeColor = System.Drawing.Color.White;
            this.groupservicio.Location = new System.Drawing.Point(24, 300);
            this.groupservicio.Name = "groupservicio";
            this.groupservicio.Size = new System.Drawing.Size(670, 422);
            this.groupservicio.TabIndex = 20;
            this.groupservicio.TabStop = false;
            this.groupservicio.Text = "Historial";
            this.groupservicio.Visible = false;
            this.groupservicio.Enter += new System.EventHandler(this.groupservicio_Enter);
            // 
            // lbcorrelativo
            // 
            this.lbcorrelativo.AutoSize = true;
            this.lbcorrelativo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbcorrelativo.ForeColor = System.Drawing.Color.Turquoise;
            this.lbcorrelativo.Location = new System.Drawing.Point(162, 250);
            this.lbcorrelativo.Name = "lbcorrelativo";
            this.lbcorrelativo.Size = new System.Drawing.Size(11, 16);
            this.lbcorrelativo.TabIndex = 42;
            this.lbcorrelativo.Text = ".";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.White;
            this.label14.Location = new System.Drawing.Point(23, 248);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(96, 18);
            this.label14.TabIndex = 41;
            this.label14.Text = "Correlativo:";
            // 
            // txtmostrardescripcion
            // 
            this.txtmostrardescripcion.Enabled = false;
            this.txtmostrardescripcion.Location = new System.Drawing.Point(132, 291);
            this.txtmostrardescripcion.Multiline = true;
            this.txtmostrardescripcion.Name = "txtmostrardescripcion";
            this.txtmostrardescripcion.Size = new System.Drawing.Size(455, 115);
            this.txtmostrardescripcion.TabIndex = 40;
            // 
            // lbfecha
            // 
            this.lbfecha.AutoSize = true;
            this.lbfecha.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbfecha.ForeColor = System.Drawing.Color.Turquoise;
            this.lbfecha.Location = new System.Drawing.Point(463, 33);
            this.lbfecha.Name = "lbfecha";
            this.lbfecha.Size = new System.Drawing.Size(11, 16);
            this.lbfecha.TabIndex = 39;
            this.lbfecha.Text = ".";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(325, 33);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(128, 18);
            this.label4.TabIndex = 38;
            this.label4.Text = "Fecha Registro:";
            // 
            // lbfechafinal
            // 
            this.lbfechafinal.AutoSize = true;
            this.lbfechafinal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbfechafinal.ForeColor = System.Drawing.Color.Turquoise;
            this.lbfechafinal.Location = new System.Drawing.Point(463, 205);
            this.lbfechafinal.Name = "lbfechafinal";
            this.lbfechafinal.Size = new System.Drawing.Size(11, 16);
            this.lbfechafinal.TabIndex = 37;
            this.lbfechafinal.Text = ".";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.ForeColor = System.Drawing.Color.White;
            this.label27.Location = new System.Drawing.Point(325, 205);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(100, 18);
            this.label27.TabIndex = 36;
            this.label27.Text = "Fecha Final:";
            // 
            // lbdireccioncliente
            // 
            this.lbdireccioncliente.AutoSize = true;
            this.lbdireccioncliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbdireccioncliente.ForeColor = System.Drawing.Color.Turquoise;
            this.lbdireccioncliente.Location = new System.Drawing.Point(463, 163);
            this.lbdireccioncliente.Name = "lbdireccioncliente";
            this.lbdireccioncliente.Size = new System.Drawing.Size(11, 16);
            this.lbdireccioncliente.TabIndex = 35;
            this.lbdireccioncliente.Text = ".";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.White;
            this.label25.Location = new System.Drawing.Point(325, 163);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(142, 18);
            this.label25.TabIndex = 34;
            this.label25.Text = "Dirección Cliente:";
            // 
            // lbtelefonocliente
            // 
            this.lbtelefonocliente.AutoSize = true;
            this.lbtelefonocliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbtelefonocliente.ForeColor = System.Drawing.Color.Turquoise;
            this.lbtelefonocliente.Location = new System.Drawing.Point(463, 120);
            this.lbtelefonocliente.Name = "lbtelefonocliente";
            this.lbtelefonocliente.Size = new System.Drawing.Size(11, 16);
            this.lbtelefonocliente.TabIndex = 33;
            this.lbtelefonocliente.Text = ".";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.White;
            this.label24.Location = new System.Drawing.Point(325, 120);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(136, 18);
            this.label24.TabIndex = 32;
            this.label24.Text = "Teléfono Cliente:";
            // 
            // lbnombrecliente
            // 
            this.lbnombrecliente.AutoSize = true;
            this.lbnombrecliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbnombrecliente.ForeColor = System.Drawing.Color.Turquoise;
            this.lbnombrecliente.Location = new System.Drawing.Point(162, 82);
            this.lbnombrecliente.Name = "lbnombrecliente";
            this.lbnombrecliente.Size = new System.Drawing.Size(11, 16);
            this.lbnombrecliente.TabIndex = 31;
            this.lbnombrecliente.Text = ".";
            // 
            // lbapellidocliente
            // 
            this.lbapellidocliente.AutoSize = true;
            this.lbapellidocliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbapellidocliente.ForeColor = System.Drawing.Color.Turquoise;
            this.lbapellidocliente.Location = new System.Drawing.Point(463, 82);
            this.lbapellidocliente.Name = "lbapellidocliente";
            this.lbapellidocliente.Size = new System.Drawing.Size(11, 16);
            this.lbapellidocliente.TabIndex = 30;
            this.lbapellidocliente.Text = ".";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.White;
            this.label18.Location = new System.Drawing.Point(325, 77);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(82, 18);
            this.label18.TabIndex = 29;
            this.label18.Text = "Contacto:";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.White;
            this.label17.Location = new System.Drawing.Point(23, 76);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(80, 18);
            this.label17.TabIndex = 28;
            this.label17.Text = "Empresa:";
            // 
            // lbfechainicio
            // 
            this.lbfechainicio.AutoSize = true;
            this.lbfechainicio.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbfechainicio.ForeColor = System.Drawing.Color.Turquoise;
            this.lbfechainicio.Location = new System.Drawing.Point(162, 205);
            this.lbfechainicio.Name = "lbfechainicio";
            this.lbfechainicio.Size = new System.Drawing.Size(11, 16);
            this.lbfechainicio.TabIndex = 27;
            this.lbfechainicio.Text = ".";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.White;
            this.label16.Location = new System.Drawing.Point(23, 205);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(104, 18);
            this.label16.TabIndex = 26;
            this.label16.Text = "Fecha Inicio:";
            // 
            // lbtipodeservicio
            // 
            this.lbtipodeservicio.AutoSize = true;
            this.lbtipodeservicio.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbtipodeservicio.ForeColor = System.Drawing.Color.Turquoise;
            this.lbtipodeservicio.Location = new System.Drawing.Point(164, 163);
            this.lbtipodeservicio.Name = "lbtipodeservicio";
            this.lbtipodeservicio.Size = new System.Drawing.Size(11, 16);
            this.lbtipodeservicio.TabIndex = 24;
            this.lbtipodeservicio.Text = ".";
            // 
            // lbidcolaborador
            // 
            this.lbidcolaborador.AutoSize = true;
            this.lbidcolaborador.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbidcolaborador.ForeColor = System.Drawing.Color.Turquoise;
            this.lbidcolaborador.Location = new System.Drawing.Point(164, 120);
            this.lbidcolaborador.Name = "lbidcolaborador";
            this.lbidcolaborador.Size = new System.Drawing.Size(11, 16);
            this.lbidcolaborador.TabIndex = 23;
            this.lbidcolaborador.Text = ".";
            // 
            // lbnombrecolaborador
            // 
            this.lbnombrecolaborador.AutoSize = true;
            this.lbnombrecolaborador.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbnombrecolaborador.ForeColor = System.Drawing.Color.Turquoise;
            this.lbnombrecolaborador.Location = new System.Drawing.Point(162, 35);
            this.lbnombrecolaborador.Name = "lbnombrecolaborador";
            this.lbnombrecolaborador.Size = new System.Drawing.Size(11, 16);
            this.lbnombrecolaborador.TabIndex = 21;
            this.lbnombrecolaborador.Text = ".";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.White;
            this.label10.Location = new System.Drawing.Point(22, 120);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(128, 18);
            this.label10.TabIndex = 14;
            this.label10.Text = "ID Colaborador:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.White;
            this.label11.Location = new System.Drawing.Point(21, 286);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(103, 18);
            this.label11.TabIndex = 11;
            this.label11.Text = "Descripción:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.White;
            this.label13.Location = new System.Drawing.Point(21, 163);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(46, 18);
            this.label13.TabIndex = 9;
            this.label13.Text = "Tipo:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.White;
            this.label15.Location = new System.Drawing.Point(21, 33);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(107, 18);
            this.label15.TabIndex = 1;
            this.label15.Text = "Colaborador:";
            // 
            // dgvcolaboradores
            // 
            this.dgvcolaboradores.AllowUserToAddRows = false;
            this.dgvcolaboradores.AllowUserToDeleteRows = false;
            this.dgvcolaboradores.AllowUserToOrderColumns = true;
            this.dgvcolaboradores.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.dgvcolaboradores.BackgroundColor = System.Drawing.Color.WhiteSmoke;
            this.dgvcolaboradores.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvcolaboradores.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            this.dgvcolaboradores.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableWithoutHeaderText;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(32)))), ((int)(((byte)(40)))));
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvcolaboradores.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.dgvcolaboradores.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvcolaboradores.Cursor = System.Windows.Forms.Cursors.Hand;
            this.dgvcolaboradores.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.dgvcolaboradores.EnableHeadersVisualStyles = false;
            this.dgvcolaboradores.GridColor = System.Drawing.Color.Black;
            this.dgvcolaboradores.Location = new System.Drawing.Point(2, 290);
            this.dgvcolaboradores.MultiSelect = false;
            this.dgvcolaboradores.Name = "dgvcolaboradores";
            this.dgvcolaboradores.ReadOnly = true;
            this.dgvcolaboradores.RowHeadersVisible = false;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(32)))), ((int)(((byte)(40)))));
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.Color.SteelBlue;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.Color.White;
            this.dgvcolaboradores.RowsDefaultCellStyle = dataGridViewCellStyle8;
            this.dgvcolaboradores.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvcolaboradores.Size = new System.Drawing.Size(18, 18);
            this.dgvcolaboradores.TabIndex = 39;
            this.dgvcolaboradores.Visible = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label5.Location = new System.Drawing.Point(355, 42);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(43, 24);
            this.label5.TabIndex = 40;
            this.label5.Text = "No.";
            // 
            // lbnoservicio
            // 
            this.lbnoservicio.AutoSize = true;
            this.lbnoservicio.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbnoservicio.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lbnoservicio.Location = new System.Drawing.Point(396, 43);
            this.lbnoservicio.Name = "lbnoservicio";
            this.lbnoservicio.Size = new System.Drawing.Size(32, 24);
            this.lbnoservicio.TabIndex = 41;
            this.lbnoservicio.Text = "00";
            // 
            // FServicios
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(32)))), ((int)(((byte)(40)))));
            this.ClientSize = new System.Drawing.Size(722, 733);
            this.Controls.Add(this.lbnoservicio);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.groupcolaborador);
            this.Controls.Add(this.dgvcolaboradores);
            this.Controls.Add(this.btnatras);
            this.Controls.Add(this.groupservicio);
            this.Controls.Add(this.btnbuscar);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.groupcliente);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.groupbuscar);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FServicios";
            this.Text = "FServicios";
            this.Load += new System.EventHandler(this.FServicios_Load);
            this.groupcliente.ResumeLayout(false);
            this.groupcliente.PerformLayout();
            this.groupcolaborador.ResumeLayout(false);
            this.groupcolaborador.PerformLayout();
            this.groupalerta.ResumeLayout(false);
            this.groupalerta.PerformLayout();
            this.groupbuscar.ResumeLayout(false);
            this.groupbuscar.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvservicios)).EndInit();
            this.groupservicio.ResumeLayout(false);
            this.groupservicio.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvcolaboradores)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupcliente;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupcolaborador;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.DateTimePicker dtfecha;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button btnbuscar;
        private System.Windows.Forms.Button btnatras;
        private System.Windows.Forms.GroupBox groupbuscar;
        private System.Windows.Forms.TextBox txtnombrec;
        private System.Windows.Forms.Label lbapellidoc;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox combobusqueda;
        private System.Windows.Forms.TextBox txtidcolaborador;
        private System.Windows.Forms.Label lbcolaborador;
        private System.Windows.Forms.ComboBox combotipo;
        private System.Windows.Forms.GroupBox groupservicio;
        private System.Windows.Forms.Label lbnombrecliente;
        private System.Windows.Forms.Label lbapellidocliente;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label lbfechainicio;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label lbtipodeservicio;
        private System.Windows.Forms.Label lbidcolaborador;
        private System.Windows.Forms.Label lbnombrecolaborador;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label lbdireccioncliente;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label lbtelefonocliente;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Button btnguardar;
        private System.Windows.Forms.Button btncargarfotos;
        private System.Windows.Forms.Label lbfechafinal;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.GroupBox groupalerta;
        private System.Windows.Forms.TextBox txtdescripcionalerta;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.DateTimePicker dtfechaalerta;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btnNuevoCliente;
        private System.Windows.Forms.Label txtdireccion;
        private System.Windows.Forms.Label txttelefono;
        private System.Windows.Forms.Label txtnit;
        private System.Windows.Forms.Label txtapellido;
        private System.Windows.Forms.Label txtnombre;
        private System.Windows.Forms.DataGridView dgvservicios;
        private System.Windows.Forms.Label lbfecha;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtmostrardescripcion;
        private System.Windows.Forms.DataGridView dgvcolaboradores;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lbnoservicio;
        private System.Windows.Forms.Label lbcorrelativo;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtcorrelativo;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox txtdescripcion;
        private System.Windows.Forms.TextBox txtfinal;
        private System.Windows.Forms.TextBox txtinicio;
        private System.Windows.Forms.ComboBox txttipo;
        private System.Windows.Forms.ComboBox txtid;
    }
}