﻿namespace ProyectosMultiples
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.Barratitulo = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.Btnrestaurar = new System.Windows.Forms.PictureBox();
            this.Btnminimizar = new System.Windows.Forms.PictureBox();
            this.Btnmaximizar = new System.Windows.Forms.PictureBox();
            this.Btncerrar = new System.Windows.Forms.PictureBox();
            this.Menuvertical = new System.Windows.Forms.Panel();
            this.btnCerrarSesion = new System.Windows.Forms.Button();
            this.lbPermiso = new System.Windows.Forms.Label();
            this.LbId = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.Panelh = new System.Windows.Forms.Panel();
            this.Btnhistorial = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel8 = new System.Windows.Forms.Panel();
            this.Panels = new System.Windows.Forms.Panel();
            this.btnservicios = new System.Windows.Forms.Button();
            this.Lbusuario = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.Panelcol = new System.Windows.Forms.Panel();
            this.PanelR = new System.Windows.Forms.Panel();
            this.Panelco = new System.Windows.Forms.Panel();
            this.Panelc = new System.Windows.Forms.Panel();
            this.Btncolaboradores = new System.Windows.Forms.Button();
            this.panel5 = new System.Windows.Forms.Panel();
            this.btnreportes = new System.Windows.Forms.Button();
            this.panel4 = new System.Windows.Forms.Panel();
            this.Btncotizaciones = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.Btnclientes = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.Panelcontenedor = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.Barratitulo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Btnrestaurar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btnminimizar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btnmaximizar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btncerrar)).BeginInit();
            this.Menuvertical.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // Barratitulo
            // 
            this.Barratitulo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(80)))), ((int)(((byte)(200)))));
            this.Barratitulo.Controls.Add(this.button1);
            this.Barratitulo.Controls.Add(this.Btnrestaurar);
            this.Barratitulo.Controls.Add(this.Btnminimizar);
            this.Barratitulo.Controls.Add(this.Btnmaximizar);
            this.Barratitulo.Controls.Add(this.Btncerrar);
            this.Barratitulo.Dock = System.Windows.Forms.DockStyle.Top;
            this.Barratitulo.Location = new System.Drawing.Point(0, 0);
            this.Barratitulo.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Barratitulo.Name = "Barratitulo";
            this.Barratitulo.Size = new System.Drawing.Size(1300, 43);
            this.Barratitulo.TabIndex = 0;
            this.Barratitulo.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Barratitulo_MouseDown);
            // 
            // button1
            // 
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(32)))), ((int)(((byte)(40)))));
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Image = global::ProyectosMultiples.Properties.Resources.p6;
            this.button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button1.Location = new System.Drawing.Point(8, 4);
            this.button1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(120, 36);
            this.button1.TabIndex = 5;
            this.button1.Text = "About";
            this.button1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_2);
            // 
            // Btnrestaurar
            // 
            this.Btnrestaurar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Btnrestaurar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Btnrestaurar.Image = ((System.Drawing.Image)(resources.GetObject("Btnrestaurar.Image")));
            this.Btnrestaurar.Location = new System.Drawing.Point(1197, 9);
            this.Btnrestaurar.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Btnrestaurar.Name = "Btnrestaurar";
            this.Btnrestaurar.Size = new System.Drawing.Size(33, 31);
            this.Btnrestaurar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Btnrestaurar.TabIndex = 1;
            this.Btnrestaurar.TabStop = false;
            this.Btnrestaurar.Visible = false;
            this.Btnrestaurar.Click += new System.EventHandler(this.Btnrestaurar_Click);
            // 
            // Btnminimizar
            // 
            this.Btnminimizar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Btnminimizar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Btnminimizar.Image = ((System.Drawing.Image)(resources.GetObject("Btnminimizar.Image")));
            this.Btnminimizar.Location = new System.Drawing.Point(1143, 9);
            this.Btnminimizar.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Btnminimizar.Name = "Btnminimizar";
            this.Btnminimizar.Size = new System.Drawing.Size(33, 31);
            this.Btnminimizar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Btnminimizar.TabIndex = 1;
            this.Btnminimizar.TabStop = false;
            this.Btnminimizar.Click += new System.EventHandler(this.Btnminimizar_Click);
            // 
            // Btnmaximizar
            // 
            this.Btnmaximizar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Btnmaximizar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Btnmaximizar.Image = ((System.Drawing.Image)(resources.GetObject("Btnmaximizar.Image")));
            this.Btnmaximizar.Location = new System.Drawing.Point(1197, 9);
            this.Btnmaximizar.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Btnmaximizar.Name = "Btnmaximizar";
            this.Btnmaximizar.Size = new System.Drawing.Size(33, 31);
            this.Btnmaximizar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Btnmaximizar.TabIndex = 1;
            this.Btnmaximizar.TabStop = false;
            this.Btnmaximizar.Click += new System.EventHandler(this.Btnmaximizar_Click);
            // 
            // Btncerrar
            // 
            this.Btncerrar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Btncerrar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Btncerrar.Image = ((System.Drawing.Image)(resources.GetObject("Btncerrar.Image")));
            this.Btncerrar.Location = new System.Drawing.Point(1251, 9);
            this.Btncerrar.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Btncerrar.Name = "Btncerrar";
            this.Btncerrar.Size = new System.Drawing.Size(33, 31);
            this.Btncerrar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Btncerrar.TabIndex = 0;
            this.Btncerrar.TabStop = false;
            this.Btncerrar.Click += new System.EventHandler(this.Btncerrar_Click);
            // 
            // Menuvertical
            // 
            this.Menuvertical.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(32)))), ((int)(((byte)(40)))));
            this.Menuvertical.Controls.Add(this.btnCerrarSesion);
            this.Menuvertical.Controls.Add(this.lbPermiso);
            this.Menuvertical.Controls.Add(this.LbId);
            this.Menuvertical.Controls.Add(this.button2);
            this.Menuvertical.Controls.Add(this.Panelh);
            this.Menuvertical.Controls.Add(this.Btnhistorial);
            this.Menuvertical.Controls.Add(this.panel3);
            this.Menuvertical.Controls.Add(this.panel8);
            this.Menuvertical.Controls.Add(this.Panels);
            this.Menuvertical.Controls.Add(this.btnservicios);
            this.Menuvertical.Controls.Add(this.Lbusuario);
            this.Menuvertical.Controls.Add(this.pictureBox2);
            this.Menuvertical.Controls.Add(this.Panelcol);
            this.Menuvertical.Controls.Add(this.PanelR);
            this.Menuvertical.Controls.Add(this.Panelco);
            this.Menuvertical.Controls.Add(this.Panelc);
            this.Menuvertical.Controls.Add(this.Btncolaboradores);
            this.Menuvertical.Controls.Add(this.panel5);
            this.Menuvertical.Controls.Add(this.btnreportes);
            this.Menuvertical.Controls.Add(this.panel4);
            this.Menuvertical.Controls.Add(this.Btncotizaciones);
            this.Menuvertical.Controls.Add(this.panel2);
            this.Menuvertical.Controls.Add(this.panel1);
            this.Menuvertical.Controls.Add(this.Btnclientes);
            this.Menuvertical.Controls.Add(this.pictureBox1);
            this.Menuvertical.Dock = System.Windows.Forms.DockStyle.Left;
            this.Menuvertical.Location = new System.Drawing.Point(0, 43);
            this.Menuvertical.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Menuvertical.Name = "Menuvertical";
            this.Menuvertical.Size = new System.Drawing.Size(293, 697);
            this.Menuvertical.TabIndex = 1;
            // 
            // btnCerrarSesion
            // 
            this.btnCerrarSesion.FlatAppearance.BorderSize = 0;
            this.btnCerrarSesion.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(80)))), ((int)(((byte)(200)))));
            this.btnCerrarSesion.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCerrarSesion.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCerrarSesion.ForeColor = System.Drawing.Color.White;
            this.btnCerrarSesion.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCerrarSesion.Location = new System.Drawing.Point(0, 671);
            this.btnCerrarSesion.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnCerrarSesion.Name = "btnCerrarSesion";
            this.btnCerrarSesion.Size = new System.Drawing.Size(285, 39);
            this.btnCerrarSesion.TabIndex = 12;
            this.btnCerrarSesion.Text = "Cerrar Sesión";
            this.btnCerrarSesion.UseVisualStyleBackColor = true;
            this.btnCerrarSesion.Visible = false;
            this.btnCerrarSesion.Click += new System.EventHandler(this.btnCerrarSesion_Click);
            // 
            // lbPermiso
            // 
            this.lbPermiso.AutoSize = true;
            this.lbPermiso.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbPermiso.ForeColor = System.Drawing.Color.White;
            this.lbPermiso.Location = new System.Drawing.Point(35, 631);
            this.lbPermiso.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbPermiso.Name = "lbPermiso";
            this.lbPermiso.Size = new System.Drawing.Size(46, 24);
            this.lbPermiso.TabIndex = 11;
            this.lbPermiso.Text = "Alto";
            this.lbPermiso.Visible = false;
            // 
            // LbId
            // 
            this.LbId.AutoSize = true;
            this.LbId.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LbId.ForeColor = System.Drawing.Color.White;
            this.LbId.Location = new System.Drawing.Point(13, 631);
            this.LbId.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LbId.Name = "LbId";
            this.LbId.Size = new System.Drawing.Size(21, 24);
            this.LbId.TabIndex = 10;
            this.LbId.Text = "0";
            this.LbId.Visible = false;
            // 
            // button2
            // 
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(80)))), ((int)(((byte)(200)))));
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.ForeColor = System.Drawing.Color.White;
            this.button2.Image = global::ProyectosMultiples.Properties.Resources.candado1;
            this.button2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button2.Location = new System.Drawing.Point(123, 624);
            this.button2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(41, 39);
            this.button2.TabIndex = 9;
            this.button2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Visible = false;
            this.button2.Click += new System.EventHandler(this.button2_Click_1);
            // 
            // Panelh
            // 
            this.Panelh.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(80)))), ((int)(((byte)(200)))));
            this.Panelh.Location = new System.Drawing.Point(283, 337);
            this.Panelh.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Panelh.Name = "Panelh";
            this.Panelh.Size = new System.Drawing.Size(7, 39);
            this.Panelh.TabIndex = 2;
            this.Panelh.Visible = false;
            // 
            // Btnhistorial
            // 
            this.Btnhistorial.FlatAppearance.BorderSize = 0;
            this.Btnhistorial.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(80)))), ((int)(((byte)(200)))));
            this.Btnhistorial.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Btnhistorial.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btnhistorial.ForeColor = System.Drawing.Color.White;
            this.Btnhistorial.Image = ((System.Drawing.Image)(resources.GetObject("Btnhistorial.Image")));
            this.Btnhistorial.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Btnhistorial.Location = new System.Drawing.Point(8, 337);
            this.Btnhistorial.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Btnhistorial.Name = "Btnhistorial";
            this.Btnhistorial.Size = new System.Drawing.Size(285, 39);
            this.Btnhistorial.TabIndex = 3;
            this.Btnhistorial.Text = "Alertas";
            this.Btnhistorial.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.Btnhistorial.UseVisualStyleBackColor = true;
            this.Btnhistorial.Visible = false;
            this.Btnhistorial.Click += new System.EventHandler(this.button2_Click);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(80)))), ((int)(((byte)(200)))));
            this.panel3.Location = new System.Drawing.Point(0, 337);
            this.panel3.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(7, 39);
            this.panel3.TabIndex = 2;
            this.panel3.Visible = false;
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(80)))), ((int)(((byte)(200)))));
            this.panel8.Location = new System.Drawing.Point(0, 290);
            this.panel8.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(7, 39);
            this.panel8.TabIndex = 3;
            this.panel8.Visible = false;
            // 
            // Panels
            // 
            this.Panels.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(80)))), ((int)(((byte)(200)))));
            this.Panels.Location = new System.Drawing.Point(283, 290);
            this.Panels.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Panels.Name = "Panels";
            this.Panels.Size = new System.Drawing.Size(7, 39);
            this.Panels.TabIndex = 3;
            this.Panels.Visible = false;
            // 
            // btnservicios
            // 
            this.btnservicios.FlatAppearance.BorderSize = 0;
            this.btnservicios.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(80)))), ((int)(((byte)(200)))));
            this.btnservicios.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnservicios.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnservicios.ForeColor = System.Drawing.Color.White;
            this.btnservicios.Image = ((System.Drawing.Image)(resources.GetObject("btnservicios.Image")));
            this.btnservicios.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnservicios.Location = new System.Drawing.Point(8, 290);
            this.btnservicios.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnservicios.Name = "btnservicios";
            this.btnservicios.Size = new System.Drawing.Size(285, 39);
            this.btnservicios.TabIndex = 4;
            this.btnservicios.Text = "Servicios";
            this.btnservicios.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnservicios.UseVisualStyleBackColor = true;
            this.btnservicios.Visible = false;
            this.btnservicios.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // Lbusuario
            // 
            this.Lbusuario.AutoSize = true;
            this.Lbusuario.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lbusuario.ForeColor = System.Drawing.Color.White;
            this.Lbusuario.Location = new System.Drawing.Point(75, 596);
            this.Lbusuario.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Lbusuario.Name = "Lbusuario";
            this.Lbusuario.Size = new System.Drawing.Size(81, 24);
            this.Lbusuario.TabIndex = 8;
            this.Lbusuario.Text = "Usuario";
            this.Lbusuario.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.Lbusuario.Visible = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::ProyectosMultiples.Properties.Resources.inicio;
            this.pictureBox2.Location = new System.Drawing.Point(96, 486);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(97, 95);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 7;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Visible = false;
            // 
            // Panelcol
            // 
            this.Panelcol.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(80)))), ((int)(((byte)(200)))));
            this.Panelcol.Location = new System.Drawing.Point(283, 431);
            this.Panelcol.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Panelcol.Name = "Panelcol";
            this.Panelcol.Size = new System.Drawing.Size(7, 39);
            this.Panelcol.TabIndex = 2;
            this.Panelcol.Visible = false;
            // 
            // PanelR
            // 
            this.PanelR.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(80)))), ((int)(((byte)(200)))));
            this.PanelR.Location = new System.Drawing.Point(283, 384);
            this.PanelR.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.PanelR.Name = "PanelR";
            this.PanelR.Size = new System.Drawing.Size(7, 39);
            this.PanelR.TabIndex = 2;
            this.PanelR.Visible = false;
            // 
            // Panelco
            // 
            this.Panelco.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(80)))), ((int)(((byte)(200)))));
            this.Panelco.Location = new System.Drawing.Point(283, 244);
            this.Panelco.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Panelco.Name = "Panelco";
            this.Panelco.Size = new System.Drawing.Size(7, 39);
            this.Panelco.TabIndex = 2;
            this.Panelco.Visible = false;
            // 
            // Panelc
            // 
            this.Panelc.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(80)))), ((int)(((byte)(200)))));
            this.Panelc.Location = new System.Drawing.Point(283, 197);
            this.Panelc.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Panelc.Name = "Panelc";
            this.Panelc.Size = new System.Drawing.Size(7, 39);
            this.Panelc.TabIndex = 1;
            this.Panelc.Visible = false;
            // 
            // Btncolaboradores
            // 
            this.Btncolaboradores.FlatAppearance.BorderSize = 0;
            this.Btncolaboradores.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(80)))), ((int)(((byte)(200)))));
            this.Btncolaboradores.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Btncolaboradores.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btncolaboradores.ForeColor = System.Drawing.Color.White;
            this.Btncolaboradores.Image = ((System.Drawing.Image)(resources.GetObject("Btncolaboradores.Image")));
            this.Btncolaboradores.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Btncolaboradores.Location = new System.Drawing.Point(8, 431);
            this.Btncolaboradores.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Btncolaboradores.Name = "Btncolaboradores";
            this.Btncolaboradores.Size = new System.Drawing.Size(285, 39);
            this.Btncolaboradores.TabIndex = 6;
            this.Btncolaboradores.Text = "Colaboradores";
            this.Btncolaboradores.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.Btncolaboradores.UseVisualStyleBackColor = true;
            this.Btncolaboradores.Visible = false;
            this.Btncolaboradores.Click += new System.EventHandler(this.button4_Click);
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(80)))), ((int)(((byte)(200)))));
            this.panel5.Location = new System.Drawing.Point(0, 431);
            this.panel5.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(7, 39);
            this.panel5.TabIndex = 3;
            this.panel5.Visible = false;
            // 
            // btnreportes
            // 
            this.btnreportes.FlatAppearance.BorderSize = 0;
            this.btnreportes.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(80)))), ((int)(((byte)(200)))));
            this.btnreportes.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnreportes.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnreportes.ForeColor = System.Drawing.Color.White;
            this.btnreportes.Image = ((System.Drawing.Image)(resources.GetObject("btnreportes.Image")));
            this.btnreportes.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnreportes.Location = new System.Drawing.Point(8, 384);
            this.btnreportes.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnreportes.Name = "btnreportes";
            this.btnreportes.Size = new System.Drawing.Size(285, 39);
            this.btnreportes.TabIndex = 4;
            this.btnreportes.Text = "Reportes";
            this.btnreportes.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnreportes.UseVisualStyleBackColor = true;
            this.btnreportes.Visible = false;
            this.btnreportes.Click += new System.EventHandler(this.button3_Click);
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(80)))), ((int)(((byte)(200)))));
            this.panel4.Location = new System.Drawing.Point(0, 384);
            this.panel4.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(7, 39);
            this.panel4.TabIndex = 3;
            this.panel4.Visible = false;
            // 
            // Btncotizaciones
            // 
            this.Btncotizaciones.FlatAppearance.BorderSize = 0;
            this.Btncotizaciones.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(80)))), ((int)(((byte)(200)))));
            this.Btncotizaciones.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Btncotizaciones.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btncotizaciones.ForeColor = System.Drawing.Color.White;
            this.Btncotizaciones.Image = ((System.Drawing.Image)(resources.GetObject("Btncotizaciones.Image")));
            this.Btncotizaciones.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Btncotizaciones.Location = new System.Drawing.Point(8, 244);
            this.Btncotizaciones.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Btncotizaciones.Name = "Btncotizaciones";
            this.Btncotizaciones.Size = new System.Drawing.Size(285, 39);
            this.Btncotizaciones.TabIndex = 2;
            this.Btncotizaciones.Text = "Cotizaciones y Ventas";
            this.Btncotizaciones.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.Btncotizaciones.UseVisualStyleBackColor = true;
            this.Btncotizaciones.Visible = false;
            this.Btncotizaciones.Click += new System.EventHandler(this.button1_Click);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(80)))), ((int)(((byte)(200)))));
            this.panel2.Location = new System.Drawing.Point(1, 244);
            this.panel2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(7, 39);
            this.panel2.TabIndex = 1;
            this.panel2.Visible = false;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(80)))), ((int)(((byte)(200)))));
            this.panel1.Location = new System.Drawing.Point(1, 197);
            this.panel1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(7, 39);
            this.panel1.TabIndex = 0;
            this.panel1.Visible = false;
            // 
            // Btnclientes
            // 
            this.Btnclientes.FlatAppearance.BorderSize = 0;
            this.Btnclientes.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(80)))), ((int)(((byte)(200)))));
            this.Btnclientes.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Btnclientes.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btnclientes.ForeColor = System.Drawing.Color.White;
            this.Btnclientes.Image = ((System.Drawing.Image)(resources.GetObject("Btnclientes.Image")));
            this.Btnclientes.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Btnclientes.Location = new System.Drawing.Point(8, 197);
            this.Btnclientes.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Btnclientes.Name = "Btnclientes";
            this.Btnclientes.Size = new System.Drawing.Size(285, 39);
            this.Btnclientes.TabIndex = 0;
            this.Btnclientes.Text = "Clientes";
            this.Btnclientes.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.Btnclientes.UseVisualStyleBackColor = true;
            this.Btnclientes.Visible = false;
            this.Btnclientes.Click += new System.EventHandler(this.Btnclientes_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(64, 7);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(161, 142);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // Panelcontenedor
            // 
            this.Panelcontenedor.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(66)))), ((int)(((byte)(82)))));
            this.Panelcontenedor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Panelcontenedor.Location = new System.Drawing.Point(293, 43);
            this.Panelcontenedor.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Panelcontenedor.Name = "Panelcontenedor";
            this.Panelcontenedor.Size = new System.Drawing.Size(1007, 697);
            this.Panelcontenedor.TabIndex = 2;
            this.Panelcontenedor.Paint += new System.Windows.Forms.PaintEventHandler(this.Panelcontenedor_Paint);
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.panel6.Location = new System.Drawing.Point(293, 43);
            this.panel6.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(13, 911);
            this.panel6.TabIndex = 0;
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 10;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick_1);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1300, 740);
            this.Controls.Add(this.panel6);
            this.Controls.Add(this.Panelcontenedor);
            this.Controls.Add(this.Menuvertical);
            this.Controls.Add(this.Barratitulo);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "Form1";
            this.Text = "   ";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Barratitulo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Btnrestaurar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btnminimizar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btnmaximizar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Btncerrar)).EndInit();
            this.Menuvertical.ResumeLayout(false);
            this.Menuvertical.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel Barratitulo;
        private System.Windows.Forms.PictureBox Btncerrar;
        private System.Windows.Forms.Panel Menuvertical;
        private System.Windows.Forms.PictureBox Btnrestaurar;
        private System.Windows.Forms.PictureBox Btnminimizar;
        private System.Windows.Forms.PictureBox Btnmaximizar;
        private System.Windows.Forms.Button Btnclientes;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button Btncolaboradores;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Button btnreportes;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Button Btnhistorial;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button Btncotizaciones;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel Panelc;
        private System.Windows.Forms.Panel Panelcol;
        private System.Windows.Forms.Panel PanelR;
        private System.Windows.Forms.Panel Panelh;
        private System.Windows.Forms.Panel Panelco;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label Lbusuario;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Panel Panels;
        private System.Windows.Forms.Button btnservicios;
        public System.Windows.Forms.Panel Panelcontenedor;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label LbId;
        private System.Windows.Forms.Button btnCerrarSesion;
        private System.Windows.Forms.Label lbPermiso;
        private System.Windows.Forms.Button button1;
    }
}

