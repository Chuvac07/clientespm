﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;


namespace ProyectosMultiples
{
    public partial class FServicios : Form
    {
        public FServicios()
        {
            InitializeComponent();
        }
        Conexion conect = new Conexion();//conectar con base de datos

        // variables globales para recibir los valores del FORM CLIENTES
        public static string idCliente = "";
        public static string nombreCliente = "";
        public static string apellidoCliente = "";
        public static string nitCliente = "";
        public static string telefonoCliente = "";
        public static string direccionCliente = "";
        public static int contadoralerta = 0;
        public static int bandera = 0;
        public static string escritorio;
        public static string carpeta;
        public static string subcarpeta;
        public static string ff;
        public OpenFileDialog openFileDialog = new OpenFileDialog();

        private void FServicios_Load(object sender, EventArgs e)
        {
            try
            {
                txtnombre.Text = nombreCliente;
                txtapellido.Text = apellidoCliente;
                txtnit.Text = nitCliente;
                txttelefono.Text = telefonoCliente;
                txtdireccion.Text = direccionCliente;

                


      
              
                
                if (txtnombre.Text != "" && txtapellido.Text != "")
                {
                    btnNuevoCliente.Enabled = false;
                    groupcolaborador.Visible = true;


                }
                conect.actualizarServicios(dgvservicios);
                conect.actualizarColaboradores(dgvcolaboradores);
                lbnoservicio.Text = Convert.ToString(dgvservicios.Rows.Count + 1);// numero de servicio

                this.dgvservicios.Columns["Inicio"].Visible = false;
                this.dgvservicios.Columns["Final"].Visible = false;
                this.dgvservicios.Columns["Descripcion"].Visible = false;
                this.dgvservicios.Columns["Colaborador"].Visible = false;
                this.dgvservicios.Columns["Telefono"].Visible = false;
                this.dgvservicios.Columns["Direccion"].Visible = false;
                this.dgvservicios.Columns["Correlativo"].Visible = false;
                foreach (DataGridViewRow columna in dgvcolaboradores.Rows)
                {
                    txtid.Items.Add(columna.Cells[0].Value.ToString());

                }
            }
            catch (Exception ex)
            {
                FMessageBox obj = new FMessageBox("Error SE2: " + ex.Message, "¡Error de Conexión!", 1);
                if (obj.ShowDialog() == DialogResult.OK) ;
            }

        }

        private void combobusqueda_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(combobusqueda.Text=="Cliente")
            {
                lbapellidoc.Visible = true;
                txtnombrec.Visible = true;
            }
            else
            {
                lbapellidoc.Visible = false;
                txtnombrec.Visible = false;
            }
            if(combobusqueda.Text=="Colaborador")
            {
                lbcolaborador.Visible = true;
                txtidcolaborador.Visible = true;
            }
            else
            {
                lbcolaborador.Visible = false;
                txtidcolaborador.Visible = false;
            }
            if (combobusqueda.Text == "Tipo") combotipo.Visible = true;
            else combotipo.Visible = false;
            
           
        }

        private void groupbuscar_Enter(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            groupbuscar.Visible = true;
            groupservicio.Visible = true;
            groupcliente.Visible = false;
            groupcolaborador.Visible = false;
            btnatras.Visible = true;
            btnbuscar.Visible = false;
            lbnoservicio.Visible = false;
            label5.Visible = false;
            dgvservicios.ClearSelection();


        }

        private void btnatras_Click(object sender, EventArgs e)
        {
            groupbuscar.Visible = false;
            groupservicio.Visible = false;
            groupcliente.Visible = true;
            groupalerta.Visible = false;
            groupcolaborador.Visible = true;
            btnatras.Visible = false;
            btnbuscar.Visible = true;
            lbnoservicio.Visible = true;
            label5.Visible = true;
        }
       
        private void button1_Click(object sender, EventArgs e)
        {
            if (contadoralerta == 0)
            {
                groupalerta.Visible = true;
                contadoralerta = 1;
            }
            else
            if (contadoralerta == 1)
            {
                groupalerta.Visible = false;
                contadoralerta = 0;
            }
        }
        private void btnguardar_Click(object sender, EventArgs e)
        {
            try
            {
               
                    string idC = idCliente;
                    DateTime fecha = dtfecha.Value;
                    string id = txtid.Text;
                    string tipo = txttipo.Text;
                    string fechainicio = txtinicio.Text;
                    string fechafinal = txtfinal.Text;
                    string descripcion = txtdescripcion.Text;
                    string correlativo = txtcorrelativo.Text;
                if (txtcorrelativo.Text != "" && txtid.Text != ""&& txtdescripcion.Text != "" && txtinicio.Text != "" && txtfinal.Text != "" && txttipo.Text != "")
                {
                    conect.ingresarservicios(fecha, descripcion, tipo, fechainicio, fechafinal, idC, id, correlativo);
                    conect.actualizarServicios(dgvservicios);

                    if (txtdescripcionalerta.Text != "")
                    {
                        DateTime fechaalerta = dtfechaalerta.Value;
                        string descripcionalerta = txtdescripcionalerta.Text;
                        string nitcliente = txtnit.Text;
                        conect.ingresaralerta(fechaalerta, descripcionalerta, 1, idCliente);
                        txtdescripcionalerta.Clear();
                        dtfechaalerta.ResetText();
                    }

                    //Almacenamiento de imagenes 
                    if (bandera==1)
                    {
                    
                        DateTime fecha1 = DateTime.Now;
                        fecha1.Minute.ToString();
                        // textBox1.Text = fecha1.Second.ToString();
                        string extension;
                       // OpenFileDialog openFileDialog = new OpenFileDialog();
                        //openFileDialog.Multiselect = true;
                        foreach (string f in openFileDialog.FileNames)
                        {
                            FileInfo fil = new FileInfo(f);
                            //ListaArchivos.Items.Add(fil);
                            extension = Path.GetExtension(Convert.ToString(fil));
                            string rutadestino = subcarpeta + "\\" + cont + " Servicio " + ff + extension;
                            if (File.Exists(rutadestino))
                            {
                                rutadestino = subcarpeta + "\\" + cont + " Servicio " + ff + fecha1.Minute.ToString() + extension;

                            }

                            File.Move(Convert.ToString(fil), rutadestino);
                            cont++;

                            
                            
                        }
                        FMessageBox obj = new FMessageBox("Imagenes almacenadas correctamente", "Almacenamiento", 0);
                        if (obj.ShowDialog() == DialogResult.OK) ;
                        bandera = 0;
                    }
                    txtid.Text = "";
                    txttipo.Text = "";
                    txtinicio.Clear();
                    txtfinal.Clear();
                    txtdescripcion.Clear();
                    txtnit.Text = "";
                    txtnombre.Text = "";
                    txtapellido.Text = "";
                    txttelefono.Text = "";
                    txtdireccion.Text = "";
                    txtcorrelativo.Text = "";
                    btnNuevoCliente.Enabled = true;
                    groupalerta.Visible = false;
                    int noServicio = Convert.ToInt32(lbnoservicio.Text);
                    noServicio++;
                    lbnoservicio.Text = Convert.ToString(noServicio);

                }
                else
                {
                    FMessageBox obj = new FMessageBox("Por favor llene todos los campos requeridos", "ERROR", 1);
                    if (obj.ShowDialog() == DialogResult.OK) ;
                }
               
            }
            catch (Exception ex)
            {
                FMessageBox obj = new FMessageBox("Error SE1: " + ex.Message, "¡Error de Conexión!", 1);
                if (obj.ShowDialog() == DialogResult.OK) ;
            }
        }

        private void btnNuevoCliente_Click(object sender, EventArgs e)
        {
            Form1.acceso = 3;

        }

       
        private void dgvservicios_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
           lbidcolaborador.Text =dgvservicios.CurrentRow.Cells[2].Value.ToString();
            lbtipodeservicio.Text = dgvservicios.CurrentRow.Cells[4].Value.ToString();
            lbfecha.Text = dgvservicios.CurrentRow.Cells[3].Value.ToString();
            lbfechainicio.Text = dgvservicios.CurrentRow.Cells[5].Value.ToString();
            lbfechafinal.Text = dgvservicios.CurrentRow.Cells[6].Value.ToString();
            txtmostrardescripcion.Text = dgvservicios.CurrentRow.Cells[7].Value.ToString();
            lbnombrecolaborador.Text = dgvservicios.CurrentRow.Cells[8].Value.ToString();
            lbnombrecliente.Text = dgvservicios.CurrentRow.Cells[9].Value.ToString();
            lbapellidocliente.Text = dgvservicios.CurrentRow.Cells[10].Value.ToString();
            lbtelefonocliente.Text = dgvservicios.CurrentRow.Cells[11].Value.ToString();
            lbdireccioncliente.Text= dgvservicios.CurrentRow.Cells[12].Value.ToString();
            lbcorrelativo.Text = dgvservicios.CurrentRow.Cells[13].Value.ToString();


        }

        private void groupservicio_Enter(object sender, EventArgs e)
        {

        }

        private void dgvmostrarservicios_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
          
            
        }

        private void txtidcolaborador_KeyUp(object sender, KeyEventArgs e)
        {
            conect.busquedaservicio(dgvservicios, txtidcolaborador, "COLABORADORESPM_id_colaboradorespm");
        }

        private void txtnombrec_KeyUp(object sender, KeyEventArgs e)
        {
            conect.busquedaservicio(dgvservicios, txtnombrec, "apellido_clientespm");
        }

        private void combotipo_KeyUp(object sender, KeyEventArgs e)
        {

        }

        private void combotipo_SelectedIndexChanged(object sender, EventArgs e)
        {
            conect.busquedaserviciotipo(dgvservicios, combotipo, "tipo_historialpm");

        }

        private void groupcolaborador_Enter(object sender, EventArgs e)
        {

        }
        int cont = 1;
        private void btncargarfotos_Click(object sender, EventArgs e)
        {
         
            try
                {
                int m = dtfecha.Value.Month;
                int d = dtfecha.Value.Day;
                int a = dtfecha.Value.Year;
                 ff = Convert.ToString(d) + "-" + Convert.ToString(m) + "-" + Convert.ToString(a);
                string mes = "";
                switch (m)
                {
                    case 1: mes = "Enero"; break;
                    case 2: mes = "Febrero"; break;
                    case 3: mes = "Marzo"; break;
                    case 4: mes = "Abril"; break;
                    case 5: mes = "Mayo"; break;
                    case 6: mes = "Junio"; break;
                    case 7: mes = "Julio"; break;
                    case 8: mes = "Agosto"; break;
                    case 9: mes = "Septiembre"; break;
                    case 10: mes = "Octubre"; break;
                    case 11: mes = "Noviembre"; break;
                    case 12: mes = "Diciembre"; break;
                }
                 escritorio = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
                string cliente = txtnombre.Text;
                //string carpeta = @"C:\Proyectos Multiples\" + mes;
                 carpeta = escritorio + "\\" + "PM Servicios" + "\\" + mes;

                 subcarpeta = carpeta + "\\" + lbnoservicio.Text + " " + cliente;

                if (!(Directory.Exists(carpeta)))
                {
                    Directory.CreateDirectory(carpeta);
                }
                if (!(Directory.Exists(subcarpeta)))
                {
                    Directory.CreateDirectory(subcarpeta);
                }



                //

                DateTime fecha1 = DateTime.Now;
                fecha1.Minute.ToString();
                // textBox1.Text = fecha1.Second.ToString();
              
                string extension;
              //  OpenFileDialog openFileDialog = new OpenFileDialog();
                openFileDialog.Multiselect = true;
                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {

                    bandera = 1;
                    
                }
            }
            catch (Exception ex)
            {
                FMessageBox obj = new FMessageBox("Error SE2: " + ex.Message, "¡Error de Conexión!", 1);
                if (obj.ShowDialog() == DialogResult.OK) ;
            }
           
        }

        private void txttipo_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
