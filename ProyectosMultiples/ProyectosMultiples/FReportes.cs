﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using System.Collections;

namespace ProyectosMultiples
{
    public partial class FReportes : Form
    {
        public FReportes()
        {
            InitializeComponent();
            DateTime fechaA = DateTime.Now;
            lbHoy.Text = fechaA.ToString("dd / MM / yyyy");
        }

        Conexion conect = new Conexion();//conectar con base de datos
        private void button1_Click(object sender, EventArgs e)
        {
            groupcotizaciones.Visible = true;
            groupservicios.Visible = false;
            groupingresos.Visible = false;
            Point direccion = new Point(60,120);
            groupcotizaciones.Location = direccion; 
        }

        private void button2_Click(object sender, EventArgs e)
        {
            groupcotizaciones.Visible = false;
            groupservicios.Visible = true;
            groupingresos.Visible = false;
        
            Point direccion = new Point(60, 120);
            groupservicios.Location = direccion;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            conect.actualizarUsuariosSistema(dgvUsuarios);
            groupcotizaciones.Visible = false;
            groupservicios.Visible = false;
            groupingresos.Visible = true;

            Point direccion = new Point(60, 120);
            groupingresos.Location = direccion;
            DateTime fecha = DateTime.Now;
            date1ingresos.Value = Convert.ToDateTime("01/01/"+fecha.Year+"");
        }

        private void combocotizacion_SelectedIndexChanged(object sender, EventArgs e)
        {
            datec1.Value = Convert.ToDateTime("01/"+DateTime.Now.Month+"/"+DateTime.Now.Year);
            conect.actualizarReporteCotizacion(dgvCotizaciones, "", "", "", "", 1);
            dgvCotizaciones.Columns[2].Visible = false; // Empresa
            dgvCotizaciones.Columns[4].Visible = false; // Telefono
            dgvCotizaciones.Columns[6].Visible = true; // Estado
            lbRi.Visible = false;
            lbRf.Visible = false;
            dtpRf.Visible = false;
            dtpRi.Visible = false;
           
            if (cbBusquedaC.Text=="Fecha")
            {
                lbRi.Visible = false;
                lbRf.Visible = false;
                dtpRf.Visible = false;
                dtpRi.Visible = false;
                lbrango1.Visible = true;
                lbrango2.Visible = true;
                datec1.Visible = true;
                datec2.Visible = true;
                this.ActiveControl = datec1;
                dgvCotizaciones.Columns[5].Visible = true; // Fecha
            }
            else
            {
                lbrango1.Visible = false;
                lbrango2.Visible = false;
                datec1.Visible = false;
                datec2.Visible = false;
                dgvCotizaciones.Columns[5].Visible = false; // Fecha
            }
            if(cbBusquedaC.Text=="Contacto")
            {
                lbRi.Visible = false;
                lbRf.Visible = false;
                dtpRf.Visible = false;
                dtpRi.Visible = false;
                lbapellido.Text = "Nombre del Contacto";
                lbapellido.Visible = true;
                txtcotizacion.Visible = true;
                txtcotizacion.Clear();
                this.ActiveControl = txtcotizacion;
            }
            else
            {
                lbapellido.Visible = false;
                txtcotizacion.Visible = false;
            }
            if(cbBusquedaC.Text=="Usuario")
            {
                lbRi.Visible = true;
                lbRf.Visible = true;
                dtpRf.Visible = true;
                dtpRi.Visible = true;
                dtpRi.Value = Convert.ToDateTime("01/" + DateTime.Now.Month + "/"+DateTime.Now.Year);
                lbusuario.Visible = true;
                txtusuario.Visible = true;
                txtusuario.Clear();
                this.ActiveControl = txtusuario;
                dgvCotizaciones.Columns[6].Visible = false; // Estado
                dgvCotizaciones.Columns[4].Visible = false;
                dgvCotizaciones.Columns[7].Visible = true; // Usuarios
            }
            else
            {
                lbusuario.Visible = false;
                txtusuario.Visible = false;
                dgvCotizaciones.Columns[7].Visible = false; // Usuarios
            }
            
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void comboservicios_SelectedIndexChanged(object sender, EventArgs e)
        {
            conect.actualizarReporteServicios(dgvServicios, "", "", "", "", 1);
            dgvServicios.Columns[3].Visible = false; // Fecha
            dgvServicios.Columns[4].Visible = true; // Tipo
            dgvServicios.Columns[5].Visible = false; // Colaborador

            if (cbBusqueda.Text == "Fecha")
            {
                dates1.Value = Convert.ToDateTime("01/"+ DateTime.Now.Month+"/"+DateTime.Now.Year);
                lbrango1s.Visible = true;
                lbrango2s.Visible = true;
                dates1.Visible = true;
                dates2.Visible = true;
                dgvServicios.Columns[3].Visible = true; // Fecha
                this.ActiveControl = dates1;
            }
            else
            {
                lbrango1s.Visible = false;
                lbrango2s.Visible = false;
                dates1.Visible = false;
                dates2.Visible = false;
                dgvServicios.Columns[3].Visible = false; // Fecha
            }
            if (cbBusqueda.Text=="Contacto")
            {
                lbusuarioservicio.Visible = true;
                txtusuarioservicio.Visible = true;
                txtusuarioservicio.Clear();
                this.ActiveControl = txtusuarioservicio;
            }
            else
            {
                lbusuarioservicio.Visible = false;
                txtusuarioservicio.Visible = false;
            }
            if(cbBusqueda.Text=="Colaborador")
            {
                lbcolaboradorservicios.Visible = true;
                txtcolaboradorservicios.Visible = true;
                txtcolaboradorservicios.Clear();
                this.ActiveControl = txtcolaboradorservicios;
                dgvServicios.Columns[4].Visible = false; // Tipo
                dgvServicios.Columns[5].Visible = true; // Colaborador
            }
            else
            {
                lbcolaboradorservicios.Visible = false;
                txtcolaboradorservicios.Visible = false;
                dgvServicios.Columns[4].Visible = true; // Tipo
                dgvServicios.Columns[5].Visible = false; // Colaborador
            }
        }

        private void Combotipo_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (cbIngresos.Text == "Todas")
            {
                lbr1ingresos.Visible = false;
                lbr2ingresos.Visible = false;
                date1ingresos.Visible = false;
                date2ingresos.Visible = false;
            }
            if (cbIngresos.Text == "Fecha")
            {
                lbr1ingresos.Visible = true;
                lbr2ingresos.Visible = true;
                date1ingresos.Visible = true;
                date2ingresos.Visible = true;
            }
            if (cbIngresos.Text=="Colaboradores")
            {
                date1ingresos.Value = Convert.ToDateTime("01/"+ DateTime.Now.Month +"/"+DateTime.Now.Year);
                lbr1ingresos.Visible = true;
                lbr2ingresos.Visible = true;
                date1ingresos.Visible = true;
                date2ingresos.Visible = true;
            }
            
        }

        // Funcion para determinar el total de Servicios
        public int totalServicios(DataGridView datos, string estado)
        {
            int total = 0;

            foreach (DataGridViewRow f in datos.Rows)
            {
                if (f.Cells[4].Value.ToString() == estado)
                {
                    total++;
                }
            }

            return total;
        }

        
        // Funcion para mostrar resultados en Grafica
        public void valoresGraficas()
        {
            ArrayList estado = new ArrayList();
            ArrayList cant = new ArrayList();
            int n1 = totalServicios(dgvServicios, "Instalación");
            int n2 = totalServicios(dgvServicios, "Mantenimiento Preventivo");
            int n3 = totalServicios(dgvServicios, "Mantenimiento Correctivo");
            int n4 = totalServicios(dgvServicios, "Garantia");
            int n5 = totalServicios(dgvServicios, "Otros");
            estado.Add("Instalación");
            estado.Add("M. Preventivo");
            estado.Add("M. Correctivo");
            estado.Add("Garantía");
            estado.Add("Otros");
            cant.Add(n1);
            cant.Add(n2);
            cant.Add(n3);
            cant.Add(n4);
            cant.Add(n5);

            chart4.Series[0].Points.DataBindXY(estado, cant);
            chart5.Series[0].Points.DataBindXY(estado, cant);
            lbTotal.Visible = true;
            lbTotal.Text = Convert.ToString(n1 + n2 + n3 + n4 + n5);
        }

        private void btnfiltrarservicios_Click(object sender, EventArgs e)
        {
            if (cbBusqueda.Text == "Todas")
            {
                conect.actualizarReporteServicios(dgvServicios, "", "", "", "", 1);
                dgvServicios.Columns[3].Visible = false; // Fecha
                dgvServicios.Columns[4].Visible = true; // Tipo
                dgvServicios.Columns[5].Visible = false; // Colaborador
                valoresGraficas();
            }

            if (cbBusqueda.Text == "Fecha")
            {
                conect.actualizarReporteServicios(dgvServicios, dates1.Value.ToString("dd/MM/yyyy"), dates2.Value.ToString("dd/MM/yyyy"), "", "", 2);
                dgvServicios.Columns[3].Visible = true; // Fecha
                dgvServicios.Columns[4].Visible = false; // Tipo
                dgvServicios.Columns[5].Visible = false; // Colaborador
                valoresGraficas();
            }

            if (cbBusqueda.Text == "Contacto")
            {
                conect.actualizarReporteServicios(dgvServicios, "", "", txtusuarioservicio.Text, "", 3);
                dgvServicios.Columns[3].Visible = false; // Fecha
                dgvServicios.Columns[4].Visible = true; // Tipo
                dgvServicios.Columns[5].Visible = false; // Colaborador
                valoresGraficas();
            }
            if (cbBusqueda.Text == "Colaborador")
            {
                conect.actualizarReporteServicios(dgvServicios, "", "", "", txtcolaboradorservicios.Text, 4);
                dgvServicios.Columns[3].Visible = false; // Fecha
                dgvServicios.Columns[4].Visible = false; // Tipo
                dgvServicios.Columns[5].Visible = true; // Colaborador
                valoresGraficas();
            }
        }

        // Funcion para determinar el total de Estados
        public int totalEstados(DataGridView datos, string estado)
        {
            int total = 0;

            foreach (DataGridViewRow f in datos.Rows)
            {
                if (f.Cells[6].Value.ToString() == estado)
                {
                    total++;
                }
            }

            return total;
        }

        // Funcion para determinar los valores de la grafica de Cotizaciones
        public void valoresGraficasC()
        {
            int n1 = totalEstados(dgvCotizaciones, "Pendiente");
            int n2 = totalEstados(dgvCotizaciones, "Vendida");
            int n3 = totalEstados(dgvCotizaciones, "Finalizada");

            ArrayList estado = new ArrayList();
            ArrayList cant = new ArrayList();
            estado.Add("Pendiente");
            estado.Add("Vendida");
            estado.Add("Finalizada");
            cant.Add(n1);
            cant.Add(n2);
            cant.Add(n3);
            chart1.Series[0].Points.DataBindXY(estado, cant);
            chart2.Series[0].Points.DataBindXY(estado, cant);
            lbTotal2.Visible = true;
            lbTotal2.Text = Convert.ToString(n1 + n2 + n3);

        }
        private void btnfiltrarcotizacion_Click(object sender, EventArgs e)
        {
            if (cbBusquedaC.Text == "Todas")
            {
                conect.actualizarReporteCotizacion(dgvCotizaciones, "","","","",1);
                dgvCotizaciones.Columns[2].Visible = false; // Empresa
                dgvCotizaciones.Columns[4].Visible = false; // Telefono
                dgvCotizaciones.Columns[5].Visible = false; // Fecha
                dgvCotizaciones.Columns[6].Visible = true; // Estado
                dgvCotizaciones.Columns[7].Visible = false; // Usuarios
                valoresGraficasC();
            }

            if (cbBusquedaC.Text == "Fecha")
            {
                conect.actualizarReporteCotizacion(dgvCotizaciones, datec1.Value.ToString("dd/MM/yyyy"), datec2.Value.ToString("dd/MM/yyyy"), "", "", 2);
                dgvCotizaciones.Columns[2].Visible = false; // Empresa
                dgvCotizaciones.Columns[4].Visible = false; // Telefono
                dgvCotizaciones.Columns[5].Visible = true; // Fecha
                dgvCotizaciones.Columns[6].Visible = false; // Estado
                dgvCotizaciones.Columns[7].Visible = false; // Usuarios
                valoresGraficasC();
            }

            if (cbBusquedaC.Text == "Contacto")
            {
                conect.actualizarReporteCotizacion(dgvCotizaciones, "", "",txtcotizacion.Text, "", 3);
                dgvCotizaciones.Columns[2].Visible = false; // Empresa
                dgvCotizaciones.Columns[4].Visible = false; // Telefono
                dgvCotizaciones.Columns[5].Visible = false; // Fecha
                dgvCotizaciones.Columns[6].Visible = true; // Estado
                dgvCotizaciones.Columns[7].Visible = false; // Usuarios
                valoresGraficasC();
            }

            if (cbBusquedaC.Text == "Usuario")
            {
                conect.actualizarReporteCotizacion(dgvCotizaciones, dtpRi.Value.ToString("dd/MM/yyyy"), dtpRf.Value.ToString("dd/MM/yyyy"), "", txtusuario.Text, 4);
                dgvCotizaciones.Columns[2].Visible = false; // Empresa
                dgvCotizaciones.Columns[4].Visible = false; // Telefono
                dgvCotizaciones.Columns[5].Visible = true; // Fecha
                dgvCotizaciones.Columns[6].Visible = false; // Estado
                dgvCotizaciones.Columns[7].Visible = true; // Usuarios
                valoresGraficasC();
            }
        }

        private void txtcotizacion_KeyUp(object sender, KeyEventArgs e)
        {
            conect.busquedaReportesCotizacion(dgvCotizaciones, txtcotizacion, "nombre_clientespm");
            dgvCotizaciones.Columns[2].Visible = false; // Empresa
            dgvCotizaciones.Columns[4].Visible = false; // Telefono
            dgvCotizaciones.Columns[5].Visible = false; // fecha
        }

        private void txtusuario_TextChanged(object sender, EventArgs e)
        {
            conect.busquedaReportesCotizacion(dgvCotizaciones, txtusuario, "nombre_usuariospm");
            dgvCotizaciones.Columns[2].Visible = false; // Empresa
            dgvCotizaciones.Columns[4].Visible = false; // Telefono
            dgvCotizaciones.Columns[5].Visible = false; // fecha
        }

        private void dgvCotizaciones_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (cbBusquedaC.Text == "Contacto")
            {
                txtcotizacion.Text = dgvCotizaciones.CurrentRow.Cells[1].Value.ToString();
            }
            if (cbBusquedaC.Text == "Usuario")
            {
                txtusuario.Text = dgvCotizaciones.CurrentRow.Cells[7].Value.ToString();
                dgvCotizaciones.Columns[4].Visible = false;
            }
        }

        private void txtcolaboradorservicios_KeyUp(object sender, KeyEventArgs e)
        {
            conect.busquedaReportesServicios(dgvServicios, txtcolaboradorservicios, "id_colaboradorespm");
            dgvServicios.Columns[3].Visible = false; // Fecha
            dgvServicios.Columns[4].Visible = false; // Tipo
        }

        private void txtusuarioservicio_KeyUp(object sender, KeyEventArgs e)
        {
            conect.busquedaReportesServicios(dgvServicios, txtusuarioservicio, "nombre_clientespm");
            dgvServicios.Columns[3].Visible = false; // Fecha
           // dgvServicios.Columns[4].Visible = false; // Tipo
        }

        private void dgvServicios_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (cbBusqueda.Text == "Contacto")
            {
                txtusuarioservicio.Text = dgvServicios.CurrentRow.Cells[1].Value.ToString();
            }
            if (cbBusqueda.Text == "Colaborador")
            {
                txtcolaboradorservicios.Text = dgvServicios.CurrentRow.Cells[5].Value.ToString();
            }
        }

        private void chart5_Click(object sender, EventArgs e)
        {

        }

        private void chart4_Click(object sender, EventArgs e)
        {

        }

        // Funcion para determinar el total de Ingresos por mes
        public double totalMeses(DataGridView datos, int m)
        {
            double total = 0;
            DateTime fechaV = DateTime.Now;
            foreach (DataGridViewRow f in datos.Rows)
            {
                fechaV = Convert.ToDateTime(f.Cells[5].Value);
                if (fechaV.Month == m)
                {
                    total += Convert.ToDouble(f.Cells[4].Value);
                }
            }

            return total;
        }

        // Funcion para determinar los totales de la grafica de Cotizaciones Vendidas

        public void valoresGraficasI(int ni, int nf)
        {
            ArrayList estado = new ArrayList();
            ArrayList cant = new ArrayList();
            chart6.Visible = true;
            groupBox5.Visible = false;
            dgvIngresos.Visible = false; 
            for (int x = ni; x <= nf; x++)
            {
                switch(x)
                {
                    case 1: estado.Add(x); cant.Add(totalMeses(dgvIngresos, x)); break;
                    case 2: estado.Add(x); cant.Add(totalMeses(dgvIngresos, x)); break;
                    case 3: estado.Add(x); cant.Add(totalMeses(dgvIngresos, x)); break;
                    case 4: estado.Add(x); cant.Add(totalMeses(dgvIngresos, x)); break;
                    case 5: estado.Add(x); cant.Add(totalMeses(dgvIngresos, x)); break;
                    case 6: estado.Add(x); cant.Add(totalMeses(dgvIngresos, x)); break;
                    case 7: estado.Add(x); cant.Add(totalMeses(dgvIngresos, x)); break;
                    case 8: estado.Add(x); cant.Add(totalMeses(dgvIngresos, x)); break;
                    case 9: estado.Add(x); cant.Add(totalMeses(dgvIngresos, x)); break;
                    case 10: estado.Add(x); cant.Add(totalMeses(dgvIngresos, x)); break;
                    case 11: estado.Add(x); cant.Add(totalMeses(dgvIngresos, x)); break;
                    case 12: estado.Add(x); cant.Add(totalMeses(dgvIngresos, x)); break;
                }
            }
            chart6.Series[0].Points.DataBindXY(estado, cant);

        }
        private void button5_Click(object sender, EventArgs e)
        {
            if (cbIngresos.Text == "Todas")
            {
                conect.actualizarReporteCotizacion(dgvIngresos, "", "", "", "", 5);
                dgvIngresos.Columns[2].Visible = false; // Empresa
                dgvIngresos.Columns[4].Visible = true; // Total
                dgvIngresos.Columns[5].Visible = true; // Fecha
                dgvIngresos.Columns[6].Visible = false; // Estado
                dgvIngresos.Columns[7].Visible = true; // Usuarios
                valoresGraficasI(1,12);
            }

            if (cbIngresos.Text == "Colaboradores")
            {
                conect.actualizarReporteCotizacion(dgvIngresos, date1ingresos.Value.ToString("dd/MM/yyyy"), date2ingresos.Value.ToString("dd/MM/yyyy"), "", "", 5);
                dgvIngresos.Columns[2].Visible = false; // Empresa
                dgvIngresos.Columns[4].Visible = true; // Total
                dgvIngresos.Columns[5].Visible = true; // Fecha
                dgvIngresos.Columns[6].Visible = false; // Estado
                dgvIngresos.Columns[7].Visible = true; // Usuarios

                ArrayList estado = new ArrayList();
                ArrayList cant = new ArrayList();
                string colaborador = "";
                foreach (DataGridViewRow f in dgvUsuarios.Rows)
                {
                    colaborador = Convert.ToString(f.Cells[1].Value);
                    estado.Add(colaborador);
                    double totalCot = 0;
                    foreach (DataGridViewRow fi in dgvIngresos.Rows)
                    {
                        if (colaborador == fi.Cells[7].Value.ToString())
                        {
                            totalCot += Convert.ToDouble(fi.Cells[4].Value);
                        }
                    }
                    cant.Add(totalCot);
                }
                chart3.Series[0].Points.DataBindXY(estado, cant);
                chart3.Visible = true;
                groupBox5.Visible = false;
                dgvIngresos.Visible = false; 
            }

            if (cbIngresos.Text == "Fecha")
            {
                conect.actualizarReporteCotizacion(dgvIngresos, date1ingresos.Value.ToString("dd/MM/yyyy"), date2ingresos.Value.ToString("dd/MM/yyyy"),"", "", 6);
                dgvIngresos.Columns[2].Visible = false; // Empresa
                dgvIngresos.Columns[4].Visible = true; // Total
                dgvIngresos.Columns[5].Visible = true; // Fecha
                dgvIngresos.Columns[6].Visible = false; // Estado
                dgvIngresos.Columns[7].Visible = true; // Usuarios
                valoresGraficasI(date1ingresos.Value.Month, date2ingresos.Value.Month);
            }
        }

        private void chart6_Click(object sender, EventArgs e)
        {
            chart6.Visible = false;
            groupBox5.Visible = true;
            dgvIngresos.Visible = true;
        }

        private void groupBox5_Enter(object sender, EventArgs e)
        {

        }

        private void chart3_Click(object sender, EventArgs e)
        {
            chart3.Visible = false;
            groupBox5.Visible = true;
            dgvIngresos.Visible = true;
        }
    }
}
