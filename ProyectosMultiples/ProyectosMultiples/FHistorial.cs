﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;

namespace ProyectosMultiples
{
    public partial class FHistorial : Form
    {
        public FHistorial()
        {
            InitializeComponent();
        } 
        Conexion conect = new Conexion();
        static public string idCotizacion;
        static public string estadoCotizacion; 
        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }

        private void lbid_Click(object sender, EventArgs e)
        {

        }

        private void Btnhistorial_Click(object sender, EventArgs e)
        {
            Form1.acceso = 5;
        }

        private void FHistorial_Load(object sender, EventArgs e)
        {
            try
            {
                conect.actualizarCotizacion(dgvCotizaciones,cbestado.Text);
                dgvCotizaciones.Columns[2].Visible = false; // Empresa
                dgvCotizaciones.Columns[7].Visible = false; // Fecha Futuro
                this.ActiveControl = txtnombre;
            }
            catch (Exception ex)
            {
                FMessageBox obj = new FMessageBox("Error H1: " + ex.Message, "¡Error de Conexión!", 1);
                if (obj.ShowDialog() == DialogResult.OK) ;
            }
        }

        private void txtnombre_KeyUp(object sender, KeyEventArgs e)
        {
            txtnit.Clear();
            conect.busquedaCotizaciones(dgvCotizaciones, txtnombre, "nombre_clientespm", cbestado.Text);
        }

        private void txtnit_KeyUp(object sender, KeyEventArgs e)
        {
            txtnombre.Clear();
            conect.busquedaCotizaciones(dgvCotizaciones, txtnit, "nit_clientespm",cbestado.Text);
        }

        private void cbestado_SelectedIndexChanged(object sender, EventArgs e)
        {
            conect.actualizarCotizacion(dgvCotizaciones, cbestado.Text);
            txtnombre.Clear();
            txtnit.Clear();
            this.ActiveControl = txtnombre;
        }
        private void dgvCotizaciones_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                DateTime fechaCotizacion;
                FAlertasOpciones obj = new FAlertasOpciones();
                FAlertasOpciones.no = dgvCotizaciones.CurrentRow.Cells[0].Value.ToString();
                FAlertasOpciones.nombre = dgvCotizaciones.CurrentRow.Cells[1].Value.ToString();
                FAlertasOpciones.empresa = dgvCotizaciones.CurrentRow.Cells[2].Value.ToString();
                FAlertasOpciones.nit = dgvCotizaciones.CurrentRow.Cells[3].Value.ToString();
                FAlertasOpciones.fecha = dgvCotizaciones.CurrentRow.Cells[4].Value.ToString();
                FAlertasOpciones.estado = dgvCotizaciones.CurrentRow.Cells[5].Value.ToString();
                FAlertasOpciones.atendido = dgvCotizaciones.CurrentRow.Cells[6].Value.ToString();
                fechaCotizacion = Convert.ToDateTime(dgvCotizaciones.CurrentRow.Cells[7].Value);
                FAlertasOpciones.telefono = dgvCotizaciones.CurrentRow.Cells[8].Value.ToString();
                FAlertasOpciones.email = dgvCotizaciones.CurrentRow.Cells[9].Value.ToString();
                FAlertasOpciones.postergar = false; 
                if (obj.ShowDialog() == DialogResult.OK)
                {
                    DateTime fechaHoy = DateTime.Now;
                    conect.modificarEstadoC(idCotizacion,estadoCotizacion);
                    conect.actualizarCotizacion(dgvCotizaciones, cbestado.Text);
                    dgvCotizaciones.Columns[2].Visible = false;
                    dgvCotizaciones.Columns[7].Visible = false;
                }
            }
            catch (Exception ex)
            {
                FMessageBox obj = new FMessageBox("Error H2: " + ex.Message, "¡Error de Conexión!", 1);
                if (obj.ShowDialog() == DialogResult.OK) ;
            }
        }
        
    }
}
