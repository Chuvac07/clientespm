﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ProyectosMultiples
{
    public partial class FAlertasOpciones : Form
    {

        static public string no;
        static public string nombre;
        static public string empresa;
        static public string nit;
        static public string fecha;
        static public string estado;
        static public string atendido;
        static public string telefono;
        static public string email;
        static public bool postergar = true; 

        public FAlertasOpciones()
        {
            InitializeComponent();
        }

        Conexion conect = new Conexion();
        private void FAlertasOpciones_Load(object sender, EventArgs e)
        {
            lbNo.Text = no;
            lbNombre.Text = nombre;
            lbEmpresa.Text = empresa;
            if (nombre == empresa)
            {
                lbEmpresa.Visible = false;
                label4.Visible = false; 
            }
            else
            {
                lbEmpresa.Visible = true;
                label4.Visible = true;
            }
            if (!postergar)
            {
                btnPost.Visible = false; 
            }
            lbNit.Text = nit;
            lbFecha.Text = fecha;
            cbEstado.Text = estado;
            lbTelefono.Text = telefono;
            lbEmail.Text = email;
            lbAt.Text = "Atendido por " + atendido;
            conect.actualizarProductoC(dgvProductos, no);
            lbTotal.Text = Convert.ToString(totalVentas(dgvProductos, "Sub-Total"));
            this.ActiveControl = btnFinalizar;
        }

        public double totalVentas(DataGridView datos, string columna)
        {
            double total = 0;

            foreach (DataGridViewRow f in datos.Rows)
            {
                total += Convert.ToDouble(f.Cells[columna].Value);
            }

            return total;
        }

        private void btnFinalizar_Click(object sender, EventArgs e)
        {
            FHistorial.estadoCotizacion = cbEstado.Text;
            FHistorial.idCotizacion = lbNo.Text;
            postergar = true;
            DialogResult = DialogResult.OK;
        }

        private void cbEstado_SelectedIndexChanged(object sender, EventArgs e)
        {
            estado = cbEstado.Text;
        }

        int c = 0;
        private void btnPost_Click(object sender, EventArgs e)
        {
            c++;
            if (c % 2 == 1)
            {
                Alertas.activar = true;
                btnPost.BackColor = Color.LightGreen;
            }
            else
            {
                Alertas.activar = false;
                btnPost.BackColor = btnFinalizar.BackColor;
            }
        }
    }
}
