﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ProyectosMultiples
{
    public partial class Alertas : Form
    {
        public Alertas()
        {
            InitializeComponent();
        }

        private void btnuevaalerta_Click(object sender, EventArgs e)
        {
            btnatras.Visible = true;
            btnuevaalerta.Visible = false;
            groupalertas.Visible = false;
            groupnuevaalerta.Visible = true;
            Point direccion = new Point(215,200);
            groupnuevaalerta.Location = direccion;
        }

        private void btnatras_Click(object sender, EventArgs e)
        {
            btnatras.Visible = false;
            btnuevaalerta.Visible = true;
            groupnuevaalerta.Visible = false;
            groupalertas.Visible = true;
        }

        static public bool activar = false;
        Conexion conect = new Conexion();
        private void Alertas_Load(object sender, EventArgs e)
        {
            try
            {
                DateTime fechaHoy = DateTime.Now;
                lbHoy.Text = fechaHoy.ToString("dd/MM/yyyy");
                fechaHoy = fechaHoy.AddDays(5);
                conect.actualizarCotizacionAlertas(dgvCotizaciones, fechaHoy.ToString("dd/MM/yyyy"));
                dgvCotizaciones.Columns[2].Visible = false;
                dgvCotizaciones.Columns[7].Visible = false;
                dgvCotizaciones.Columns[8].Visible = false;
                dgvCotizaciones.Columns[9].Visible = false;
                conect.actualizarAlertasSerivicios(dgvSerivicios, fechaHoy.ToString("dd/MM/yyyy"));
                dgvSerivicios.Columns[2].Visible = false;
                dgvSerivicios.Columns[3].Visible = false;
                dgvSerivicios.Columns[7].Visible = false;
            }
            catch (Exception ex)
            {
                FMessageBox obj = new FMessageBox("Error A1: " + ex.Message, "¡Error de Conexión!", 1);
                if (obj.ShowDialog() == DialogResult.OK) ;
            }
        }

        private void dgvCotizaciones_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                DateTime fechaCotizacion; 
                FAlertasOpciones obj = new FAlertasOpciones();
                FAlertasOpciones.no = dgvCotizaciones.CurrentRow.Cells[0].Value.ToString();
                FAlertasOpciones.nombre = dgvCotizaciones.CurrentRow.Cells[1].Value.ToString();
                FAlertasOpciones.empresa = dgvCotizaciones.CurrentRow.Cells[2].Value.ToString();
                FAlertasOpciones.nit = dgvCotizaciones.CurrentRow.Cells[3].Value.ToString();
                FAlertasOpciones.fecha = dgvCotizaciones.CurrentRow.Cells[4].Value.ToString();
                FAlertasOpciones.estado = dgvCotizaciones.CurrentRow.Cells[5].Value.ToString();
                FAlertasOpciones.atendido = dgvCotizaciones.CurrentRow.Cells[6].Value.ToString();
                fechaCotizacion = Convert.ToDateTime(dgvCotizaciones.CurrentRow.Cells[7].Value);
                FAlertasOpciones.telefono = dgvCotizaciones.CurrentRow.Cells[8].Value.ToString();
                FAlertasOpciones.email = dgvCotizaciones.CurrentRow.Cells[9].Value.ToString();

                if (obj.ShowDialog() == DialogResult.OK)
                {
                    DateTime fechaHoy = DateTime.Now;
                    fechaHoy = fechaHoy.AddDays(5);
                    if (activar)
                    {
                        conect.modificarEstadoAlertas(FAlertasOpciones.no, FAlertasOpciones.estado, fechaCotizacion.AddDays(15).ToString());
                    }
                    else
                    {
                        conect.modificarEstadoAlertas(FAlertasOpciones.no, FAlertasOpciones.estado, fechaCotizacion.ToString());
                    }
                    activar = false;
                    conect.actualizarCotizacionAlertas(dgvCotizaciones, fechaHoy.ToString("dd/MM/yyyy"));
                    dgvCotizaciones.Columns[7].Visible = false;
                    dgvCotizaciones.Columns[8].Visible = false;
                    dgvCotizaciones.Columns[9].Visible = false;
                }
            }
            catch (Exception ex)
            {
                FMessageBox obj = new FMessageBox("Error A2: " + ex.Message, "¡Error de Conexión!", 1);
                if (obj.ShowDialog() == DialogResult.OK) ;
            }
        }

        private void dgvSerivicios_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                DateTime fechaCotizacion;
                FAlertasOpciones2 obj = new FAlertasOpciones2();
                FAlertasOpciones2.no = dgvSerivicios.CurrentRow.Cells[0].Value.ToString();
                FAlertasOpciones2.nombre = dgvSerivicios.CurrentRow.Cells[1].Value.ToString();
                FAlertasOpciones2.empresa = dgvSerivicios.CurrentRow.Cells[2].Value.ToString();
                FAlertasOpciones2.nit = dgvSerivicios.CurrentRow.Cells[3].Value.ToString();
                FAlertasOpciones2.telefono = dgvSerivicios.CurrentRow.Cells[4].Value.ToString();
                FAlertasOpciones2.fecha = dgvSerivicios.CurrentRow.Cells[5].Value.ToString();
                fechaCotizacion = Convert.ToDateTime(dgvSerivicios.CurrentRow.Cells[5].Value);
                FAlertasOpciones2.descripcion = dgvSerivicios.CurrentRow.Cells[6].Value.ToString();
                FAlertasOpciones2.email = dgvSerivicios.CurrentRow.Cells[7].Value.ToString();

                if (obj.ShowDialog() == DialogResult.OK)
                {
                    DateTime fechaHoy = DateTime.Now;
                    fechaHoy = fechaHoy.AddDays(5);

                    conect.actualizarAlertasSerivicios(dgvSerivicios, fechaHoy.ToString("dd/MM/yyyy"));
                    dgvSerivicios.Columns[2].Visible = false;
                    dgvSerivicios.Columns[3].Visible = false;
                    dgvSerivicios.Columns[7].Visible = false;
                }
            }
            catch (Exception ex)
            {
                FMessageBox obj = new FMessageBox("Error A3: " + ex.Message, "¡Error de Conexión!", 1);
                if (obj.ShowDialog() == DialogResult.OK) ;
            }
        }
    }
}
