﻿namespace ProyectosMultiples
{
    partial class FReportes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea13 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend7 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series13 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea14 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series14 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea15 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series15 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea16 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend8 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series16 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle21 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle22 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea17 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend9 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series17 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea18 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series18 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle23 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle24 = new System.Windows.Forms.DataGridViewCellStyle();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lbHoy = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.groupcotizaciones = new System.Windows.Forms.GroupBox();
            this.lbTotal2 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.lbRi = new System.Windows.Forms.Label();
            this.dtpRi = new System.Windows.Forms.DateTimePicker();
            this.lbRf = new System.Windows.Forms.Label();
            this.dtpRf = new System.Windows.Forms.DateTimePicker();
            this.lbusuario = new System.Windows.Forms.Label();
            this.txtusuario = new System.Windows.Forms.TextBox();
            this.lbapellido = new System.Windows.Forms.Label();
            this.txtcotizacion = new System.Windows.Forms.TextBox();
            this.lbrango2 = new System.Windows.Forms.Label();
            this.lbrango1 = new System.Windows.Forms.Label();
            this.datec2 = new System.Windows.Forms.DateTimePicker();
            this.datec1 = new System.Windows.Forms.DateTimePicker();
            this.btnfiltrarcotizacion = new System.Windows.Forms.Button();
            this.cbBusquedaC = new System.Windows.Forms.ComboBox();
            this.dgvCotizaciones = new System.Windows.Forms.DataGridView();
            this.chart2 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.groupservicios = new System.Windows.Forms.GroupBox();
            this.lbTotal = new System.Windows.Forms.Label();
            this.dgvServicios = new System.Windows.Forms.DataGridView();
            this.chart5 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.dates1 = new System.Windows.Forms.DateTimePicker();
            this.txtcolaboradorservicios = new System.Windows.Forms.TextBox();
            this.lbcolaboradorservicios = new System.Windows.Forms.Label();
            this.lbusuarioservicio = new System.Windows.Forms.Label();
            this.txtusuarioservicio = new System.Windows.Forms.TextBox();
            this.lbtipo = new System.Windows.Forms.Label();
            this.cbBusqueda = new System.Windows.Forms.ComboBox();
            this.lbrango2s = new System.Windows.Forms.Label();
            this.lbrango1s = new System.Windows.Forms.Label();
            this.dates2 = new System.Windows.Forms.DateTimePicker();
            this.btnfiltrarservicios = new System.Windows.Forms.Button();
            this.chart4 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.groupingresos = new System.Windows.Forms.GroupBox();
            this.dgvIngresos = new System.Windows.Forms.DataGridView();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.lbr2ingresos = new System.Windows.Forms.Label();
            this.lbr1ingresos = new System.Windows.Forms.Label();
            this.date2ingresos = new System.Windows.Forms.DateTimePicker();
            this.date1ingresos = new System.Windows.Forms.DateTimePicker();
            this.button5 = new System.Windows.Forms.Button();
            this.cbIngresos = new System.Windows.Forms.ComboBox();
            this.chart3 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.chart6 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.dgvUsuarios = new System.Windows.Forms.DataGridView();
            this.groupBox1.SuspendLayout();
            this.groupcotizaciones.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCotizaciones)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            this.groupservicios.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvServicios)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart5)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart4)).BeginInit();
            this.groupingresos.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvIngresos)).BeginInit();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvUsuarios)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lbHoy);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.button3);
            this.groupBox1.Controls.Add(this.button2);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.ForeColor = System.Drawing.Color.White;
            this.groupBox1.Location = new System.Drawing.Point(80, 30);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox1.Size = new System.Drawing.Size(1105, 99);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Consultas";
            // 
            // lbHoy
            // 
            this.lbHoy.AutoSize = true;
            this.lbHoy.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbHoy.ForeColor = System.Drawing.Color.Turquoise;
            this.lbHoy.Location = new System.Drawing.Point(874, 33);
            this.lbHoy.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbHoy.Name = "lbHoy";
            this.lbHoy.Size = new System.Drawing.Size(92, 31);
            this.lbHoy.TabIndex = 27;
            this.lbHoy.Text = "--/--/--";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(770, 33);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(112, 31);
            this.label8.TabIndex = 26;
            this.label8.Text = "Fecha: ";
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(66)))), ((int)(((byte)(82)))));
            this.button3.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(80)))), ((int)(((byte)(200)))));
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Image = global::ProyectosMultiples.Properties.Resources.p21;
            this.button3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button3.Location = new System.Drawing.Point(528, 27);
            this.button3.Margin = new System.Windows.Forms.Padding(4);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(220, 50);
            this.button3.TabIndex = 2;
            this.button3.Text = "Ventas";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(66)))), ((int)(((byte)(82)))));
            this.button2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(80)))), ((int)(((byte)(200)))));
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Image = global::ProyectosMultiples.Properties.Resources.compras;
            this.button2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button2.Location = new System.Drawing.Point(286, 27);
            this.button2.Margin = new System.Windows.Forms.Padding(4);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(220, 50);
            this.button2.TabIndex = 1;
            this.button2.Text = "Servicios";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(66)))), ((int)(((byte)(82)))));
            this.button1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(80)))), ((int)(((byte)(200)))));
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Image = global::ProyectosMultiples.Properties.Resources.p9;
            this.button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button1.Location = new System.Drawing.Point(36, 27);
            this.button1.Margin = new System.Windows.Forms.Padding(4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(220, 50);
            this.button1.TabIndex = 0;
            this.button1.Text = "Cotizaciones";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // groupcotizaciones
            // 
            this.groupcotizaciones.Controls.Add(this.lbTotal2);
            this.groupcotizaciones.Controls.Add(this.groupBox4);
            this.groupcotizaciones.Controls.Add(this.dgvCotizaciones);
            this.groupcotizaciones.Controls.Add(this.chart2);
            this.groupcotizaciones.Controls.Add(this.chart1);
            this.groupcotizaciones.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupcotizaciones.ForeColor = System.Drawing.Color.White;
            this.groupcotizaciones.Location = new System.Drawing.Point(80, 137);
            this.groupcotizaciones.Margin = new System.Windows.Forms.Padding(4);
            this.groupcotizaciones.Name = "groupcotizaciones";
            this.groupcotizaciones.Padding = new System.Windows.Forms.Padding(4);
            this.groupcotizaciones.Size = new System.Drawing.Size(1105, 650);
            this.groupcotizaciones.TabIndex = 1;
            this.groupcotizaciones.TabStop = false;
            this.groupcotizaciones.Text = "Cotizaciones";
            this.groupcotizaciones.Visible = false;
            // 
            // lbTotal2
            // 
            this.lbTotal2.AutoSize = true;
            this.lbTotal2.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTotal2.Location = new System.Drawing.Point(30, 87);
            this.lbTotal2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbTotal2.Name = "lbTotal2";
            this.lbTotal2.Size = new System.Drawing.Size(66, 32);
            this.lbTotal2.TabIndex = 57;
            this.lbTotal2.Text = "000";
            this.lbTotal2.Visible = false;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.lbRi);
            this.groupBox4.Controls.Add(this.dtpRi);
            this.groupBox4.Controls.Add(this.lbRf);
            this.groupBox4.Controls.Add(this.dtpRf);
            this.groupBox4.Controls.Add(this.lbusuario);
            this.groupBox4.Controls.Add(this.txtusuario);
            this.groupBox4.Controls.Add(this.lbapellido);
            this.groupBox4.Controls.Add(this.txtcotizacion);
            this.groupBox4.Controls.Add(this.lbrango2);
            this.groupBox4.Controls.Add(this.lbrango1);
            this.groupBox4.Controls.Add(this.datec2);
            this.groupBox4.Controls.Add(this.datec1);
            this.groupBox4.Controls.Add(this.btnfiltrarcotizacion);
            this.groupBox4.Controls.Add(this.cbBusquedaC);
            this.groupBox4.ForeColor = System.Drawing.Color.White;
            this.groupBox4.Location = new System.Drawing.Point(516, 30);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox4.Size = new System.Drawing.Size(551, 197);
            this.groupBox4.TabIndex = 5;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Filtros";
            // 
            // lbRi
            // 
            this.lbRi.AutoSize = true;
            this.lbRi.Location = new System.Drawing.Point(219, 112);
            this.lbRi.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbRi.Name = "lbRi";
            this.lbRi.Size = new System.Drawing.Size(113, 20);
            this.lbRi.TabIndex = 15;
            this.lbRi.Text = "Rango inicio";
            this.lbRi.Visible = false;
            // 
            // dtpRi
            // 
            this.dtpRi.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpRi.Location = new System.Drawing.Point(223, 137);
            this.dtpRi.Margin = new System.Windows.Forms.Padding(4);
            this.dtpRi.Name = "dtpRi";
            this.dtpRi.Size = new System.Drawing.Size(136, 26);
            this.dtpRi.TabIndex = 14;
            this.dtpRi.Visible = false;
            // 
            // lbRf
            // 
            this.lbRf.AutoSize = true;
            this.lbRf.Location = new System.Drawing.Point(381, 112);
            this.lbRf.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbRf.Name = "lbRf";
            this.lbRf.Size = new System.Drawing.Size(104, 20);
            this.lbRf.TabIndex = 13;
            this.lbRf.Text = "Rango final";
            this.lbRf.Visible = false;
            // 
            // dtpRf
            // 
            this.dtpRf.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpRf.Location = new System.Drawing.Point(385, 137);
            this.dtpRf.Margin = new System.Windows.Forms.Padding(4);
            this.dtpRf.Name = "dtpRf";
            this.dtpRf.Size = new System.Drawing.Size(136, 26);
            this.dtpRf.TabIndex = 12;
            this.dtpRf.Visible = false;
            // 
            // lbusuario
            // 
            this.lbusuario.AutoSize = true;
            this.lbusuario.Location = new System.Drawing.Point(219, 42);
            this.lbusuario.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbusuario.Name = "lbusuario";
            this.lbusuario.Size = new System.Drawing.Size(99, 20);
            this.lbusuario.TabIndex = 11;
            this.lbusuario.Text = "ID Usuario";
            this.lbusuario.Visible = false;
            // 
            // txtusuario
            // 
            this.txtusuario.Location = new System.Drawing.Point(223, 69);
            this.txtusuario.Margin = new System.Windows.Forms.Padding(4);
            this.txtusuario.Name = "txtusuario";
            this.txtusuario.Size = new System.Drawing.Size(228, 26);
            this.txtusuario.TabIndex = 10;
            this.txtusuario.Visible = false;
            this.txtusuario.TextChanged += new System.EventHandler(this.txtusuario_TextChanged);
            // 
            // lbapellido
            // 
            this.lbapellido.AutoSize = true;
            this.lbapellido.Location = new System.Drawing.Point(219, 42);
            this.lbapellido.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbapellido.Name = "lbapellido";
            this.lbapellido.Size = new System.Drawing.Size(141, 20);
            this.lbapellido.TabIndex = 10;
            this.lbapellido.Text = "Apellido Cliente";
            this.lbapellido.Visible = false;
            // 
            // txtcotizacion
            // 
            this.txtcotizacion.Location = new System.Drawing.Point(223, 69);
            this.txtcotizacion.Margin = new System.Windows.Forms.Padding(4);
            this.txtcotizacion.Name = "txtcotizacion";
            this.txtcotizacion.Size = new System.Drawing.Size(228, 26);
            this.txtcotizacion.TabIndex = 9;
            this.txtcotizacion.Visible = false;
            this.txtcotizacion.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtcotizacion_KeyUp);
            // 
            // lbrango2
            // 
            this.lbrango2.AutoSize = true;
            this.lbrango2.Location = new System.Drawing.Point(219, 112);
            this.lbrango2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbrango2.Name = "lbrango2";
            this.lbrango2.Size = new System.Drawing.Size(104, 20);
            this.lbrango2.TabIndex = 8;
            this.lbrango2.Text = "Rango final";
            this.lbrango2.Visible = false;
            // 
            // lbrango1
            // 
            this.lbrango1.AutoSize = true;
            this.lbrango1.Location = new System.Drawing.Point(219, 42);
            this.lbrango1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbrango1.Name = "lbrango1";
            this.lbrango1.Size = new System.Drawing.Size(118, 20);
            this.lbrango1.TabIndex = 7;
            this.lbrango1.Text = "Rango inicial";
            this.lbrango1.Visible = false;
            // 
            // datec2
            // 
            this.datec2.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.datec2.Location = new System.Drawing.Point(223, 137);
            this.datec2.Margin = new System.Windows.Forms.Padding(4);
            this.datec2.Name = "datec2";
            this.datec2.Size = new System.Drawing.Size(136, 26);
            this.datec2.TabIndex = 6;
            this.datec2.Visible = false;
            // 
            // datec1
            // 
            this.datec1.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.datec1.Location = new System.Drawing.Point(223, 69);
            this.datec1.Margin = new System.Windows.Forms.Padding(4);
            this.datec1.Name = "datec1";
            this.datec1.Size = new System.Drawing.Size(136, 26);
            this.datec1.TabIndex = 5;
            this.datec1.Visible = false;
            // 
            // btnfiltrarcotizacion
            // 
            this.btnfiltrarcotizacion.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(66)))), ((int)(((byte)(82)))));
            this.btnfiltrarcotizacion.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnfiltrarcotizacion.Image = global::ProyectosMultiples.Properties.Resources.p6;
            this.btnfiltrarcotizacion.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnfiltrarcotizacion.Location = new System.Drawing.Point(32, 95);
            this.btnfiltrarcotizacion.Margin = new System.Windows.Forms.Padding(4);
            this.btnfiltrarcotizacion.Name = "btnfiltrarcotizacion";
            this.btnfiltrarcotizacion.Size = new System.Drawing.Size(160, 68);
            this.btnfiltrarcotizacion.TabIndex = 4;
            this.btnfiltrarcotizacion.Text = "Filtrar";
            this.btnfiltrarcotizacion.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnfiltrarcotizacion.UseVisualStyleBackColor = false;
            this.btnfiltrarcotizacion.Click += new System.EventHandler(this.btnfiltrarcotizacion_Click);
            // 
            // cbBusquedaC
            // 
            this.cbBusquedaC.FormattingEnabled = true;
            this.cbBusquedaC.Items.AddRange(new object[] {
            "Todas",
            "Fecha",
            "Contacto",
            "Usuario"});
            this.cbBusquedaC.Location = new System.Drawing.Point(32, 42);
            this.cbBusquedaC.Margin = new System.Windows.Forms.Padding(4);
            this.cbBusquedaC.Name = "cbBusquedaC";
            this.cbBusquedaC.Size = new System.Drawing.Size(160, 28);
            this.cbBusquedaC.TabIndex = 3;
            this.cbBusquedaC.Text = "Todas";
            this.cbBusquedaC.SelectedIndexChanged += new System.EventHandler(this.combocotizacion_SelectedIndexChanged);
            // 
            // dgvCotizaciones
            // 
            this.dgvCotizaciones.AllowUserToAddRows = false;
            this.dgvCotizaciones.AllowUserToDeleteRows = false;
            this.dgvCotizaciones.AllowUserToOrderColumns = true;
            this.dgvCotizaciones.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.dgvCotizaciones.BackgroundColor = System.Drawing.Color.WhiteSmoke;
            this.dgvCotizaciones.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvCotizaciones.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            this.dgvCotizaciones.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableWithoutHeaderText;
            dataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle17.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(32)))), ((int)(((byte)(40)))));
            dataGridViewCellStyle17.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle17.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle17.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle17.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle17.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvCotizaciones.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle17;
            this.dgvCotizaciones.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCotizaciones.Cursor = System.Windows.Forms.Cursors.Hand;
            this.dgvCotizaciones.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.dgvCotizaciones.EnableHeadersVisualStyles = false;
            this.dgvCotizaciones.GridColor = System.Drawing.Color.Black;
            this.dgvCotizaciones.Location = new System.Drawing.Point(516, 251);
            this.dgvCotizaciones.Margin = new System.Windows.Forms.Padding(4);
            this.dgvCotizaciones.MultiSelect = false;
            this.dgvCotizaciones.Name = "dgvCotizaciones";
            this.dgvCotizaciones.ReadOnly = true;
            this.dgvCotizaciones.RowHeadersVisible = false;
            dataGridViewCellStyle18.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle18.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(32)))), ((int)(((byte)(40)))));
            dataGridViewCellStyle18.SelectionBackColor = System.Drawing.Color.SteelBlue;
            dataGridViewCellStyle18.SelectionForeColor = System.Drawing.Color.White;
            this.dgvCotizaciones.RowsDefaultCellStyle = dataGridViewCellStyle18;
            this.dgvCotizaciones.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvCotizaciones.Size = new System.Drawing.Size(551, 370);
            this.dgvCotizaciones.TabIndex = 52;
            this.dgvCotizaciones.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvCotizaciones_CellDoubleClick);
            // 
            // chart2
            // 
            this.chart2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.chart2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(32)))), ((int)(((byte)(40)))));
            chartArea13.Name = "ChartArea1";
            chartArea13.Visible = false;
            this.chart2.ChartAreas.Add(chartArea13);
            this.chart2.Cursor = System.Windows.Forms.Cursors.Hand;
            legend7.Alignment = System.Drawing.StringAlignment.Center;
            legend7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(32)))), ((int)(((byte)(40)))));
            legend7.Docking = System.Windows.Forms.DataVisualization.Charting.Docking.Left;
            legend7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            legend7.ForeColor = System.Drawing.Color.White;
            legend7.IsTextAutoFit = false;
            legend7.LegendStyle = System.Windows.Forms.DataVisualization.Charting.LegendStyle.Row;
            legend7.Name = "Legend1";
            legend7.TableStyle = System.Windows.Forms.DataVisualization.Charting.LegendTableStyle.Wide;
            this.chart2.Legends.Add(legend7);
            this.chart2.Location = new System.Drawing.Point(7, 26);
            this.chart2.Name = "chart2";
            series13.BorderWidth = 5;
            series13.ChartArea = "ChartArea1";
            series13.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Doughnut;
            series13.IsValueShownAsLabel = true;
            series13.LabelForeColor = System.Drawing.Color.White;
            series13.Legend = "Legend1";
            series13.MarkerBorderWidth = 3;
            series13.Name = "Series1";
            this.chart2.Series.Add(series13);
            this.chart2.Size = new System.Drawing.Size(1091, 91);
            this.chart2.TabIndex = 56;
            this.chart2.Text = "chart2";
            // 
            // chart1
            // 
            this.chart1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.chart1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(32)))), ((int)(((byte)(40)))));
            chartArea14.AlignmentOrientation = System.Windows.Forms.DataVisualization.Charting.AreaAlignmentOrientations.Horizontal;
            chartArea14.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(32)))), ((int)(((byte)(40)))));
            chartArea14.BorderColor = System.Drawing.Color.White;
            chartArea14.Name = "ChartArea1";
            this.chart1.ChartAreas.Add(chartArea14);
            this.chart1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chart1.Location = new System.Drawing.Point(16, 91);
            this.chart1.Name = "chart1";
            series14.BorderWidth = 5;
            series14.ChartArea = "ChartArea1";
            series14.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Doughnut;
            series14.IsValueShownAsLabel = true;
            series14.LabelForeColor = System.Drawing.Color.White;
            series14.Legend = "Legend1";
            series14.MarkerBorderWidth = 3;
            series14.Name = "Series1";
            this.chart1.Series.Add(series14);
            this.chart1.Size = new System.Drawing.Size(477, 541);
            this.chart1.TabIndex = 55;
            this.chart1.Text = "chart1";
            // 
            // groupservicios
            // 
            this.groupservicios.Controls.Add(this.lbTotal);
            this.groupservicios.Controls.Add(this.dgvServicios);
            this.groupservicios.Controls.Add(this.chart5);
            this.groupservicios.Controls.Add(this.groupBox3);
            this.groupservicios.Controls.Add(this.chart4);
            this.groupservicios.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupservicios.ForeColor = System.Drawing.Color.White;
            this.groupservicios.Location = new System.Drawing.Point(34, 167);
            this.groupservicios.Margin = new System.Windows.Forms.Padding(4);
            this.groupservicios.Name = "groupservicios";
            this.groupservicios.Padding = new System.Windows.Forms.Padding(4);
            this.groupservicios.Size = new System.Drawing.Size(1105, 650);
            this.groupservicios.TabIndex = 2;
            this.groupservicios.TabStop = false;
            this.groupservicios.Text = "Servicios";
            this.groupservicios.Visible = false;
            // 
            // lbTotal
            // 
            this.lbTotal.AutoSize = true;
            this.lbTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTotal.Location = new System.Drawing.Point(29, 76);
            this.lbTotal.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbTotal.Name = "lbTotal";
            this.lbTotal.Size = new System.Drawing.Size(66, 32);
            this.lbTotal.TabIndex = 16;
            this.lbTotal.Text = "000";
            this.lbTotal.Visible = false;
            // 
            // dgvServicios
            // 
            this.dgvServicios.AllowUserToAddRows = false;
            this.dgvServicios.AllowUserToDeleteRows = false;
            this.dgvServicios.AllowUserToOrderColumns = true;
            this.dgvServicios.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.dgvServicios.BackgroundColor = System.Drawing.Color.WhiteSmoke;
            this.dgvServicios.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvServicios.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            this.dgvServicios.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableWithoutHeaderText;
            dataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle19.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(32)))), ((int)(((byte)(40)))));
            dataGridViewCellStyle19.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle19.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle19.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle19.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle19.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvServicios.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle19;
            this.dgvServicios.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvServicios.Cursor = System.Windows.Forms.Cursors.Hand;
            this.dgvServicios.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.dgvServicios.EnableHeadersVisualStyles = false;
            this.dgvServicios.GridColor = System.Drawing.Color.Black;
            this.dgvServicios.Location = new System.Drawing.Point(516, 275);
            this.dgvServicios.Margin = new System.Windows.Forms.Padding(4);
            this.dgvServicios.MultiSelect = false;
            this.dgvServicios.Name = "dgvServicios";
            this.dgvServicios.ReadOnly = true;
            this.dgvServicios.RowHeadersVisible = false;
            dataGridViewCellStyle20.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle20.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(32)))), ((int)(((byte)(40)))));
            dataGridViewCellStyle20.SelectionBackColor = System.Drawing.Color.SteelBlue;
            dataGridViewCellStyle20.SelectionForeColor = System.Drawing.Color.White;
            this.dgvServicios.RowsDefaultCellStyle = dataGridViewCellStyle20;
            this.dgvServicios.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvServicios.Size = new System.Drawing.Size(551, 344);
            this.dgvServicios.TabIndex = 59;
            this.dgvServicios.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvServicios_CellDoubleClick);
            // 
            // chart5
            // 
            this.chart5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.chart5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(32)))), ((int)(((byte)(40)))));
            chartArea15.AlignmentOrientation = System.Windows.Forms.DataVisualization.Charting.AreaAlignmentOrientations.Horizontal;
            chartArea15.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(32)))), ((int)(((byte)(40)))));
            chartArea15.BorderColor = System.Drawing.Color.White;
            chartArea15.Name = "ChartArea1";
            this.chart5.ChartAreas.Add(chartArea15);
            this.chart5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chart5.Location = new System.Drawing.Point(7, 58);
            this.chart5.Name = "chart5";
            series15.BorderWidth = 5;
            series15.ChartArea = "ChartArea1";
            series15.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Doughnut;
            series15.IsValueShownAsLabel = true;
            series15.LabelForeColor = System.Drawing.Color.White;
            series15.Legend = "Legend1";
            series15.MarkerBorderWidth = 3;
            series15.Name = "Series1";
            this.chart5.Series.Add(series15);
            this.chart5.Size = new System.Drawing.Size(502, 585);
            this.chart5.TabIndex = 57;
            this.chart5.Text = "chart5";
            this.chart5.Click += new System.EventHandler(this.chart5_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.dates1);
            this.groupBox3.Controls.Add(this.txtcolaboradorservicios);
            this.groupBox3.Controls.Add(this.lbcolaboradorservicios);
            this.groupBox3.Controls.Add(this.lbusuarioservicio);
            this.groupBox3.Controls.Add(this.txtusuarioservicio);
            this.groupBox3.Controls.Add(this.lbtipo);
            this.groupBox3.Controls.Add(this.cbBusqueda);
            this.groupBox3.Controls.Add(this.lbrango2s);
            this.groupBox3.Controls.Add(this.lbrango1s);
            this.groupBox3.Controls.Add(this.dates2);
            this.groupBox3.Controls.Add(this.btnfiltrarservicios);
            this.groupBox3.ForeColor = System.Drawing.Color.White;
            this.groupBox3.Location = new System.Drawing.Point(516, 76);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox3.Size = new System.Drawing.Size(551, 183);
            this.groupBox3.TabIndex = 5;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Filtros";
            // 
            // dates1
            // 
            this.dates1.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dates1.Location = new System.Drawing.Point(255, 65);
            this.dates1.Margin = new System.Windows.Forms.Padding(4);
            this.dates1.Name = "dates1";
            this.dates1.Size = new System.Drawing.Size(136, 26);
            this.dates1.TabIndex = 5;
            this.dates1.Visible = false;
            // 
            // txtcolaboradorservicios
            // 
            this.txtcolaboradorservicios.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtcolaboradorservicios.Location = new System.Drawing.Point(254, 64);
            this.txtcolaboradorservicios.Margin = new System.Windows.Forms.Padding(4);
            this.txtcolaboradorservicios.Name = "txtcolaboradorservicios";
            this.txtcolaboradorservicios.Size = new System.Drawing.Size(249, 28);
            this.txtcolaboradorservicios.TabIndex = 15;
            this.txtcolaboradorservicios.Visible = false;
            this.txtcolaboradorservicios.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtcolaboradorservicios_KeyUp);
            // 
            // lbcolaboradorservicios
            // 
            this.lbcolaboradorservicios.AutoSize = true;
            this.lbcolaboradorservicios.Location = new System.Drawing.Point(250, 40);
            this.lbcolaboradorservicios.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbcolaboradorservicios.Name = "lbcolaboradorservicios";
            this.lbcolaboradorservicios.Size = new System.Drawing.Size(136, 20);
            this.lbcolaboradorservicios.TabIndex = 14;
            this.lbcolaboradorservicios.Text = "ID Colaborador";
            this.lbcolaboradorservicios.Visible = false;
            // 
            // lbusuarioservicio
            // 
            this.lbusuarioservicio.AutoSize = true;
            this.lbusuarioservicio.Location = new System.Drawing.Point(250, 40);
            this.lbusuarioservicio.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbusuarioservicio.Name = "lbusuarioservicio";
            this.lbusuarioservicio.Size = new System.Drawing.Size(186, 20);
            this.lbusuarioservicio.TabIndex = 13;
            this.lbusuarioservicio.Text = "Nombre del Contacto";
            this.lbusuarioservicio.Visible = false;
            // 
            // txtusuarioservicio
            // 
            this.txtusuarioservicio.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtusuarioservicio.Location = new System.Drawing.Point(254, 64);
            this.txtusuarioservicio.Margin = new System.Windows.Forms.Padding(4);
            this.txtusuarioservicio.Name = "txtusuarioservicio";
            this.txtusuarioservicio.Size = new System.Drawing.Size(249, 28);
            this.txtusuarioservicio.TabIndex = 12;
            this.txtusuarioservicio.Visible = false;
            this.txtusuarioservicio.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtusuarioservicio_KeyUp);
            // 
            // lbtipo
            // 
            this.lbtipo.AutoSize = true;
            this.lbtipo.Location = new System.Drawing.Point(250, 40);
            this.lbtipo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbtipo.Name = "lbtipo";
            this.lbtipo.Size = new System.Drawing.Size(45, 20);
            this.lbtipo.TabIndex = 11;
            this.lbtipo.Text = "Tipo";
            this.lbtipo.Visible = false;
            // 
            // cbBusqueda
            // 
            this.cbBusqueda.FormattingEnabled = true;
            this.cbBusqueda.Items.AddRange(new object[] {
            "Todas",
            "Fecha",
            "Contacto",
            "Colaborador"});
            this.cbBusqueda.Location = new System.Drawing.Point(66, 44);
            this.cbBusqueda.Margin = new System.Windows.Forms.Padding(4);
            this.cbBusqueda.Name = "cbBusqueda";
            this.cbBusqueda.Size = new System.Drawing.Size(139, 28);
            this.cbBusqueda.TabIndex = 9;
            this.cbBusqueda.Text = "Todas";
            this.cbBusqueda.SelectedIndexChanged += new System.EventHandler(this.comboservicios_SelectedIndexChanged);
            // 
            // lbrango2s
            // 
            this.lbrango2s.AutoSize = true;
            this.lbrango2s.Location = new System.Drawing.Point(250, 100);
            this.lbrango2s.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbrango2s.Name = "lbrango2s";
            this.lbrango2s.Size = new System.Drawing.Size(104, 20);
            this.lbrango2s.TabIndex = 8;
            this.lbrango2s.Text = "Rango final";
            this.lbrango2s.Visible = false;
            // 
            // lbrango1s
            // 
            this.lbrango1s.AutoSize = true;
            this.lbrango1s.Location = new System.Drawing.Point(250, 40);
            this.lbrango1s.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbrango1s.Name = "lbrango1s";
            this.lbrango1s.Size = new System.Drawing.Size(118, 20);
            this.lbrango1s.TabIndex = 7;
            this.lbrango1s.Text = "Rango inicial";
            this.lbrango1s.Visible = false;
            // 
            // dates2
            // 
            this.dates2.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dates2.Location = new System.Drawing.Point(254, 126);
            this.dates2.Margin = new System.Windows.Forms.Padding(4);
            this.dates2.Name = "dates2";
            this.dates2.Size = new System.Drawing.Size(136, 26);
            this.dates2.TabIndex = 6;
            this.dates2.Visible = false;
            // 
            // btnfiltrarservicios
            // 
            this.btnfiltrarservicios.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(66)))), ((int)(((byte)(82)))));
            this.btnfiltrarservicios.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnfiltrarservicios.Image = global::ProyectosMultiples.Properties.Resources.p6;
            this.btnfiltrarservicios.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnfiltrarservicios.Location = new System.Drawing.Point(66, 84);
            this.btnfiltrarservicios.Margin = new System.Windows.Forms.Padding(4);
            this.btnfiltrarservicios.Name = "btnfiltrarservicios";
            this.btnfiltrarservicios.Size = new System.Drawing.Size(139, 68);
            this.btnfiltrarservicios.TabIndex = 4;
            this.btnfiltrarservicios.Text = "Filtrar";
            this.btnfiltrarservicios.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnfiltrarservicios.UseVisualStyleBackColor = false;
            this.btnfiltrarservicios.Click += new System.EventHandler(this.btnfiltrarservicios_Click);
            // 
            // chart4
            // 
            this.chart4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.chart4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(32)))), ((int)(((byte)(40)))));
            chartArea16.Name = "ChartArea1";
            chartArea16.Visible = false;
            this.chart4.ChartAreas.Add(chartArea16);
            this.chart4.Cursor = System.Windows.Forms.Cursors.Hand;
            legend8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(32)))), ((int)(((byte)(40)))));
            legend8.Docking = System.Windows.Forms.DataVisualization.Charting.Docking.Top;
            legend8.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            legend8.ForeColor = System.Drawing.Color.White;
            legend8.IsTextAutoFit = false;
            legend8.LegendStyle = System.Windows.Forms.DataVisualization.Charting.LegendStyle.Row;
            legend8.Name = "Legend1";
            legend8.TableStyle = System.Windows.Forms.DataVisualization.Charting.LegendTableStyle.Wide;
            this.chart4.Legends.Add(legend8);
            this.chart4.Location = new System.Drawing.Point(7, 26);
            this.chart4.Name = "chart4";
            series16.BorderWidth = 5;
            series16.ChartArea = "ChartArea1";
            series16.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Doughnut;
            series16.IsValueShownAsLabel = true;
            series16.LabelForeColor = System.Drawing.Color.White;
            series16.Legend = "Legend1";
            series16.MarkerBorderWidth = 3;
            series16.Name = "Series1";
            this.chart4.Series.Add(series16);
            this.chart4.Size = new System.Drawing.Size(1091, 157);
            this.chart4.TabIndex = 58;
            this.chart4.Text = "chart4";
            this.chart4.Click += new System.EventHandler(this.chart4_Click);
            // 
            // groupingresos
            // 
            this.groupingresos.Controls.Add(this.dgvIngresos);
            this.groupingresos.Controls.Add(this.groupBox5);
            this.groupingresos.Controls.Add(this.chart3);
            this.groupingresos.Controls.Add(this.chart6);
            this.groupingresos.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupingresos.ForeColor = System.Drawing.Color.White;
            this.groupingresos.Location = new System.Drawing.Point(13, 198);
            this.groupingresos.Margin = new System.Windows.Forms.Padding(4);
            this.groupingresos.Name = "groupingresos";
            this.groupingresos.Padding = new System.Windows.Forms.Padding(4);
            this.groupingresos.Size = new System.Drawing.Size(1105, 650);
            this.groupingresos.TabIndex = 3;
            this.groupingresos.TabStop = false;
            this.groupingresos.Text = "Ingresos por Ventas";
            this.groupingresos.Visible = false;
            // 
            // dgvIngresos
            // 
            this.dgvIngresos.AllowUserToAddRows = false;
            this.dgvIngresos.AllowUserToDeleteRows = false;
            this.dgvIngresos.AllowUserToOrderColumns = true;
            this.dgvIngresos.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.dgvIngresos.BackgroundColor = System.Drawing.Color.WhiteSmoke;
            this.dgvIngresos.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvIngresos.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            this.dgvIngresos.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableWithoutHeaderText;
            dataGridViewCellStyle21.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle21.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(32)))), ((int)(((byte)(40)))));
            dataGridViewCellStyle21.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle21.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle21.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle21.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle21.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvIngresos.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle21;
            this.dgvIngresos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvIngresos.Cursor = System.Windows.Forms.Cursors.Hand;
            this.dgvIngresos.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.dgvIngresos.EnableHeadersVisualStyles = false;
            this.dgvIngresos.GridColor = System.Drawing.Color.Black;
            this.dgvIngresos.Location = new System.Drawing.Point(85, 252);
            this.dgvIngresos.Margin = new System.Windows.Forms.Padding(4);
            this.dgvIngresos.MultiSelect = false;
            this.dgvIngresos.Name = "dgvIngresos";
            this.dgvIngresos.ReadOnly = true;
            this.dgvIngresos.RowHeadersVisible = false;
            dataGridViewCellStyle22.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle22.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(32)))), ((int)(((byte)(40)))));
            dataGridViewCellStyle22.SelectionBackColor = System.Drawing.Color.SteelBlue;
            dataGridViewCellStyle22.SelectionForeColor = System.Drawing.Color.White;
            this.dgvIngresos.RowsDefaultCellStyle = dataGridViewCellStyle22;
            this.dgvIngresos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvIngresos.Size = new System.Drawing.Size(948, 344);
            this.dgvIngresos.TabIndex = 61;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.lbr2ingresos);
            this.groupBox5.Controls.Add(this.lbr1ingresos);
            this.groupBox5.Controls.Add(this.date2ingresos);
            this.groupBox5.Controls.Add(this.date1ingresos);
            this.groupBox5.Controls.Add(this.button5);
            this.groupBox5.Controls.Add(this.cbIngresos);
            this.groupBox5.ForeColor = System.Drawing.Color.White;
            this.groupBox5.Location = new System.Drawing.Point(85, 41);
            this.groupBox5.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox5.Size = new System.Drawing.Size(948, 191);
            this.groupBox5.TabIndex = 5;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Filtros";
            this.groupBox5.Enter += new System.EventHandler(this.groupBox5_Enter);
            // 
            // lbr2ingresos
            // 
            this.lbr2ingresos.AutoSize = true;
            this.lbr2ingresos.Location = new System.Drawing.Point(510, 109);
            this.lbr2ingresos.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbr2ingresos.Name = "lbr2ingresos";
            this.lbr2ingresos.Size = new System.Drawing.Size(104, 20);
            this.lbr2ingresos.TabIndex = 8;
            this.lbr2ingresos.Text = "Rango final";
            this.lbr2ingresos.Visible = false;
            // 
            // lbr1ingresos
            // 
            this.lbr1ingresos.AutoSize = true;
            this.lbr1ingresos.Location = new System.Drawing.Point(510, 45);
            this.lbr1ingresos.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbr1ingresos.Name = "lbr1ingresos";
            this.lbr1ingresos.Size = new System.Drawing.Size(118, 20);
            this.lbr1ingresos.TabIndex = 7;
            this.lbr1ingresos.Text = "Rango inicial";
            this.lbr1ingresos.Visible = false;
            // 
            // date2ingresos
            // 
            this.date2ingresos.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.date2ingresos.Location = new System.Drawing.Point(510, 133);
            this.date2ingresos.Margin = new System.Windows.Forms.Padding(4);
            this.date2ingresos.Name = "date2ingresos";
            this.date2ingresos.Size = new System.Drawing.Size(201, 26);
            this.date2ingresos.TabIndex = 6;
            this.date2ingresos.Visible = false;
            // 
            // date1ingresos
            // 
            this.date1ingresos.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.date1ingresos.Location = new System.Drawing.Point(510, 71);
            this.date1ingresos.Margin = new System.Windows.Forms.Padding(4);
            this.date1ingresos.Name = "date1ingresos";
            this.date1ingresos.Size = new System.Drawing.Size(201, 26);
            this.date1ingresos.TabIndex = 5;
            this.date1ingresos.Value = new System.DateTime(2019, 5, 1, 22, 0, 0, 0);
            this.date1ingresos.Visible = false;
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(66)))), ((int)(((byte)(82)))));
            this.button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button5.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button5.Image = global::ProyectosMultiples.Properties.Resources.p6;
            this.button5.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button5.Location = new System.Drawing.Point(241, 91);
            this.button5.Margin = new System.Windows.Forms.Padding(4);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(203, 68);
            this.button5.TabIndex = 4;
            this.button5.Text = "Filtrar";
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // cbIngresos
            // 
            this.cbIngresos.FormattingEnabled = true;
            this.cbIngresos.Items.AddRange(new object[] {
            "Todas",
            "Fecha",
            "Colaboradores"});
            this.cbIngresos.Location = new System.Drawing.Point(241, 45);
            this.cbIngresos.Margin = new System.Windows.Forms.Padding(4);
            this.cbIngresos.Name = "cbIngresos";
            this.cbIngresos.Size = new System.Drawing.Size(204, 28);
            this.cbIngresos.TabIndex = 3;
            this.cbIngresos.Text = "Todas";
            this.cbIngresos.SelectedIndexChanged += new System.EventHandler(this.comboBox2_SelectedIndexChanged);
            // 
            // chart3
            // 
            this.chart3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.chart3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(32)))), ((int)(((byte)(40)))));
            chartArea17.AxisX.IsLabelAutoFit = false;
            chartArea17.AxisX.LabelStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            chartArea17.AxisX.LabelStyle.ForeColor = System.Drawing.Color.White;
            chartArea17.AxisX.LineColor = System.Drawing.Color.White;
            chartArea17.AxisX.MajorGrid.LineColor = System.Drawing.Color.White;
            chartArea17.AxisX.MajorGrid.LineWidth = 2;
            chartArea17.AxisY.LabelStyle.ForeColor = System.Drawing.Color.White;
            chartArea17.AxisY.LineColor = System.Drawing.Color.White;
            chartArea17.AxisY.MajorGrid.LineColor = System.Drawing.Color.White;
            chartArea17.AxisY.MajorGrid.LineWidth = 2;
            chartArea17.AxisY.TitleForeColor = System.Drawing.Color.White;
            chartArea17.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(32)))), ((int)(((byte)(40)))));
            chartArea17.BorderColor = System.Drawing.Color.DimGray;
            chartArea17.Name = "ChartArea1";
            this.chart3.ChartAreas.Add(chartArea17);
            this.chart3.Cursor = System.Windows.Forms.Cursors.Hand;
            legend9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(32)))), ((int)(((byte)(40)))));
            legend9.Docking = System.Windows.Forms.DataVisualization.Charting.Docking.Top;
            legend9.Enabled = false;
            legend9.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            legend9.ForeColor = System.Drawing.Color.White;
            legend9.IsTextAutoFit = false;
            legend9.LegendStyle = System.Windows.Forms.DataVisualization.Charting.LegendStyle.Row;
            legend9.Name = "Legend1";
            legend9.TableStyle = System.Windows.Forms.DataVisualization.Charting.LegendTableStyle.Wide;
            this.chart3.Legends.Add(legend9);
            this.chart3.Location = new System.Drawing.Point(7, 23);
            this.chart3.Name = "chart3";
            series17.BorderWidth = 5;
            series17.ChartArea = "ChartArea1";
            series17.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Bar;
            series17.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            series17.IsValueShownAsLabel = true;
            series17.LabelBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(32)))), ((int)(((byte)(40)))));
            series17.LabelBorderWidth = 0;
            series17.LabelForeColor = System.Drawing.Color.Turquoise;
            series17.Legend = "Legend1";
            series17.MarkerBorderWidth = 3;
            series17.Name = "Series1";
            this.chart3.Series.Add(series17);
            this.chart3.Size = new System.Drawing.Size(1091, 620);
            this.chart3.TabIndex = 62;
            this.chart3.Text = "chart3";
            this.chart3.Visible = false;
            this.chart3.Click += new System.EventHandler(this.chart3_Click);
            // 
            // chart6
            // 
            this.chart6.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.chart6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(32)))), ((int)(((byte)(40)))));
            this.chart6.BackImageTransparentColor = System.Drawing.Color.LightGray;
            chartArea18.AlignmentOrientation = System.Windows.Forms.DataVisualization.Charting.AreaAlignmentOrientations.Horizontal;
            chartArea18.AxisX.InterlacedColor = System.Drawing.Color.White;
            chartArea18.AxisX.IsLabelAutoFit = false;
            chartArea18.AxisX.LabelStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            chartArea18.AxisX.LabelStyle.ForeColor = System.Drawing.Color.White;
            chartArea18.AxisX.LineColor = System.Drawing.Color.Turquoise;
            chartArea18.AxisX.LineWidth = 3;
            chartArea18.AxisX.MajorGrid.LineColor = System.Drawing.Color.White;
            chartArea18.AxisX.MajorGrid.LineWidth = 2;
            chartArea18.AxisY.IsLabelAutoFit = false;
            chartArea18.AxisY.LabelStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            chartArea18.AxisY.LabelStyle.ForeColor = System.Drawing.Color.White;
            chartArea18.AxisY.LineColor = System.Drawing.Color.Turquoise;
            chartArea18.AxisY.LineWidth = 3;
            chartArea18.AxisY.MajorGrid.LineColor = System.Drawing.Color.White;
            chartArea18.AxisY.MajorGrid.LineWidth = 2;
            chartArea18.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(32)))), ((int)(((byte)(40)))));
            chartArea18.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(32)))), ((int)(((byte)(40)))));
            chartArea18.IsSameFontSizeForAllAxes = true;
            chartArea18.Name = "ChartArea1";
            this.chart6.ChartAreas.Add(chartArea18);
            this.chart6.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chart6.Location = new System.Drawing.Point(13, 26);
            this.chart6.Name = "chart6";
            this.chart6.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.Grayscale;
            series18.BackImageTransparentColor = System.Drawing.Color.Transparent;
            series18.BackSecondaryColor = System.Drawing.Color.Transparent;
            series18.BorderColor = System.Drawing.Color.WhiteSmoke;
            series18.BorderDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Dash;
            series18.BorderWidth = 5;
            series18.ChartArea = "ChartArea1";
            series18.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series18.Color = System.Drawing.Color.Red;
            series18.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            series18.IsValueShownAsLabel = true;
            series18.LabelBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(32)))), ((int)(((byte)(40)))));
            series18.LabelBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(32)))), ((int)(((byte)(40)))));
            series18.LabelBorderWidth = 2;
            series18.LabelForeColor = System.Drawing.Color.White;
            series18.Legend = "Legend1";
            series18.MarkerBorderColor = System.Drawing.Color.White;
            series18.MarkerBorderWidth = 5;
            series18.MarkerColor = System.Drawing.Color.White;
            series18.MarkerImageTransparentColor = System.Drawing.Color.Maroon;
            series18.MarkerStyle = System.Windows.Forms.DataVisualization.Charting.MarkerStyle.Circle;
            series18.Name = "Series1";
            series18.ShadowColor = System.Drawing.Color.Transparent;
            this.chart6.Series.Add(series18);
            this.chart6.Size = new System.Drawing.Size(1072, 605);
            this.chart6.TabIndex = 59;
            this.chart6.Text = "chart6";
            this.chart6.Visible = false;
            this.chart6.Click += new System.EventHandler(this.chart6_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.panel1.Location = new System.Drawing.Point(1276, 1);
            this.panel1.Margin = new System.Windows.Forms.Padding(4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(13, 814);
            this.panel1.TabIndex = 4;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Margin = new System.Windows.Forms.Padding(4);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1289, 14);
            this.panel2.TabIndex = 5;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            this.panel3.Location = new System.Drawing.Point(0, 807);
            this.panel3.Margin = new System.Windows.Forms.Padding(4);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1289, 14);
            this.panel3.TabIndex = 6;
            // 
            // dgvUsuarios
            // 
            this.dgvUsuarios.AllowUserToAddRows = false;
            this.dgvUsuarios.AllowUserToDeleteRows = false;
            this.dgvUsuarios.AllowUserToOrderColumns = true;
            this.dgvUsuarios.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.dgvUsuarios.BackgroundColor = System.Drawing.Color.WhiteSmoke;
            this.dgvUsuarios.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvUsuarios.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            this.dgvUsuarios.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableWithoutHeaderText;
            dataGridViewCellStyle23.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle23.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(32)))), ((int)(((byte)(40)))));
            dataGridViewCellStyle23.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle23.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle23.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle23.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle23.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvUsuarios.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle23;
            this.dgvUsuarios.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvUsuarios.Cursor = System.Windows.Forms.Cursors.Hand;
            this.dgvUsuarios.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.dgvUsuarios.EnableHeadersVisualStyles = false;
            this.dgvUsuarios.GridColor = System.Drawing.Color.Black;
            this.dgvUsuarios.Location = new System.Drawing.Point(1207, 22);
            this.dgvUsuarios.Margin = new System.Windows.Forms.Padding(4);
            this.dgvUsuarios.MultiSelect = false;
            this.dgvUsuarios.Name = "dgvUsuarios";
            this.dgvUsuarios.ReadOnly = true;
            this.dgvUsuarios.RowHeadersVisible = false;
            dataGridViewCellStyle24.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle24.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(32)))), ((int)(((byte)(40)))));
            dataGridViewCellStyle24.SelectionBackColor = System.Drawing.Color.SteelBlue;
            dataGridViewCellStyle24.SelectionForeColor = System.Drawing.Color.White;
            this.dgvUsuarios.RowsDefaultCellStyle = dataGridViewCellStyle24;
            this.dgvUsuarios.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvUsuarios.Size = new System.Drawing.Size(61, 51);
            this.dgvUsuarios.TabIndex = 40;
            this.dgvUsuarios.Visible = false;
            // 
            // FReportes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(32)))), ((int)(((byte)(40)))));
            this.ClientSize = new System.Drawing.Size(1288, 820);
            this.Controls.Add(this.groupcotizaciones);
            this.Controls.Add(this.groupservicios);
            this.Controls.Add(this.groupingresos);
            this.Controls.Add(this.dgvUsuarios);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "FReportes";
            this.Text = "FReportes";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupcotizaciones.ResumeLayout(false);
            this.groupcotizaciones.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCotizaciones)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            this.groupservicios.ResumeLayout(false);
            this.groupservicios.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvServicios)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart5)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart4)).EndInit();
            this.groupingresos.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvIngresos)).EndInit();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvUsuarios)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.GroupBox groupcotizaciones;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button btnfiltrarcotizacion;
        private System.Windows.Forms.ComboBox cbBusquedaC;
        private System.Windows.Forms.Label lbrango2;
        private System.Windows.Forms.Label lbrango1;
        private System.Windows.Forms.DateTimePicker datec2;
        private System.Windows.Forms.DateTimePicker datec1;
        private System.Windows.Forms.GroupBox groupservicios;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label lbrango2s;
        private System.Windows.Forms.Label lbrango1s;
        private System.Windows.Forms.DateTimePicker dates2;
        private System.Windows.Forms.DateTimePicker dates1;
        private System.Windows.Forms.Button btnfiltrarservicios;
        private System.Windows.Forms.GroupBox groupingresos;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label lbr2ingresos;
        private System.Windows.Forms.Label lbr1ingresos;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.ComboBox cbIngresos;
        private System.Windows.Forms.ComboBox cbBusqueda;
        private System.Windows.Forms.Label lbtipo;
        private System.Windows.Forms.Label lbapellido;
        private System.Windows.Forms.TextBox txtcotizacion;
        private System.Windows.Forms.Label lbusuario;
        private System.Windows.Forms.TextBox txtusuario;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label lbusuarioservicio;
        private System.Windows.Forms.TextBox txtusuarioservicio;
        private System.Windows.Forms.TextBox txtcolaboradorservicios;
        private System.Windows.Forms.Label lbcolaboradorservicios;
        private System.Windows.Forms.DataGridView dgvCotizaciones;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart2;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart5;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart4;
        private System.Windows.Forms.DataGridView dgvServicios;
        private System.Windows.Forms.Label lbTotal;
        private System.Windows.Forms.Label lbTotal2;
        private System.Windows.Forms.DataGridView dgvIngresos;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart6;
        private System.Windows.Forms.DateTimePicker date2ingresos;
        private System.Windows.Forms.DateTimePicker date1ingresos;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart3;
        private System.Windows.Forms.DataGridView dgvUsuarios;
        private System.Windows.Forms.Label lbHoy;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label lbRi;
        private System.Windows.Forms.DateTimePicker dtpRi;
        private System.Windows.Forms.Label lbRf;
        private System.Windows.Forms.DateTimePicker dtpRf;
    }
}