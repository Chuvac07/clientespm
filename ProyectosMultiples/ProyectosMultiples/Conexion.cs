﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using ProyectosMultiples.Properties;

namespace ProyectosMultiples
{
    class Conexion
    {
        public static string obtenerdireccion()
        {
            return Settings.Default.ProyectosMultiplesConnectionString;
        }

        string direccion = obtenerdireccion();
        public SqlConnection conectar = new SqlConnection();
        public SqlCommand cmd;
        public Conexion()
        {
            conectar.ConnectionString = direccion;
        }

        public void abrir()
        {
            try
            {
                conectar.Open();
            }
            catch (Exception ex)
            {
                FMessageBox obj = new FMessageBox("Error: " + ex.Message, "¡Error de Conexión!", 1);
                if (obj.ShowDialog() == DialogResult.OK) ;
            }
        }

        public void cerrar()
        {
            conectar.Close();
        }
        
        // Funcion para ingresar Clientes
        public void ingresarClientes(string nit, string nombres, string apellidos, string tipo, string telefono, string direccion, string email)
        {
            try
            {
                conectar.Open();
                string ingresar = "INSERT INTO CLIENTESPM (nit_clientespm, nombre_clientespm, apellido_clientespm, tipo_clientepm, telefono_clientespm, direccion_clientespm, email_clientespm) VALUES (@nit, @nombre, @apellido, @tipo, @telefono, @direccion, @email)";
                cmd = new SqlCommand(ingresar, conectar);
                cmd.Parameters.AddWithValue("@nit", nit);
                cmd.Parameters.AddWithValue("@nombre", nombres);
                cmd.Parameters.AddWithValue("@apellido", apellidos);
                cmd.Parameters.AddWithValue("@tipo", tipo);
                cmd.Parameters.AddWithValue("@telefono", telefono);
                cmd.Parameters.AddWithValue("@direccion", direccion);
                cmd.Parameters.AddWithValue("@email", email);
                cmd.ExecuteNonQuery();
                //MessageBox.Show("Elemento Almacenado con Exito.", "Almacenado");
                FMessageBox obj = new FMessageBox("Elemento almacenado con exito.", "1. Base De Datos",0);
                if (obj.ShowDialog() == DialogResult.OK);
                conectar.Close();
            }
            catch (Exception ex)
            {
                FMessageBox obj = new FMessageBox("Error 001: " + ex.Message, "¡Error de Conexión!", 1);
                if (obj.ShowDialog() == DialogResult.OK) ;
            }
        }

        // FUNCION PARA INGRESAR COLABORADORES
        public void ingresarColaboradores(string codigo, string nombre, string dpi, string telefono, string direccion, string email)
        {
            try
            {
                conectar.Open();
                string ingresar = "INSERT INTO COLABORADORESPM (id_colaboradorespm, nombre_colaboradorespm, dpi_colaboradoresi, telefono_colaboradorespm, direccion_colaboradorespm,email_colaboradorespm) VALUES (@codigo, @nombre,@dpi, @telefono, @direccion, @email)";
                cmd = new SqlCommand(ingresar, conectar);
                cmd.Parameters.AddWithValue("@codigo", codigo);
                cmd.Parameters.AddWithValue("@nombre", nombre);
                cmd.Parameters.AddWithValue("@dpi", dpi);
                cmd.Parameters.AddWithValue("@telefono", telefono);
                cmd.Parameters.AddWithValue("@direccion", direccion);
                cmd.Parameters.AddWithValue("@email", email);
                cmd.ExecuteNonQuery();
                FMessageBox obj = new FMessageBox("Elemento almacenado con exito.", "1. Base De Datos", 0);
                if (obj.ShowDialog() == DialogResult.OK) ;
                conectar.Close();
            }
            catch (Exception ex)
            {
                FMessageBox obj = new FMessageBox("Error 001: " + ex.Message, "¡Error de Conexión!", 1);
                if (obj.ShowDialog() == DialogResult.OK) ;
            }
        }

        // FUNCION PARA INGRESAR COTIZACIONES
        public void ingresarCotizaciones(DateTime fecha, string estado, int idUsuario, string nitCliente, string condiciones, string tiempo, string observaciones, DateTime fecha2, double total)
        {
            try
            {
                conectar.Open();
                string ingresar1 = "INSERT INTO COTIZACIONESPM (fecha_cotizacionespm, estado_cotizacionespm, USUARIOSPM_id_usuariospm, CLIENTESPM_id_clientespm, condiciones_cotizacionespm, tiempo_cotizacionespm,observaciones_cotizacionespm,fechafuturo_cotizacionespm, total_cotizacionespm) VALUES ";
                string ingresar2 = "(@fecha, @estado,@idUsuario, @idCliente, @condiciones, @tiempo, @observaciones, @fecha2, @total)";
                string ingresar = ingresar1 + ingresar2;
                cmd = new SqlCommand(ingresar, conectar);
                cmd.Parameters.AddWithValue("@fecha", fecha);
                cmd.Parameters.AddWithValue("@estado", estado);
                cmd.Parameters.AddWithValue("@idUsuario", idUsuario);
                cmd.Parameters.AddWithValue("@idCliente", nitCliente);
                cmd.Parameters.AddWithValue("@condiciones", condiciones);
                cmd.Parameters.AddWithValue("@tiempo", tiempo);
                cmd.Parameters.AddWithValue("@observaciones", observaciones);
                cmd.Parameters.AddWithValue("@fecha2", fecha2);
                cmd.Parameters.AddWithValue("@total", total);
                cmd.ExecuteNonQuery();
                FMessageBox obj = new FMessageBox("Elemento almacenado con exito.", "1. Base De Datos",0);
                if (obj.ShowDialog() == DialogResult.OK);
                conectar.Close();
            }
            catch (Exception ex)
            {
                FMessageBox obj = new FMessageBox("Error 001: " + ex.Message, "¡Error de Conexión!", 1);
                if (obj.ShowDialog() == DialogResult.OK) ;
            }
        }

        // FUNCION PARA INGRESAR PRODUCTOS EN LAS COTIZACIONES
        public void ingresarProductosCot(int cantidad, string descripcion, double precioU, double subtotal, int idCotizaciones)
        {
            try
            {
                conectar.Open();
                string ingresar1 = "INSERT INTO PRODUCTOPM (cantidad_productopm, descripcion_productopm, precio_productopm, subtotal_productopm, COTIZACIONESPM_id_cotizacionespm) ";
                string ingresar2 = "VALUES (@cantidad, @descripcion, @precioU, @subtotal, @idCotizaciones)";
                string ingresar = ingresar1 +ingresar2;
                cmd = new SqlCommand(ingresar, conectar);
                cmd.Parameters.AddWithValue("@cantidad", cantidad);
                cmd.Parameters.AddWithValue("@descripcion", descripcion);
                cmd.Parameters.AddWithValue("@precioU", precioU);
                cmd.Parameters.AddWithValue("@subtotal", subtotal);
                cmd.Parameters.AddWithValue("@idCotizaciones", idCotizaciones);
                cmd.ExecuteNonQuery();
                //MessageBox.Show("Elemento Almacenado con Exito.", "Almacenado");
                conectar.Close();
            }
            catch (Exception ex)
            {
                FMessageBox obj = new FMessageBox("Error 001: " + ex.Message, "¡Error de Conexión!", 1);
                if (obj.ShowDialog() == DialogResult.OK) ;
            }
        }

        // FUNCION PARA ELIMINAR COLABORADORES
        public void eliminarColaboradores(string codigo)
        {
            try
            {
                conectar.Open();
                string eliminar = "DELETE FROM COLABORADORESPM WHERE id_colaboradorespm=@codigo";
                cmd = new SqlCommand(eliminar, conectar);
                cmd.Parameters.AddWithValue("@codigo", codigo);
                cmd.ExecuteNonQuery();
                FMessageBox obj = new FMessageBox("¡Elemento Eliminado!", "2. Base De Datos", 0);
                if (obj.ShowDialog() == DialogResult.OK) ;
                conectar.Close();
            }
            catch (Exception ex)
            {
                FMessageBox obj = new FMessageBox("Error 002: " + ex.Message, "¡Error de Conexión!", 1);
                if (obj.ShowDialog() == DialogResult.OK) ;
            }
        }

        // Funcion para modificar Clientes
        public void modificarClientes(string id, string nit, string nombres, string apellidos, string telefono, string direccion, string email)
        {
            try
            {
                conectar.Open();
                string modificar = "UPDATE CLIENTESPM SET nombre_clientespm = @nombre, apellido_clientespm = @apellido, telefono_clientespm = @telefono, direccion_clientespm = @direccion, email_clientespm = @email, nit_clientespm =@nit WHERE id_clientespm =@id";
                cmd = new SqlCommand(modificar, conectar);
                cmd.Parameters.AddWithValue("@id", id);
                cmd.Parameters.AddWithValue("@nombre", nombres);
                cmd.Parameters.AddWithValue("@apellido", apellidos);
                cmd.Parameters.AddWithValue("@telefono", telefono);
                cmd.Parameters.AddWithValue("@direccion", direccion);
                cmd.Parameters.AddWithValue("@email", email);
                cmd.Parameters.AddWithValue("@nit", nit);
                cmd.ExecuteNonQuery();
                FMessageBox obj = new FMessageBox("¡Elemento Modificado!", "3. Base De Datos", 0);
                if (obj.ShowDialog() == DialogResult.OK) ;
                conectar.Close();
            }
            catch (Exception ex)
            {
                FMessageBox obj = new FMessageBox("Error 003: " + ex.Message, "¡Error de Conexión!", 1);
                if (obj.ShowDialog() == DialogResult.OK) ;
            }
        }

        // Funcion para modificar COLABORADORES
        public void modificarColaboradores(string codigo, string nombre, string dpi, string telefono, string direccion, string email)
        {
            try
            {
                conectar.Open();
                string modificar = "UPDATE COLABORADORESPM SET nombre_colaboradorespm =@nombre, id_colaboradorespm = @codigo, dpi_colaboradoresi = @dpi, telefono_colaboradorespm = @telefono ,direccion_colaboradorespm = @direccion, email_colaboradorespm = @email WHERE nombre_colaboradorespm =@nombre";
                cmd = new SqlCommand(modificar, conectar);
                cmd.Parameters.AddWithValue("@codigo", codigo);
                cmd.Parameters.AddWithValue("@nombre", nombre);
                cmd.Parameters.AddWithValue("@dpi", dpi);
                cmd.Parameters.AddWithValue("@telefono", telefono);
                cmd.Parameters.AddWithValue("@direccion", direccion);
                cmd.Parameters.AddWithValue("@email", email);
                cmd.ExecuteNonQuery();
                FMessageBox obj = new FMessageBox("¡Elemento Modificado!", "3. Base De Datos", 0);
                if (obj.ShowDialog() == DialogResult.OK) ;
                conectar.Close();
            }
            catch (Exception ex)
            {
                FMessageBox obj = new FMessageBox("Error 003: " + ex.Message, "¡Error de Conexión!", 1);
                if (obj.ShowDialog() == DialogResult.OK) ;
            }
        }

        // Funcion para modificar el estado de las COTIZACIONES
        public void modificarEstadoC(string id, string estado)
        {
            try
            {
                conectar.Open();
                string modificar = "UPDATE COTIZACIONESPM SET estado_cotizacionespm = @estado WHERE id_cotizacionespm =@id";
                cmd = new SqlCommand(modificar, conectar);
                cmd.Parameters.AddWithValue("@estado", estado);
                cmd.Parameters.AddWithValue("@id", id);
                cmd.ExecuteNonQuery();
                //MessageBox.Show("Elemento Modificado con Exito.", "Conexión con la Base de Datos");
                conectar.Close();
            }
            catch (Exception ex)
            {
                FMessageBox obj = new FMessageBox("Error 003: " + ex.Message, "¡Error de Conexión!", 1);
                if (obj.ShowDialog() == DialogResult.OK) ;
            }
        }

        // Funcion para modificar el estado y fecha de las ALERTAS
        public void modificarEstadoAlertas(string id, string estado, string fecha)
        {
            try
            {
                conectar.Open();
                string modificar = "UPDATE COTIZACIONESPM SET estado_cotizacionespm = @estado, fechafuturo_cotizacionespm = @fecha WHERE id_cotizacionespm =@id";
                cmd = new SqlCommand(modificar, conectar);
                cmd.Parameters.AddWithValue("@estado", estado);
                cmd.Parameters.AddWithValue("@fecha", fecha);
                cmd.Parameters.AddWithValue("@id", id);
                cmd.ExecuteNonQuery();
                //MessageBox.Show("Elemento Modificado con Exito.", "Conexión con la Base de Datos");
                conectar.Close();
            }
            catch (Exception ex)
            {
                FMessageBox obj = new FMessageBox("Error 003: " + ex.Message, "¡Error de Conexión!", 1);
                if (obj.ShowDialog() == DialogResult.OK) ;
            }
        }


        // Actualizar el DataGridView de clientes que recibe
        public void actualizarClientes(DataGridView TablaC, string tipo)
        {
            string actualizar1 = "SELECT id_clientespm AS 'No.', apellido_clientespm AS 'Empresa', nombre_clientespm AS 'Contacto', nit_clientespm AS 'NIT', tipo_clientepm AS 'Tipo', telefono_clientespm AS 'Teléfono', direccion_clientespm AS 'Dirección', email_clientespm AS 'Email' FROM CLIENTESPM ";
            string actualizar2 = "";
            if (tipo != "TODO")
                actualizar2 = "WHERE tipo_clientepm = '"+tipo+"'";

            string actualizar3 = "ORDER By id_clientespm";
            string actualizar = actualizar1 + actualizar2 + actualizar3;
            SqlCommand cmd = new SqlCommand(actualizar, conectar);
            SqlDataAdapter adaptador = new SqlDataAdapter(cmd);
            System.Data.DataTable tabla = new System.Data.DataTable();
            adaptador.Fill(tabla);
            TablaC.DataSource = tabla;
            TablaC.Columns[0].Width = 30;
            TablaC.Columns[1].Width = 150;
            TablaC.Columns[2].Width = 150;
            TablaC.Columns[3].Width = 80;
            TablaC.Columns[4].Width = 80;
            TablaC.Columns[5].Width = 90;
            TablaC.Columns[6].Width = 180;
            TablaC.Columns[7].Width = 200;
            conectar.Close();
        }

        // Actualizar el DataGridView de USUARIOS PRINCIPALES
        public void actualizarUsuarios(DataGridView TablaC)
        {
            string actualizar = "SELECT Id_usuariospm AS 'No.', nombre_usuariospm AS 'Nombre', dpi_usuariospm AS 'DPI', telefono_usuariospm AS 'Teléfono', direccion_usuariospm AS 'Dirección', email_usuariospm AS 'Email', permiso_usuariospm AS 'Permiso', usuario_usuariospm AS 'Usuario', contra_usuariospm AS 'PASSWORD' FROM USUARIOSPM ORDER By Id_usuariospm";
            SqlCommand cmd = new SqlCommand(actualizar, conectar);
            SqlDataAdapter adaptador = new SqlDataAdapter(cmd);
            System.Data.DataTable tabla = new System.Data.DataTable();
            adaptador.Fill(tabla);
            TablaC.DataSource = tabla;
            TablaC.Columns[0].Width = 100;
            TablaC.Columns[1].Width = 100;
            TablaC.Columns[2].Width = 100;
            TablaC.Columns[3].Width = 100;
            TablaC.Columns[4].Width = 100;
            TablaC.Columns[5].Width = 100;
            TablaC.Columns[6].Width = 100;
            TablaC.Columns[7].Width = 100;
            TablaC.Columns[8].Width = 100;
            conectar.Close();
        }

        // Actualizar el DataGridView de colaboradores que recibe
        public void actualizarColaboradores(DataGridView TablaC)
        {
            string actualizar = "SELECT id_colaboradorespm AS 'Codigo',nombre_colaboradorespm AS 'Nombre', dpi_colaboradoresi AS 'DPI', telefono_colaboradorespm AS 'Teléfono', direccion_colaboradorespm AS 'Dirección', email_colaboradorespm AS 'Email' FROM COLABORADORESPM ORDER By nombre_colaboradorespm";
            SqlCommand cmd = new SqlCommand(actualizar, conectar);
            SqlDataAdapter adaptador = new SqlDataAdapter(cmd);
            System.Data.DataTable tabla = new System.Data.DataTable();
            adaptador.Fill(tabla);
            TablaC.DataSource = tabla;
            TablaC.Columns[0].Width = 150;
            TablaC.Columns[1].Width = 150;
            TablaC.Columns[2].Width = 80;
            TablaC.Columns[3].Width = 80;
            TablaC.Columns[4].Width = 175;
            TablaC.Columns[5].Width = 200;
            conectar.Close();
        }

        // Actualizar el DataGridView de las COTIZACIONES
        public void actualizarCotizacion(DataGridView TablaC, string estado)
        {
            string actualizar1 = "SELECT id_cotizacionespm AS 'No.', nombre_clientespm AS 'Contacto', apellido_clientespm AS 'Empresa', nit_clientespm AS 'Nit', fecha_cotizacionespm AS 'Fecha Cotización',estado_cotizacionespm 'Estado', nombre_usuariospm AS 'Usuario', fechafuturo_cotizacionespm AS 'Futuro', telefono_clientespm AS 'Teléfono', email_clientespm AS 'E-Mail'";
            string actualizar2 = "FROM COTIZACIONESPM INNER JOIN USUARIOSPM ON USUARIOSPM_id_usuariospm = Id_usuariospm ";
            string actualizar3 = "INNER JOIN CLIENTESPM ON CLIENTESPM_id_clientespm = id_clientespm ";
            string actualizar4 = "";
            if (estado == "Todos")
            {
                actualizar4 = "";
            }
            else
            {
                actualizar4 = "WHERE estado_cotizacionespm = '" + estado + "'";
            }
            string actualizar = actualizar1 + actualizar2 + actualizar3 + actualizar4;
            SqlCommand cmd = new SqlCommand(actualizar, conectar);
            SqlDataAdapter adaptador = new SqlDataAdapter(cmd);
            System.Data.DataTable tabla = new System.Data.DataTable();
            adaptador.Fill(tabla);
            TablaC.DataSource = tabla;
            TablaC.Columns[0].Width = 40;
            TablaC.Columns[1].Width = 150;
            TablaC.Columns[2].Width = 150;
            TablaC.Columns[3].Width = 100;
            TablaC.Columns[4].Width = 150;
            TablaC.Columns[5].Width = 100;
            TablaC.Columns[6].Width = 120;
            TablaC.Columns[7].Width = 150;
            TablaC.Columns[8].Width = 100;
            TablaC.Columns[9].Width = 150;
            conectar.Close();
        }

        // Actualizar el DataGridView de las ALERTAS
        public void actualizarCotizacionAlertas(DataGridView TablaC, string fecha)
        {
            string actualizar1 = "SELECT id_cotizacionespm AS 'No.', nombre_clientespm AS 'Contacto', apellido_clientespm AS 'Empresa', nit_clientespm AS 'Nit', fecha_cotizacionespm AS 'Fecha Cotización',estado_cotizacionespm 'Estado', nombre_usuariospm AS 'Usuario', fechafuturo_cotizacionespm AS 'Futuro', telefono_clientespm AS 'Teléfono', email_clientespm AS 'E-Mail'";
            string actualizar2 = "FROM COTIZACIONESPM INNER JOIN USUARIOSPM ON USUARIOSPM_id_usuariospm = Id_usuariospm ";
            string actualizar3 = "INNER JOIN CLIENTESPM ON CLIENTESPM_id_clientespm = id_clientespm ";
            string actualizar4 = "WHERE estado_cotizacionespm = 'Pendiente' AND fechafuturo_cotizacionespm <= '"+fecha+ " 23:59' Order By fechafuturo_cotizacionespm";

            string actualizar = actualizar1 + actualizar2 + actualizar3 + actualizar4;
            SqlCommand cmd = new SqlCommand(actualizar, conectar);
            SqlDataAdapter adaptador = new SqlDataAdapter(cmd);
            System.Data.DataTable tabla = new System.Data.DataTable();
            adaptador.Fill(tabla);
            TablaC.DataSource = tabla;
            TablaC.Columns[0].Width = 40;
            TablaC.Columns[1].Width = 150;
            TablaC.Columns[2].Width = 150;
            TablaC.Columns[3].Width = 100;
            TablaC.Columns[4].Width = 160;
            TablaC.Columns[5].Width = 100;
            TablaC.Columns[6].Width = 150;
            TablaC.Columns[7].Width = 150;
            conectar.Close();
        }

        // Actualizar el DataGridView de los PRODUCTOS DE LAS COTIZACIONES
        public void actualizarProductoC(DataGridView TablaC, string cotizacion)
        {
            string actualizar1 = "SELECT cantidad_productopm AS 'Cantidad', descripcion_productopm AS 'Descripción', precio_productopm AS 'Precio Unitario', subtotal_productopm AS 'Sub-Total' ";
            string actualizar2 = "FROM PRODUCTOPM WHERE COTIZACIONESPM_id_cotizacionespm = " + cotizacion + "";
            string actualizar = actualizar1 + actualizar2;
            SqlCommand cmd = new SqlCommand(actualizar, conectar);
            SqlDataAdapter adaptador = new SqlDataAdapter(cmd);
            System.Data.DataTable tabla = new System.Data.DataTable();
            adaptador.Fill(tabla);
            TablaC.DataSource = tabla;
            TablaC.Columns[0].Width = 100;
            TablaC.Columns[1].Width = 360;
            TablaC.Columns[2].Width = 150;
            TablaC.Columns[3].Width = 150;
            conectar.Close();
        }
        // Buscar Clientes automaticamente
        public void busqueda(DataGridView tablaC, TextBox texto, string columna, string tipo)
        {
            try
            {
                conectar.Open();
                string buscar1 = "SELECT id_clientespm AS 'No.', apellido_clientespm AS 'Empresa',nombre_clientespm AS 'Contacto', nit_clientespm AS 'NIT', tipo_clientepm AS 'Tipo', telefono_clientespm AS 'Teléfono', direccion_clientespm AS 'Dirección', email_clientespm AS 'Email' ";
                string buscar2 = "FROM CLIENTESPM WHERE tipo_clientepm = '" + tipo +"' AND " + columna + " like ('" + texto.Text + "%')";
                string buscar = buscar1 + buscar2;
                SqlCommand cmd = new SqlCommand(buscar, conectar);
                cmd.ExecuteNonQuery();
                SqlDataAdapter adaptador = new SqlDataAdapter(cmd);
                System.Data.DataTable tabla = new System.Data.DataTable();
                adaptador.Fill(tabla);
                tablaC.DataSource = tabla;
                conectar.Close();
            }
            catch (Exception ex)
            {
                FMessageBox obj = new FMessageBox("Error 004: " + ex.Message, "¡Error de Conexión!", 1);
                if (obj.ShowDialog() == DialogResult.OK) ;
            }
        }

        // Buscar Clientes automaticamente desde COTIZACIONES
        public void busquedaCotizaciones(DataGridView tablaC, TextBox texto, string columna, string estado)
        {
            try
            {
                conectar.Open();
                string buscar1 = "SELECT id_cotizacionespm AS 'No.', nombre_clientespm AS 'Contacto', apellido_clientespm AS 'Empresa', nit_clientespm AS 'Nit', fecha_cotizacionespm AS 'Fecha Cotización',estado_cotizacionespm 'Estado', nombre_usuariospm AS 'Usuario', fechafuturo_cotizacionespm AS 'Futuro', telefono_clientespm AS 'Teléfono', email_clientespm AS 'E-Mail'";
                string buscar2 = "FROM COTIZACIONESPM INNER JOIN USUARIOSPM ON USUARIOSPM_id_usuariospm = Id_usuariospm ";
                string buscar3 = "INNER JOIN CLIENTESPM ON CLIENTESPM_id_clientespm = id_clientespm ";
                string buscar4 = "";
                if (estado == "Todos")
                {
                    buscar4 = "WHERE " + columna + " like ('" + texto.Text + "%')";
                }
                else
                {
                    buscar4 = "WHERE " + columna + " like ('" + texto.Text + "%') AND estado_cotizacionespm = '" + estado + "'";
                }
                string buscar = buscar1 + buscar2 + buscar3 + buscar4;

                SqlCommand cmd = new SqlCommand(buscar, conectar);
                cmd.ExecuteNonQuery();
                SqlDataAdapter adaptador = new SqlDataAdapter(cmd);
                System.Data.DataTable tabla = new System.Data.DataTable();
                adaptador.Fill(tabla);
                tablaC.DataSource = tabla;
                conectar.Close();
            }
            catch (Exception ex)
            {
                FMessageBox obj = new FMessageBox("Error 004: " + ex.Message, "¡Error de Conexión!", 1);
                if (obj.ShowDialog() == DialogResult.OK) ;
            }
        }
        // FUNCION PARA INGRESAR USUARIOS DEL SISTEMA
        public void ingresarUsuariosSistema(string nombre, string dpi, string telefono, string direccion, string email,string permiso, string usuario, string contra)
        {
            try
            {
                conectar.Open();
                string ingresar = "INSERT INTO USUARIOSPM (nombre_usuariospm,dpi_usuariospm, telefono_usuariospm,direccion_usuariospm,email_usuariospm,permiso_usuariospm, usuario_usuariospm, contra_usuariospm) VALUES (@nombre,@dpi, @telefono, @direccion, @email, @permiso, @usuario, @contra)";
                cmd = new SqlCommand(ingresar, conectar);
                cmd.Parameters.AddWithValue("@nombre", nombre);
                cmd.Parameters.AddWithValue("@dpi", dpi);
                cmd.Parameters.AddWithValue("@telefono", telefono);
                cmd.Parameters.AddWithValue("@direccion", direccion);
                cmd.Parameters.AddWithValue("@email", email);
                cmd.Parameters.AddWithValue("@permiso", permiso);
                cmd.Parameters.AddWithValue("@usuario", usuario);
                cmd.Parameters.AddWithValue("@contra", contra);
                cmd.ExecuteNonQuery();
                FMessageBox obj = new FMessageBox("Elemento almacenado con exito.", "1. Base De Datos", 0);
                if (obj.ShowDialog() == DialogResult.OK) ;

                conectar.Close();
            }
            catch (Exception ex)
            {
                FMessageBox obj = new FMessageBox("Error 001: " + ex.Message, "¡Error de Conexión!", 1);
                if (obj.ShowDialog() == DialogResult.OK) ;
            }
        }
        // Actualizar el DataGridView de USUARIOS DEL SISTEMA que recibe
        public void actualizarUsuariosSistema(DataGridView TablaC)
        {
            string actualizar = "SELECT Id_usuariospm AS 'No.', nombre_usuariospm AS 'Nombre', dpi_usuariospm AS 'DPI', telefono_usuariospm AS 'Teléfono', direccion_usuariospm AS 'Dirección', email_usuariospm AS 'Email', permiso_usuariospm 'Permiso',usuario_usuariospm AS 'Usuario', contra_usuariospm AS 'Password' FROM USUARIOSPM ORDER By nombre_usuariospm";
            SqlCommand cmd = new SqlCommand(actualizar, conectar);
            SqlDataAdapter adaptador = new SqlDataAdapter(cmd);
            System.Data.DataTable tabla = new System.Data.DataTable();
            adaptador.Fill(tabla);
            TablaC.DataSource = tabla;
            TablaC.Columns[0].Width = 40;
            TablaC.Columns[1].Width = 150;
            TablaC.Columns[2].Width = 150;
            TablaC.Columns[3].Width = 80;
            TablaC.Columns[4].Width = 150;
            TablaC.Columns[5].Width = 150;
            TablaC.Columns[6].Width = 80;
            TablaC.Columns[7].Width = 100;
            TablaC.Columns[8].Width = 100;
            conectar.Close();
        }

        // Actualizar el DataGridView de las ALERTAS de los SERVICIOS
        public void actualizarAlertasSerivicios(DataGridView TablaC, string fecha)
        {

            string actualizar1 = "SELECT id_alertaspm AS 'No.', nombre_clientespm AS 'Contacto', apellido_clientespm AS 'Empresa', nit_clientespm AS 'Nit', telefono_clientespm AS 'Teléfono', fecha_alertaspm AS 'Fecha de Alerta', descripcion_alertaspm  AS 'Descripción', email_clientespm AS 'E-Mail' ";
            string actualizar2 = "FROM ALERTASPM ";
            string actualizar3 = "INNER JOIN CLIENTESPM ON CLIENTESPM_id_clientespm = id_clientespm ";
            string actualizar4 = "WHERE estado_alertaspm = 1 AND fecha_alertaspm <= '" + fecha + " 23:59' Order By fecha_alertaspm";

            string actualizar = actualizar1 + actualizar2 + actualizar3 + actualizar4;
            SqlCommand cmd = new SqlCommand(actualizar, conectar);
            SqlDataAdapter adaptador = new SqlDataAdapter(cmd);
            System.Data.DataTable tabla = new System.Data.DataTable();
            adaptador.Fill(tabla);
            TablaC.DataSource = tabla;
            TablaC.Columns[0].Width = 40;
            TablaC.Columns[1].Width = 150;
            TablaC.Columns[2].Width = 150;
            TablaC.Columns[3].Width = 100;
            TablaC.Columns[4].Width = 100;
            TablaC.Columns[5].Width = 160;
            TablaC.Columns[6].Width = 300;
            conectar.Close();
        }

        // Actualizar Reporte de Cotizaciones 
        public void actualizarReporteCotizacion(DataGridView TablaC, string fechaIn, string fechaFin, string contacto, string usuario, int filtro)
        {

            string actualizar1 = "SELECT id_cotizacionespm AS 'No.', nombre_clientespm AS 'Contacto', apellido_clientespm AS 'Empresa', nit_clientespm AS 'Nit', total_cotizacionespm AS 'Total', fecha_cotizacionespm AS 'Fecha Cotización', estado_cotizacionespm  AS 'Estado', nombre_usuariospm AS 'Usuario' ";
            string actualizar2 = "FROM COTIZACIONESPM ";
            string actualizar3 = "INNER JOIN CLIENTESPM ON CLIENTESPM_id_clientespm = id_clientespm ";
            string actualizar4 = "INNER JOIN USUARIOSPM ON USUARIOSPM_id_usuariospm = Id_usuariospm ";
            string actualizar5 = "";
            if (filtro == 1)
            {
                actualizar5 = "Order By id_cotizacionespm";
            }
            if (filtro == 2)
            {
                actualizar5 = "WHERE fecha_cotizacionespm >= '" + fechaIn + " 0:0' AND fecha_cotizacionespm <= '" + fechaFin + " 23:59' Order By fecha_cotizacionespm";
            }
            if (filtro == 3)
            {
                actualizar5 = "WHERE nombre_clientespm = '" + contacto + "' Order By id_cotizacionespm";
            }
            if (filtro == 4)
            {
                actualizar5 = "WHERE fecha_cotizacionespm >= '" + fechaIn + " 0:0' AND fecha_cotizacionespm <= '" + fechaFin + " 23:59' AND nombre_usuariospm = '" + usuario + "' Order By id_cotizacionespm";
            }
            if (filtro == 5)
            {
                actualizar5 = "WHERE fecha_cotizacionespm >= '" + fechaIn + " 0:0' AND fecha_cotizacionespm <= '" + fechaFin + " 23:59' AND estado_cotizacionespm = 'Vendida' Order By id_cotizacionespm";
            }
            if (filtro == 6)
            {
                actualizar5 = "WHERE estado_cotizacionespm = 'Vendida' AND fecha_cotizacionespm >= '" + fechaIn + " 0:0' AND fecha_cotizacionespm <= '" + fechaFin + " 23:59' Order By fecha_cotizacionespm";
            }

            string actualizar = actualizar1 + actualizar2 + actualizar3 + actualizar4 + actualizar5;
            SqlCommand cmd = new SqlCommand(actualizar, conectar);
            SqlDataAdapter adaptador = new SqlDataAdapter(cmd);
            System.Data.DataTable tabla = new System.Data.DataTable();
            adaptador.Fill(tabla);
            TablaC.DataSource = tabla;
            TablaC.Columns[0].Width = 40;
            TablaC.Columns[1].Width = 150;
            TablaC.Columns[2].Width = 150;
            TablaC.Columns[3].Width = 100;
            TablaC.Columns[4].Width = 100;
            TablaC.Columns[5].Width = 160;
            TablaC.Columns[6].Width = 150;
            TablaC.Columns[7].Width = 150;
            conectar.Close();
        }

        // Actualizar Reporte de Cotizaciones 
        public void actualizarReporteServicios(DataGridView TablaC, string fechaIn, string fechaFin, string contacto, string usuario, int filtro)
        {

            string actualizar1 = "SELECT id_historialpm AS 'No.', nombre_clientespm AS 'Contacto', nit_clientespm AS 'Nit', fecha_historialpm AS 'Fecha Servicio', tipo_historialpm AS 'Tipo', id_colaboradorespm AS 'Colaborador'";
            string actualizar2 = "FROM HISTORIALPM ";
            string actualizar3 = "INNER JOIN CLIENTESPM ON CLIENTESPM_id_clientespm = id_clientespm ";
            string actualizar4 = "INNER JOIN COLABORADORESPM ON COLABORADORESPM_id_colaboradorespm = id_colaboradorespm ";
            string actualizar5 = "";
            if (filtro == 1)
            {
                actualizar5 = "Order By id_historialpm";
            }
            if (filtro == 2)
            {
                actualizar5 = "WHERE fecha_historialpm >= '" + fechaIn + " 0:0' AND fecha_historialpm <= '" + fechaFin + " 23:59' Order By fecha_historialpm";
            }
            if (filtro == 3)
            {
                actualizar5 = "WHERE nombre_clientespm = '" + contacto + "' Order By id_historialpm";
            }
            if (filtro == 4)
            {
                actualizar5 = "WHERE id_colaboradorespm = '" + usuario + "' Order By id_historialpm";
            }

            string actualizar = actualizar1 + actualizar2 + actualizar3 + actualizar4 + actualizar5;
            SqlCommand cmd = new SqlCommand(actualizar, conectar);
            SqlDataAdapter adaptador = new SqlDataAdapter(cmd);
            System.Data.DataTable tabla = new System.Data.DataTable();
            adaptador.Fill(tabla);
            TablaC.DataSource = tabla;
            TablaC.Columns[0].Width = 40;
            TablaC.Columns[1].Width = 150;
            TablaC.Columns[2].Width = 100;
            TablaC.Columns[3].Width = 150;
            TablaC.Columns[4].Width = 125;
            TablaC.Columns[5].Width = 100;
            conectar.Close();
        }

        // Funcion para modificar USUARIOS DEL SISTEMA
        public void modificarUsuariossistema(string nombre, string dpi, string telefono, string direccion, string email, string permiso, string usuario, string contra)
        {
            try
            {
                conectar.Open();
                string modificar = "UPDATE USUARIOSPM SET dpi_usuariospm = @dpi, telefono_usuariospm = @telefono ,direccion_usuariospm = @direccion, email_usuariospm = @email, permiso_usuariospm = @permiso, usuario_usuariospm = @usuario, contra_usuariospm = @contra WHERE nombre_usuariospm =@nombre";
                cmd = new SqlCommand(modificar, conectar);
                cmd.Parameters.AddWithValue("@nombre", nombre);
                cmd.Parameters.AddWithValue("@dpi", dpi);
                cmd.Parameters.AddWithValue("@telefono", telefono);
                cmd.Parameters.AddWithValue("@direccion", direccion);
                cmd.Parameters.AddWithValue("@email", email);
                cmd.Parameters.AddWithValue("@permiso", permiso);
                cmd.Parameters.AddWithValue("@usuario", usuario);
                cmd.Parameters.AddWithValue("@contra", contra);
                cmd.ExecuteNonQuery();
                FMessageBox obj = new FMessageBox("¡Elemento Modificado!", "3. Base De Datos", 0);
                if (obj.ShowDialog() == DialogResult.OK) ;
                conectar.Close();
            }
            catch (Exception ex)
            {
                FMessageBox obj = new FMessageBox("Error 003: " + ex.Message, "¡Error de Conexión!", 1);
                if (obj.ShowDialog() == DialogResult.OK) ;
            }
        }

        // Funcion para modificar Alertas DE SERVICIOS
        public void modificarAlertasServicios(string estado, string fecha, string id)
        {
            try
            {
                conectar.Open();
                string modificar1 = "UPDATE ALERTASPM SET fecha_alertaspm = @fecha, estado_alertaspm = @estado ";
                string modificar2 = "WHERE id_alertaspm =@id";
                string modificar = modificar1 + modificar2;
                cmd = new SqlCommand(modificar, conectar);
                cmd.Parameters.AddWithValue("@id", id);
                cmd.Parameters.AddWithValue("@fecha", fecha);
                cmd.Parameters.AddWithValue("@estado", estado);
                cmd.ExecuteNonQuery();
                FMessageBox obj = new FMessageBox("¡Elemento Modificado!", "3. Base De Datos", 0);
                if (obj.ShowDialog() == DialogResult.OK) ;
                conectar.Close();
            }
            catch (Exception ex)
            {
                FMessageBox obj = new FMessageBox("Error 003: " + ex.Message, "¡Error de Conexión!", 1);
                if (obj.ShowDialog() == DialogResult.OK) ;
            }
        }

        // FUNCION PARA ELIMINAR COLABORADORES
        public void eliminarusuariossistema(string usuario)
        {
            try
            {
                conectar.Open();
                string eliminar = "DELETE FROM USUARIOSPM WHERE usuario_usuariospm=@usuario";
                cmd = new SqlCommand(eliminar, conectar);
                cmd.Parameters.AddWithValue("@usuario", usuario);
                cmd.ExecuteNonQuery();
                FMessageBox obj = new FMessageBox("¡Elemento Eliminado!", "2. Base De Datos", 0);
                if (obj.ShowDialog() == DialogResult.OK) ;
                conectar.Close();
            }
            catch (Exception ex)
            {
                FMessageBox obj = new FMessageBox("Error 002: " + ex.Message, "¡Error de Conexión!", 1);
                if (obj.ShowDialog() == DialogResult.OK) ;
            }
        }

        // FUNCION PARA INGRESAR SERVICIOS
        public void ingresarservicios(DateTime fecha, string descripcion, string tipo, string fechainicio, string fechafinal, string nit, string id,string correlativo)
        {
            try
            {
                conectar.Open();
                string ingresar = "INSERT INTO HISTORIALPM (fecha_historialpm, descripcion_historialpm , tipo_historialpm,fechainicio_historialpm,fechafinal_historialpm,CLIENTESPM_id_clientespm, COLABORADORESPM_id_colaboradorespm,correlativo_historialpm) VALUES (@fecha ,@descripcion, @tipo, @fechainicio, @fechafinal, @nit, @id,@correlativo)";
                cmd = new SqlCommand(ingresar, conectar);
                cmd.Parameters.AddWithValue("@fecha", fecha);
                cmd.Parameters.AddWithValue("@descripcion", descripcion);
                cmd.Parameters.AddWithValue("@tipo", tipo);
                cmd.Parameters.AddWithValue("@fechainicio", fechainicio);
                cmd.Parameters.AddWithValue("@fechafinal", fechafinal);
                cmd.Parameters.AddWithValue("@nit", nit);
                cmd.Parameters.AddWithValue("@id",id);
                cmd.Parameters.AddWithValue("@correlativo", correlativo);
                cmd.ExecuteNonQuery();
                FMessageBox obj = new FMessageBox("Elemento almacenado con exito.", "1. Base De Datos", 0);
                if (obj.ShowDialog() == DialogResult.OK) ;
                conectar.Close();
            }
            catch (Exception ex)
            {
                FMessageBox obj = new FMessageBox("Error 001: " + ex.Message, "¡Error de Conexión!", 1);
                if (obj.ShowDialog() == DialogResult.OK) ;
            }
        }

        public void actualizarServicios(DataGridView TablaC)
        {
            string actualizar1 = "SELECT id_historialpm AS 'No.', nit_clientespm AS 'Nit', COLABORADORESPM_id_colaboradorespm AS 'IDColaborador', fecha_historialpm AS 'Fecha', tipo_historialpm AS 'Tipo', fechainicio_historialpm AS 'Inicio', fechafinal_historialpm AS 'Final',descripcion_historialpm AS 'Descripcion',";
            string actualizar2 = "nombre_colaboradorespm AS 'Colaborador', apellido_clientespm AS 'Empresa', nombre_clientespm AS 'Contacto',telefono_clientespm AS 'Telefono',direccion_clientespm AS 'Direccion',correlativo_historialpm AS 'Correlativo' ";    
            string actualizar3 = "FROM HISTORIALPM INNER JOIN CLIENTESPM ON CLIENTESPM_id_clientespm = id_clientespm ";
            string actualizar4 = "INNER JOIN COLABORADORESPM ON COLABORADORESPM_id_colaboradorespm = id_colaboradorespm ";
            string actualizar = actualizar1 + actualizar2 + actualizar3 + actualizar4;

            SqlCommand cmd = new SqlCommand(actualizar, conectar);
            SqlDataAdapter adaptador = new SqlDataAdapter(cmd);
            System.Data.DataTable tabla = new System.Data.DataTable();
            adaptador.Fill(tabla);
            TablaC.DataSource = tabla;
            TablaC.Columns[0].Width = 40;
            TablaC.Columns[1].Width = 80;
            TablaC.Columns[2].Width = 100;
            TablaC.Columns[3].Width = 100;
            TablaC.Columns[4].Width = 100;
            TablaC.Columns[5].Width = 80;
            TablaC.Columns[6].Width = 80;
            TablaC.Columns[7].Width = 150;
            TablaC.Columns[8].Width = 80;
            TablaC.Columns[9].Width = 120;
            TablaC.Columns[10].Width = 80;
            TablaC.Columns[11].Width = 80;
            TablaC.Columns[12].Width = 80;
            TablaC.Columns[13].Width = 80;


            conectar.Close();
        }
        // Buscar SERVICIO automaticamente
        public void busquedaservicio(DataGridView tablaC, TextBox texto, string columna)
        {
            try
            {
                conectar.Open();
                string buscar1 = "SELECT id_historialpm AS 'No.', nit_clientespm AS 'Nit', COLABORADORESPM_id_colaboradorespm AS 'IDColaborador', fecha_historialpm AS 'Fecha', tipo_historialpm AS 'Tipo', fechainicio_historialpm AS 'Inicio', fechafinal_historialpm AS 'Final',descripcion_historialpm AS 'Descripcion',";
                string buscar2 = "nombre_colaboradorespm AS 'Colaborador', apellido_clientespm AS 'Empresa', nombre_clientespm AS 'Contacto', telefono_clientespm AS 'Telefono',direccion_clientespm AS 'Direccion', correlativo_historialpm AS 'Correlativo' ";
                string buscar3 = "FROM HISTORIALPM INNER JOIN CLIENTESPM ON CLIENTESPM_id_clientespm = id_clientespm ";
                string buscar4 = "INNER JOIN COLABORADORESPM ON COLABORADORESPM_id_colaboradorespm = id_colaboradorespm ";
                string buscar5 = "WHERE " + columna + " like ('" + texto.Text + "%')";
                string buscar = buscar1 + buscar2 + buscar3 + buscar4 + buscar5;

               
                SqlCommand cmd = new SqlCommand(buscar, conectar);
                cmd.ExecuteNonQuery();
                SqlDataAdapter adaptador = new SqlDataAdapter(cmd);
                System.Data.DataTable tabla = new System.Data.DataTable();
                adaptador.Fill(tabla);
                tablaC.DataSource = tabla;
                conectar.Close();
            }
            catch (Exception ex)
            {
                FMessageBox obj = new FMessageBox("Error 004: " + ex.Message, "¡Error de Conexión!", 1);
                if (obj.ShowDialog() == DialogResult.OK) ;
            }
        }
        // Buscar SERVICIO POR TIPO automaticamente
        public void busquedaserviciotipo(DataGridView tablaC, ComboBox texto, string columna)
        {
            try
            {
                conectar.Open();
                string buscar1 = "SELECT id_historialpm AS 'No.', nit_clientespm AS 'Nit', COLABORADORESPM_id_colaboradorespm AS 'IDColaborador', fecha_historialpm AS 'Fecha', tipo_historialpm AS 'Tipo', fechainicio_historialpm AS 'Inicio', fechafinal_historialpm AS 'Final',descripcion_historialpm AS 'Descripcion',";
                string buscar2 = "nombre_colaboradorespm AS 'Colaborador', apellido_clientespm AS 'Empresa',nombre_clientespm AS 'Contacto', telefono_clientespm AS 'Telefono',direccion_clientespm AS 'Direccion', correlativo_historialpm AS 'Correlativo' ";
                string buscar3 = "FROM HISTORIALPM INNER JOIN CLIENTESPM ON CLIENTESPM_id_clientespm = id_clientespm ";
                string buscar4 = "INNER JOIN COLABORADORESPM ON COLABORADORESPM_id_colaboradorespm = id_colaboradorespm ";
                string buscar5 = "WHERE " + columna + " like ('" + texto.Text + "%')";
                string buscar = buscar1 + buscar2 + buscar3 + buscar4 + buscar5;


                SqlCommand cmd = new SqlCommand(buscar, conectar);
                cmd.ExecuteNonQuery();
                SqlDataAdapter adaptador = new SqlDataAdapter(cmd);
                System.Data.DataTable tabla = new System.Data.DataTable();
                adaptador.Fill(tabla);
                tablaC.DataSource = tabla;
                conectar.Close();
            }
            catch (Exception ex)
            {
                FMessageBox obj = new FMessageBox("Error 004: " + ex.Message, "¡Error de Conexión!", 1);
                if (obj.ShowDialog() == DialogResult.OK) ;
            }
        }


        // Buscar Datos en Cotizacion para los REPORTES
        public void busquedaReportesCotizacion(DataGridView tablaC, TextBox texto, string columna)
        {
            try
            {
                conectar.Open();
                string buscar1 = "SELECT id_cotizacionespm AS 'No.', nombre_clientespm AS 'Contacto', apellido_clientespm AS 'Empresa', nit_clientespm AS 'Nit', telefono_clientespm AS 'Teléfono', fecha_cotizacionespm AS 'Fecha Cotización', estado_cotizacionespm  AS 'Estado', nombre_usuariospm AS 'Usuario' ";
                string buscar2 = "FROM COTIZACIONESPM ";
                string buscar3 = "INNER JOIN CLIENTESPM ON CLIENTESPM_id_clientespm = id_clientespm ";
                string buscar4 = "INNER JOIN USUARIOSPM ON USUARIOSPM_id_usuariospm = Id_usuariospm ";
                string buscar5 = "WHERE " + columna + " like ('" + texto.Text + "%')";
                string buscar = buscar1 + buscar2 + buscar3 + buscar4 + buscar5;


                SqlCommand cmd = new SqlCommand(buscar, conectar);
                cmd.ExecuteNonQuery();
                SqlDataAdapter adaptador = new SqlDataAdapter(cmd);
                System.Data.DataTable tabla = new System.Data.DataTable();
                adaptador.Fill(tabla);
                tablaC.DataSource = tabla;
                conectar.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error de Conexión:");
            }
        }

        // Buscar Datos en Cotizacion para los REPORTES
        public void busquedaReportesServicios(DataGridView tablaC, TextBox texto, string columna)
        {
            try
            {
                conectar.Open();
                string buscar1 = "SELECT id_historialpm AS 'No.', nombre_clientespm AS 'Contacto', nit_clientespm AS 'Nit', fecha_historialpm AS 'Fecha Servicio', tipo_historialpm AS 'Tipo', id_colaboradorespm AS 'Colaborador'";
                string buscar2 = "FROM HISTORIALPM ";
                string buscar3 = "INNER JOIN CLIENTESPM ON CLIENTESPM_id_clientespm = id_clientespm ";
                string buscar4 = "INNER JOIN COLABORADORESPM ON COLABORADORESPM_id_colaboradorespm = id_colaboradorespm ";
                string buscar5 = "WHERE " + columna + " like ('" + texto.Text + "%')";
                string buscar = buscar1 + buscar2 + buscar3 + buscar4 + buscar5;

                SqlCommand cmd = new SqlCommand(buscar, conectar);
                cmd.ExecuteNonQuery();
                SqlDataAdapter adaptador = new SqlDataAdapter(cmd);
                System.Data.DataTable tabla = new System.Data.DataTable();
                adaptador.Fill(tabla);
                tablaC.DataSource = tabla;
                conectar.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, "Error de Conexión:");
            }
        }
        

        // FUNCION PARA INGRESAR ALERTA
        public void ingresaralerta(DateTime fecha, string descripcion,int estado, string id)
        {
            try
            {
                conectar.Open();
                string ingresar = "INSERT INTO ALERTASPM (fecha_alertaspm, descripcion_alertaspm ,estado_alertaspm,CLIENTESPM_id_clientespm) VALUES (@fecha ,@descripcion,@estado, @id)";
                cmd = new SqlCommand(ingresar, conectar);
                cmd.Parameters.AddWithValue("@fecha", fecha);
                cmd.Parameters.AddWithValue("@descripcion", descripcion);
                cmd.Parameters.AddWithValue("@estado", estado);
                cmd.Parameters.AddWithValue("@id", id);
                cmd.ExecuteNonQuery();
                //FMessageBox obj = new FMessageBox("Elemento almacenado con exito.", "1. Base De Datos", 0);
                //if (obj.ShowDialog() == DialogResult.OK) ;

                conectar.Close();
            }
            catch (Exception ex)
            {
                FMessageBox obj = new FMessageBox("Error 001: " + ex.Message, "¡Error de Conexión!", 1);
                if (obj.ShowDialog() == DialogResult.OK) ;
            }
        }



    }
}
